import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gameapp/routes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:connectivity/connectivity.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart';


final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

void main() async {
  initializeBindingAndError();
  initializeOneSignal();
  initializePersistenceData();

  runApp(MyApp());
}

void initializeBindingAndError() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  ErrorWidget.builder = (FlutterErrorDetails details) => Container();
}

void initializeAdmob() {
  Admob.initialize('ca-app-pub-5634554394830090~6837716453', );
}



void initializeOneSignal() {
  OneSignal.shared.init("8b45cfd5-ebee-46ef-b00d-47008edb83d6");
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  /*OneSignal.shared.setNotificationOpenedHandler((data) async {
    OSNotificationOpenedResult openedResult = data;
    OSNotificationPayload notification = openedResult.notification.payload;
    navigatorKey.currentState.pushAndRemoveUntil(new MaterialPageRoute(builder: (context) => StartUserPage(index: notification.additionalData['indexStart'],)), ModalRoute.withName('/'));
  }); */
}

void initializePersistenceData() async {
  Firestore.instance.settings(persistenceEnabled: true);

  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.none) {
      await Firestore.instance.settings(source: Source.cache);
  } else {
      await Firestore.instance.settings(source: Source.serverAndCache);
  }  
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin, WidgetsBindingObserver {

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    
    switch(state){
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.resumed:
        _renewToken();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.suspending:
        break;
    }
  }

  _renewToken() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user.getIdToken() == null) {
      user.getIdToken(refresh: true);
    }
    
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Game Sync',
      navigatorKey: navigatorKey,
      initialRoute: 'screen',
      routes: Routes.routes,
      theme: ThemeData(
        brightness: Brightness.dark,
        bottomAppBarColor: HexColor('23395d'),
        primaryColor: HexColor('203454'),
        //primaryColor: HexColor('c7620c'),
        accentColor: HexColor('b3cde0'),
      ),
    );
  }
}