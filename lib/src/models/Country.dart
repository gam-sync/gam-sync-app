class Country {
  String countryName;
  String countryShortCode;
  List<Regions> regions;

  Country({this.countryName, this.countryShortCode, this.regions});

  Country.fromJson(Map<String, dynamic> json) {
    countryName = json['countryName'];
    countryShortCode = json['countryShortCode'];
    if (json['regions'] != null) {
      regions = new List<Regions>();
      json['regions'].forEach((v) {
        regions.add(new Regions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['countryName'] = this.countryName;
    data['countryShortCode'] = this.countryShortCode;
    if (this.regions != null) {
      data['regions'] = this.regions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Regions {
  String name;
  String shortCode;

  Regions({this.name, this.shortCode});

  Regions.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    shortCode = json['shortCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['shortCode'] = this.shortCode;
    return data;
  }
}