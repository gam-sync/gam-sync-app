class ProfileLoL {
  final String id;
  final String accountId;
  final String puuid;
  final String name;
  final int profileIconId;
  final int revisionDate;
  final int summonerLevel;

  ProfileLoL({this.id, this.accountId, this.puuid, this.name, this.profileIconId, this.revisionDate, this.summonerLevel});

  factory ProfileLoL.fromJson(Map<String, dynamic> json) {
    return ProfileLoL(
      id: json['id'],
      accountId: json['accountId'],
      puuid: json['puuid'],
      name: json['name'],
      profileIconId: json['profileIconId'],
      revisionDate: json['revisionDate'],
      summonerLevel: json['summonerLevel']
    );
  }
}



class ListQueueLoL {
  final List<QueueLoL> queues;

  ListQueueLoL({
    this.queues
  });

  factory ListQueueLoL.fromJson(List<dynamic> parsedJson) {

    List<QueueLoL> queues = new List<QueueLoL>();

    return new ListQueueLoL(
       queues: queues,
    );
  }
}

class QueueLoL {
    String leagueId;
    String queueType;
    String tier;
    String rank;
    String summonerId;
    String summonerName;
    int leaguePoints;
    int wins;
    int losses;
    bool veteran;
    bool inactive;
    bool freshBlood;
    bool hotStreak;

    QueueLoL({
        this.leagueId,
        this.queueType,
        this.tier,
        this.rank,
        this.summonerId,
        this.summonerName,
        this.leaguePoints,
        this.wins,
        this.losses,
        this.veteran,
        this.inactive,
        this.freshBlood,
        this.hotStreak,
    });

    factory QueueLoL.fromJson(Map<String, dynamic> json) => new QueueLoL(
        leagueId: json["leagueId"],
        queueType: json["queueType"],
        tier: json["tier"],
        rank: json["rank"],
        summonerId: json["summonerId"],
        summonerName: json["summonerName"],
        leaguePoints: json["leaguePoints"],
        wins: json["wins"],
        losses: json["losses"],
        veteran: json["veteran"],
        inactive: json["inactive"],
        freshBlood: json["freshBlood"],
        hotStreak: json["hotStreak"],
    );
    
    Map<String, dynamic> toJson() => {
        "leagueId": leagueId,
        "queueType": queueType,
        "tier": tier,
        "rank": rank,
        "summonerId": summonerId,
        "summonerName": summonerName,
        "leaguePoints": leaguePoints,
        "wins": wins,
        "losses": losses,
        "veteran": veteran,
        "inactive": inactive,
        "freshBlood": freshBlood,
        "hotStreak": hotStreak,
    };
}


class MatchList {
    List<Match> matches;
    int endIndex;
    int startIndex;
    int totalGames;

    MatchList({
        this.matches,
        this.endIndex,
        this.startIndex,
        this.totalGames,
    });

    factory MatchList.fromJson(Map<String, dynamic> json) => MatchList(
        matches: List<Match>.from(json["matches"].map((x) => Match.fromJson(x))),
        endIndex: json["endIndex"],
        startIndex: json["startIndex"],
        totalGames: json["totalGames"],
    );

    Map<String, dynamic> toJson() => {
        "matches": List<dynamic>.from(matches.map((x) => x.toJson())),
        "endIndex": endIndex,
        "startIndex": startIndex,
        "totalGames": totalGames,
    };
}

class Match {
    Lane lane;
    int gameId;
    int champion;
    PlatformId platformId;
    int timestamp;
    int queue;
    Role role;
    int season;

    Match({
        this.lane,
        this.gameId,
        this.champion,
        this.platformId,
        this.timestamp,
        this.queue,
        this.role,
        this.season,
    });

    factory Match.fromJson(Map<String, dynamic> json) => Match(
        lane: laneValues.map[json["lane"]],
        gameId: json["gameId"],
        champion: json["champion"],
        platformId: platformIdValues.map[json["platformId"]],
        timestamp: json["timestamp"],
        queue: json["queue"],
        role: roleValues.map[json["role"]],
        season: json["season"],
    );

    Map<String, dynamic> toJson() => {
        "lane": laneValues.reverse[lane],
        "gameId": gameId,
        "champion": champion,
        "platformId": platformIdValues.reverse[platformId],
        "timestamp": timestamp,
        "queue": queue,
        "role": roleValues.reverse[role],
        "season": season,
    };
}

enum Lane { BOTTOM, MID, NONE, TOP, JUNGLE }

final laneValues = EnumValues({
    "BOTTOM": Lane.BOTTOM,
    "JUNGLE": Lane.JUNGLE,
    "MID": Lane.MID,
    "NONE": Lane.NONE,
    "TOP": Lane.TOP
});

enum PlatformId { EUW1 }

final platformIdValues = EnumValues({
    "EUW1": PlatformId.EUW1
});

enum Role { DUO_CARRY, DUO_SUPPORT, SOLO, DUO, NONE }

final roleValues = EnumValues({
    "DUO": Role.DUO,
    "DUO_CARRY": Role.DUO_CARRY,
    "DUO_SUPPORT": Role.DUO_SUPPORT,
    "NONE": Role.NONE,
    "SOLO": Role.SOLO
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
