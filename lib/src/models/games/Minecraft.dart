class UserMine {
  String id;
  String name;

  UserMine({this.id, this.name});

  UserMine.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
}