class OverwatchUser {
    String icon;
    String name;
    int level;
    String levelIcon;
    int prestige;
    String prestigeIcon;
    int rating;
    String ratingIcon;
    int gamesWon;
    Stats quickPlayStats;
    Stats competitiveStats;

    OverwatchUser({
        this.icon,
        this.name,
        this.level,
        this.levelIcon,
        this.prestige,
        this.prestigeIcon,
        this.rating,
        this.ratingIcon,
        this.gamesWon,
        //this.quickPlayStats,
        //this.competitiveStats,
    });

    factory OverwatchUser.fromJson(Map<String, dynamic> json) => OverwatchUser(
        icon: json["icon"],
        name: json["name"],
        level: json["level"],
        levelIcon: json["levelIcon"],
        prestige: json["prestige"],
        prestigeIcon: json["prestigeIcon"],
        rating: json["rating"],
        ratingIcon: json["ratingIcon"],
        gamesWon: json["gamesWon"],
        //quickPlayStats: json["quickPlayStats"].length == 0 ? null : Stats.fromJson(json["quickPlayStats"]),
        //competitiveStats: json["competitiveStats"].length == 0 ? null : Stats.fromJson(json["competitiveStats"]),
    );

    Map<String, dynamic> toJson() => {
        "icon": icon,
        "name": name,
        "level": level,
        "levelIcon": levelIcon,
        "prestige": prestige,
        "prestigeIcon": prestigeIcon,
        "rating": rating,
        "ratingIcon": ratingIcon,
        "gamesWon": gamesWon,
        //"quickPlayStats": quickPlayStats.toJson().length == 0 ? {} : quickPlayStats.toJson(),
        //"competitiveStats": competitiveStats.toJson().length == 0 ? {} : competitiveStats.toJson(),
    };
}

class Stats {
    double eliminationsAvg;
    int damageDoneAvg;
    double deathsAvg;
    double finalBlowsAvg;
    int healingDoneAvg;
    double objectiveKillsAvg;
    String objectiveTimeAvg;
    double soloKillsAvg;
    Games games;
    Awards awards;

    Stats({
        this.eliminationsAvg,
        this.damageDoneAvg,
        this.deathsAvg,
        this.finalBlowsAvg,
        this.healingDoneAvg,
        this.objectiveKillsAvg,
        this.objectiveTimeAvg,
        this.soloKillsAvg,
        this.games,
        this.awards,
    });

    factory Stats.fromJson(Map<String, dynamic> json) => Stats(
        eliminationsAvg: json["eliminationsAvg"].toDouble(),
        damageDoneAvg: json["damageDoneAvg"],
        deathsAvg: json["deathsAvg"].toDouble(),
        finalBlowsAvg: json["finalBlowsAvg"].toDouble(),
        healingDoneAvg: json["healingDoneAvg"],
        objectiveKillsAvg: json["objectiveKillsAvg"].toDouble(),
        objectiveTimeAvg: json["objectiveTimeAvg"],
        soloKillsAvg: json["soloKillsAvg"].toDouble(),
        games: Games.fromJson(json["games"]),
        awards: Awards.fromJson(json["awards"]),
    );

    Map<String, dynamic> toJson() => {
        "eliminationsAvg": eliminationsAvg,
        "damageDoneAvg": damageDoneAvg,
        "deathsAvg": deathsAvg,
        "finalBlowsAvg": finalBlowsAvg,
        "healingDoneAvg": healingDoneAvg,
        "objectiveKillsAvg": objectiveKillsAvg,
        "objectiveTimeAvg": objectiveTimeAvg,
        "soloKillsAvg": soloKillsAvg,
        "games": games.toJson(),
        "awards": awards.toJson(),
    };
}

class Awards {
    int cards;
    int medals;
    int medalsBronze;
    int medalsSilver;
    int medalsGold;

    Awards({
        this.cards,
        this.medals,
        this.medalsBronze,
        this.medalsSilver,
        this.medalsGold,
    });

    factory Awards.fromJson(Map<String, dynamic> json) => Awards(
        cards: json["cards"],
        medals: json["medals"],
        medalsBronze: json["medalsBronze"],
        medalsSilver: json["medalsSilver"],
        medalsGold: json["medalsGold"],
    );

    Map<String, dynamic> toJson() => {
        "cards": cards,
        "medals": medals,
        "medalsBronze": medalsBronze,
        "medalsSilver": medalsSilver,
        "medalsGold": medalsGold,
    };
}

class Games {
    int played;
    int won;

    Games({
        this.played,
        this.won,
    });

    factory Games.fromJson(Map<String, dynamic> json) => Games(
        played: json["played"],
        won: json["won"],
    );

    Map<String, dynamic> toJson() => {
        "played": played,
        "won": won,
    };
}