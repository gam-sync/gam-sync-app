import 'dart:convert';

BrawlUser userFromJson(String str) => BrawlUser.fromJson(json.decode(str));

String userToJson(BrawlUser data) => json.encode(data.toJson());

class BrawlUser {
    String tag;
    Id id;
    String name;
    String nameColorCode;
    int brawlersUnlocked;
    int victories;
    int soloShowdownVictories;
    int duoShowdownVictories;
    int totalExp;
    String expFmt;
    int expLevel;
    int trophies;
    int highestTrophies;
    int avatarId;
    String avatarUrl;
    String bestTimeAsBigBrawler;
    String bestRoboRumbleTime;
    bool hasSkins;
    Club club;
    List<Brawler> brawlers;

    BrawlUser({
        this.tag,
        this.id,
        this.name,
        this.nameColorCode,
        this.brawlersUnlocked,
        this.victories,
        this.soloShowdownVictories,
        this.duoShowdownVictories,
        this.totalExp,
        this.expFmt,
        this.expLevel,
        this.trophies,
        this.highestTrophies,
        this.avatarId,
        this.avatarUrl,
        this.bestTimeAsBigBrawler,
        this.bestRoboRumbleTime,
        this.hasSkins,
        this.club,
        this.brawlers,
    });

    factory BrawlUser.fromJson(Map<String, dynamic> json) => new BrawlUser(
        tag: json["tag"],
        id: Id.fromJson(json["id"]),
        name: json["name"],
        nameColorCode: json["nameColorCode"],
        brawlersUnlocked: json["brawlersUnlocked"],
        victories: json["victories"],
        soloShowdownVictories: json["soloShowdownVictories"],
        duoShowdownVictories: json["duoShowdownVictories"],
        totalExp: json["totalExp"],
        expFmt: json["expFmt"],
        expLevel: json["expLevel"],
        trophies: json["trophies"],
        highestTrophies: json["highestTrophies"],
        avatarId: json["avatarId"],
        avatarUrl: json["avatarUrl"],
        bestTimeAsBigBrawler: json["bestTimeAsBigBrawler"],
        bestRoboRumbleTime: json["bestRoboRumbleTime"],
        hasSkins: json["hasSkins"],
        club: null,
        brawlers: new List<Brawler>.from(json["brawlers"].map((x) => Brawler.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "tag": tag,
        "id": id.toJson(),
        "name": name,
        "nameColorCode": nameColorCode,
        "brawlersUnlocked": brawlersUnlocked,
        "victories": victories,
        "soloShowdownVictories": soloShowdownVictories,
        "duoShowdownVictories": duoShowdownVictories,
        "totalExp": totalExp,
        "expFmt": expFmt,
        "expLevel": expLevel,
        "trophies": trophies,
        "highestTrophies": highestTrophies,
        "avatarId": avatarId,
        "avatarUrl": avatarUrl,
        "bestTimeAsBigBrawler": bestTimeAsBigBrawler,
        "bestRoboRumbleTime": bestRoboRumbleTime,
        "hasSkins": hasSkins,
        "club": null,
        "brawlers": new List<dynamic>.from(brawlers.map((x) => x.toJson())),
    };
}

class Brawler {
    Brawler({
        this.name,
        this.hasSkin,
        this.skin,
        this.trophies,
        this.highestTrophies,
        this.power,
        this.rank,
    });

    factory Brawler.fromJson(Map<String, dynamic> json) => new Brawler(
        name: json["name"],
        hasSkin: json["hasSkin"],
        skin: json["skin"] == null ? null : json["skin"],
        trophies: json["trophies"],
        highestTrophies: json["highestTrophies"],
        power: json["power"],
        rank: json["rank"],
    );

    bool hasSkin;
    int highestTrophies;
    String name;
    int power;
    int rank;
    String skin;
    int trophies;

    Map<String, dynamic> toJson() => {
        "name": name,
        "hasSkin": hasSkin,
        "skin": skin == null ? null : skin,
        "trophies": trophies,
        "highestTrophies": highestTrophies,
        "power": power,
        "rank": rank,
    };
}

class Club {
    Club({
        this.id,
        this.tag,
        this.name,
        this.role,
        this.badgeId,
        this.badgeUrl,
        this.members,
        this.trophies,
        this.requiredTrophies,
        this.onlineMembers,
    });

    factory Club.fromJson(Map<String, dynamic> json) => new Club(
        id: Id.fromJson(json["id"]),
        tag: json["tag"],
        name: json["name"],
        role: json["role"],
        badgeId: json["badgeId"],
        badgeUrl: json["badgeUrl"],
        members: json["members"],
        trophies: json["trophies"],
        requiredTrophies: json["requiredTrophies"],
        onlineMembers: json["onlineMembers"],
    );

    int badgeId;
    String badgeUrl;
    Id id;
    int members;
    String name;
    int onlineMembers;
    int requiredTrophies;
    String role;
    String tag;
    int trophies;

    Map<String, dynamic> toJson() => {
        "id": id.toJson(),
        "tag": tag,
        "name": name,
        "role": role,
        "badgeId": badgeId,
        "badgeUrl": badgeUrl,
        "members": members,
        "trophies": trophies,
        "requiredTrophies": requiredTrophies,
        "onlineMembers": onlineMembers,
    };
}

class Id {
    Id({
        this.high,
        this.low,
    });

    factory Id.fromJson(Map<String, dynamic> json) => new Id(
        high: json["high"],
        low: json["low"],
    );

    int high;
    int low;

    Map<String, dynamic> toJson() => {
        "high": high,
        "low": low,
    };
}
