// Flutter & Dart
import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Firebase
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:gameapp/src/models/Notification.dart' as prefix0;
import 'package:gameapp/src/providers/notification_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:flutter_twitter_login/flutter_twitter_login.dart';

// Notifications
import 'package:onesignal_flutter/onesignal_flutter.dart';

// Utils
import 'package:path/path.dart' as p;

class UserProvider {
  static login(BuildContext context,
      {bool providerGoogle,
      String email,
      String password,
      bool providerTwitter}) async {
    FirebaseUser user;

    final Map<String, dynamic> errors = {
      'mail_not_found': null,
      'fail_password': null,
      'unknown': null,
    };

    try {
      if (providerGoogle == null && providerTwitter == null) {
        user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password);
      }
      if (providerTwitter) {
        user = await signInWithTwitter(context);
      }

      /*if (providerGoogle) {
        user = await signInWithGoogle();
      } */
    } catch (error) {
      if (error is PlatformException) {
        switch (error.code) {
          case "ERROR_USER_NOT_FOUND":
            errors['mail_not_found'] =
                'El correo electronico no esta registrado';
            return errors;
            break;
          case "sign_in_failed":
            errors['mail_not_found'] =
                'Ha ocurrido un error al iniciar sesión';
            break;
          case "ERROR_WRONG_PASSWORD":
            errors['fail_password'] = 'La contraseña es incorrecta';
            return errors;
            break;
          case "ERROR_TOO_MANY_REQUESTS":
            errors['unknown'] = 'Se ha producido un error desconocido';
            return errors;
            break;
          case "ERROR_USER_DISABLED":
            errors['user_disabled'] = "Este usuario se ha deshabilitado";
            return errors;
            break;
          case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
            ToastUtil.show(
                "Esta cuenta ya existe en otro proveedor de cuentas.", context);
        }
      }
    }

    var userData = await getDataUser(user.uid);

    if (!userData.data['banned']) {
      var status = await OneSignal.shared.getPermissionSubscriptionState();

      // Update oneSignalId for send push notifications
      Firestore.instance
          .collection('users')
          .document(user.uid)
          .updateData({'oneSignalId': status.subscriptionStatus.userId});
      Navigator.of(context).pushNamedAndRemoveUntil(
        'startuser',
        (Route<dynamic> route) => false,
      );
    } else {
      errors['user_banned'] = "Tu cuenta ha sido bloqueada.";
      ToastUtil.show("Tu cuenta ha sido bloqueada", context);
      return errors;
    }
  }

  static Future<FirebaseUser> signInWithTwitter(BuildContext context) async {
    TwitterLogin twitterInstance = new TwitterLogin(
        consumerKey: "1k8FsWvzVTaPdTMRg6x3dIiau",
        consumerSecret: "a3s0nXSTdQfoZvIVA4mRRuNQp6D9LFSXWQdAc10VhCGu4Wp0MP");

    TwitterLoginResult twitterLoginResult = await twitterInstance.authorize();

    TwitterSession currentUserTwitter = twitterLoginResult.session;

    AuthCredential authCredential = TwitterAuthProvider.getCredential(
        authToken: currentUserTwitter.token,
        authTokenSecret: currentUserTwitter.secret);

    FirebaseUser currentUser =
        await FirebaseAuth.instance.signInWithCredential(authCredential);

    DocumentSnapshot userData = await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .get();

    var status = await OneSignal.shared.getPermissionSubscriptionState();

    if (userData.exists) {
      if (!userData.data['banned']) {
        // Update oneSignalId for send push notifications
        Firestore.instance
            .collection('users')
            .document(currentUser.uid)
            .updateData({'oneSignalId': status.subscriptionStatus.userId});
        Navigator.of(context).pushNamedAndRemoveUntil(
          'startuser',
          (Route<dynamic> route) => false,
        );
      }
      return currentUser;
    } else {
      var randomNumber = new Random();
      int random = randomNumber.nextInt(999999);
      Map<String, dynamic> userToRegister = {
        'name': currentUser.displayName,
        'genre': 'Otro',
        'photoUrl': currentUser.photoUrl,
        'isEmailVerified': true,
        'email': currentUser.email,
        'bio': '',
        'banned': false,
        'city': 'Desconocido',
        'country': 'Desconocido',
        'random': random
      };
      registerWithGoogle(userToRegister, currentUser.uid);
      Firestore.instance
          .collection('users')
          .document(currentUser.uid)
          .updateData({'oneSignalId': status.subscriptionStatus.userId});
      Navigator.of(context).pushNamedAndRemoveUntil(
        'startuser',
        (Route<dynamic> route) => false,
      );
      return currentUser;
    }
  }

  /*static Future<FirebaseUser> signInWithGoogle() async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    GoogleSignIn googleSignIn = GoogleSignIn();

    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    FirebaseUser user = await _auth.signInWithCredential(credential);

    DocumentSnapshot userData =
        await Firestore.instance.collection('users').document(user.uid).get();

    if (userData.exists) {
      return user;
    } else {
      var randomNumber = new Random();
      int random = randomNumber.nextInt(999999);
      Map<String, dynamic> userToRegister = {
        'name': googleSignInAccount.displayName,
        'genre': 'Otro',
        'photoUrl': googleSignInAccount.photoUrl,
        'isEmailVerified': true,
        'email': googleSignInAccount.email,
        'bio': '',
        'banned': false,
        'city': 'Desconocido',
        'country': 'Desconocido',
        'random': random
      };
      await registerWithGoogle(userToRegister, user.uid);
      return user;
    }
  } */

  static Future<dynamic> registerWithGoogle(
      Map<String, dynamic> userToRegister, String uid) async {
    var status = await OneSignal.shared.getPermissionSubscriptionState();

    userToRegister['oneSignalId'] = status.subscriptionStatus.userId;
    userToRegister['settings'] = {'allowMessages': false};

    Firestore.instance
        .collection('users')
        .document(uid)
        .setData(userToRegister);
  }

  // Register user
  static Future<dynamic> register(Map<String, dynamic> userToRegister,
      String email, String password, BuildContext context, File image) async {
    FirebaseUser user;
    try {
      user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
    } catch (error) {
      if (error is PlatformException) {
        switch (error.code) {
          case "ERROR_EMAIL_ALREADY_IN_USE":
            return "Este email ya existe en nuestros registros.";
        }
      }
    }

    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var randomNumber = new Random();
    int random = randomNumber.nextInt(999999);

    //var urlPhotoProfile = await uploadPhotoProfile(context, image, user.uid);

    userToRegister['email'] = user.email;
    userToRegister['isEmailVerified'] = user.isEmailVerified;
    userToRegister['oneSignalId'] = status.subscriptionStatus.userId;
    userToRegister['photoUrl'] = 'https://firebasestorage.googleapis.com/v0/b/gameapp-99e0b.appspot.com/o/profile.png?alt=media&token=160188bf-b961-4db0-8d1f-66da0a0f8774';
    userToRegister['banned'] = false;
    userToRegister['isAdmin'] = false;
    userToRegister['created_at'] = FieldValue.serverTimestamp();
    userToRegister['settings'] = {
      'allowMessages': true,
      'showGames': true,
      'allowMentions': true
    };
    userToRegister['random'] = random;

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .setData(userToRegister);
    Navigator.of(context).pushNamedAndRemoveUntil(
      'startuser',
      (Route<dynamic> route) => false,
    );
  }

  static Future<String> uploadPhotoProfile(
      BuildContext context, File image, String uid) async {
    if (image != null) {
      String fileName = p.basename(image.path);
      var dateTime = DateTime.now();
      StorageReference ref = FirebaseStorage.instance
          .ref()
          .child('images/$uid')
          .child("p-" +
              dateTime.millisecondsSinceEpoch.toString() +
              "." +
              fileName.split(".").last);
      StorageUploadTask uploadTask = ref.putFile(image);

      return await (await uploadTask.onComplete).ref.getDownloadURL();
    } else {
      StorageReference ref = FirebaseStorage.instance
          .ref()
          .child('images/default')
          .child('defaultIcon.png');

      return (await ref.getDownloadURL()).toString();
    }
  }

  static Future<String> editPhoto(
      BuildContext context, File image, String uid) async {
    String fileName = p.basename(image.path);
    var dateTime = DateTime.now();
    StorageReference ref = FirebaseStorage.instance
        .ref()
        .child('images/$uid')
        .child("p-" +
            dateTime.millisecondsSinceEpoch.toString() +
            "." +
            fileName.split(".").last);

    StorageUploadTask uploadTask = ref.putFile(image);

    return await (await uploadTask.onComplete).ref.getDownloadURL();
  }

  // 1 writes
  static checkToken(BuildContext context) async {
    FirebaseUser user = await getCurrentAuthUser();
    if (user == null) {
      Navigator.pushReplacementNamed(context, 'start');
    } else {
      var status = await OneSignal.shared.getPermissionSubscriptionState();
      Firestore.instance
          .collection('users')
          .document(user.uid)
          .updateData({'oneSignalId': status.subscriptionStatus.userId});
      
      Navigator.of(context).pushNamedAndRemoveUntil(
        'startuser',
        (Route<dynamic> route) => false,
      );
    }
  }

  static Future<FirebaseUser> getCurrentAuthUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    return user;
  }

  static Future<DocumentSnapshot> getDataAuthUser() async {
    FirebaseUser user = await getCurrentAuthUser();
    DocumentSnapshot authData =
        await Firestore.instance.collection('users').document(user.uid).get();

    return authData;
  }

  static Future<DocumentSnapshot> getDataUser(String uid) async {
    DocumentSnapshot userData =
        await Firestore.instance.collection('users').document(uid).get();

    return userData;
  }

  static Stream<DocumentSnapshot> getDataUserSnapshots(String userId) {
    var userData =
        Firestore.instance.collection('users').document(userId).snapshots();

    return userData;
  }

  // 1 writes
  static logout(BuildContext context) async {
    FirebaseUser user = await getCurrentAuthUser();

    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .updateData({'oneSignalId': null});
    FirebaseAuth.instance.signOut();
    Navigator.of(context).pushNamedAndRemoveUntil(
      'start',
      (Route<dynamic> route) => false,
    );
  }

  // 2 writes - 2 reads
  static addFriend(String toUserUid) async {
    FirebaseUser user = await getCurrentAuthUser();
    DocumentSnapshot userData = await getDataUser(user.uid);
    DocumentSnapshot receiverData = await getDataUser(toUserUid);

    Map<String, dynamic> request = {
      'status': 0,
      'created_at': DateTime.now().millisecondsSinceEpoch.toString(),
      'canaccept': false
    };

    // Esta peticion es la que se guarda en el usuario que la ha mandado
    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('friends')
        .document(toUserUid)
        .setData(request);

    // Esta peticion es la que se guarda en el usuario que recibe la solicitud
    request['canaccept'] = true;
    await Firestore.instance
        .collection('users')
        .document(toUserUid)
        .collection('friends')
        .document(user.uid)
        .setData(request);

    Os.sendNotification(userData['name'], prefix0.Notification.FRIEND_REQUEST, [receiverData['oneSignalId']]);
  }

  static deleteFriend(String toUserUid) async {
    FirebaseUser user = await getCurrentAuthUser();
    DocumentSnapshot userData = await getDataUser(user.uid);
    DocumentSnapshot receiverData = await getDataUser(toUserUid);

    Map<String, dynamic> request = {
      'status': 0,
      'created_at': DateTime.now().millisecondsSinceEpoch.toString(),
      'canaccept': false
    };

    // Esta peticion es la que se guarda en el usuario que la ha mandado
    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('friends')
        .document(toUserUid)
        .delete();

    // Esta peticion es la que se guarda en el usuario que recibe la solicitud
    request['canaccept'] = true;
    await Firestore.instance
        .collection('users')
        .document(toUserUid)
        .collection('friends')
        .document(user.uid)
        .delete();

  }

  static acceptFriend(String toUserUID) async {
    FirebaseUser user = await getCurrentAuthUser();
    DocumentSnapshot userData = await getDataUser(user.uid);
    DocumentSnapshot receiverData = await getDataUser(toUserUID);

    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('friends')
        .document(toUserUID)
        .updateData({'status': 1});

    await Firestore.instance
        .collection('users')
        .document(toUserUID)
        .collection('friends')
        .document(user.uid)
        .updateData({'status': 1});

    Os.sendNotification(userData['name'], prefix0.Notification.ACCEPTED_REQUEST, [receiverData['oneSignalId']]);
  }

  static removeFriend(String toUserUID) async {
    FirebaseUser user = await getCurrentAuthUser();

    Firestore.instance
        .collection('users')
        .document(toUserUID)
        .collection('friends')
        .document(user.uid)
        .delete();
    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('friends')
        .document(toUserUID)
        .delete();
  }

  static Future<int> getRequestPendingCount() async {
    int count = 0;
    FirebaseUser user = await getCurrentAuthUser();
    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('friends')
        .where("status", isEqualTo: 0)
        .where('canaccept', isEqualTo: true)
        .getDocuments()
        .then((data) => count = data.documents.length);

    return count;
  }

  static Stream<QuerySnapshot> getRequestPending(String uid) {
    Stream<QuerySnapshot> requests = Firestore.instance
        .collection('users')
        .document(uid)
        .collection('friends')
        .where('status', isEqualTo: 0)
        .where('canaccept', isEqualTo: true)
        .snapshots();

    return requests;
  }

  static getNotSeenMessages() async {
    FirebaseUser currentUser = await getCurrentAuthUser();

    var count = 0;

    await Firestore.instance
        .collection('chats')
        .where('members', arrayContains: currentUser.uid)
        .getDocuments()
        .then((data) {
      for (var i = 0; i < data.documents.length; i++) {
        for (var j = 0; j < data.documents[i].data['members'].length; j++) {
          if (currentUser.uid == data.documents[i].data['members'][j]) {
            count += data.documents[i].data['notseen'][j];
          }
        }
      }
    });

    return count;
  }

  static getNonSeenMessages() async {
    FirebaseUser currentUser = await getCurrentAuthUser();

    return Firestore.instance.collection('chats').where('members', arrayContains: currentUser.uid);
  }

  static setSeen(String chatId, int type) async {
    FirebaseUser currentUser = await getCurrentAuthUser();
    List notseen;

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .get()
        .then((data) {
      notseen = data.data['notseen'];
      for (var i = 0; i < data.data['members'].length; i++) {
        if (currentUser.uid == data.data['members'][i] && type == 1) {
          notseen[i] = 0;
          break;
        } else if (currentUser.uid != data.data['members'][i] && type == 2) {
          notseen[i] += 1;
          break;
        }
      }
    });

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .updateData({'notseen': notseen});
  }

  static setOnline(String chatId, int type) async {
    FirebaseUser currentUser = await getCurrentAuthUser();
    List online;

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .get()
        .then((data) {
      online = data.data['online'];
      for (var i = 0; i < data.data['members'].length; i++) {
        if (currentUser.uid == data.data['members'][i]) {
          switch (type) {
            case 1:
              online[i] = true;
              break;
            case 2:
              online[i] = false;
          }
        }
      }
    });

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .updateData({'online': online});
  }

  static getOnline(String chatId) async {
    FirebaseUser currentUser = await getCurrentAuthUser();
    bool isOnline = false;

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .get()
        .then((data) {
      for (var i = 0; i < data.data['members'].length; i++) {
        if (currentUser.uid != data.data['members'][i]) {
          isOnline = data.data['online'][i];
          break;
        }
      }
    });

    return isOnline;
  }

  static getAllowMessages() async {
    FirebaseUser currentUser = await getCurrentAuthUser();

    bool allowMessages = false;

    await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .get()
        .then((user) {
      allowMessages = user.data['settings']['allowMessages'];
    });

    return allowMessages;
  }

  static setAllowMessages(bool allow) async {
    FirebaseUser currentUser = await getCurrentAuthUser();

    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({
      'settings': {'allowMessages': allow}
    });
  }

  static updateUser(Map<String, dynamic> data) async {
    FirebaseUser currentUser = await getCurrentAuthUser();

    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData(data);
  }

  static getSettings() async {
    DocumentSnapshot currentDataUser = await getDataAuthUser();

    return currentDataUser.data['settings'].cast<String, dynamic>();
  }

  static isBanned(BuildContext context) async {
    logout(context);
    ToastUtil.show("Tu cuenta ha sido baneada.", context);
  }

  static isAdmin() async {
    DocumentSnapshot currentDataUser = await getDataAuthUser();

    if (currentDataUser.data['isAdmin']) {
      return true;
    }
  }

  static setRandom(BuildContext context) async {
    DocumentSnapshot currentDataUser = await getDataAuthUser();

    if(currentDataUser.data['random'] == null) {
      var randomNumber = new Random();
      int random = randomNumber.nextInt(999999);
      Firestore.instance.collection('users').document(currentDataUser.documentID).updateData({'random': random});
    }
  }

  static Future<int> getFriendsCount(String uid) async {
    int count = 0;
    await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('friends')
        .where("status", isEqualTo: 1)
        .getDocuments()
        .then((data) => count = data.documents.length);

    return count;
  }

  static Future<int> getAchievementsCount(String uid) async {
    int count = 0;
    await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('achievements')
        .getDocuments()
        .then((data) => count = data.documents.length);

    return count;
  }

  static Future<int> getGamesCount(String uid) async {
    int count = 0;
    await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('games')
        .getDocuments()
        .then((data) => count = data.documents.length);
    return count;
  }

  static getGamesFromUser(String uid) async {
    QuerySnapshot games = await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('games')
        .getDocuments();

    return games;
  }

  static getGame(String gameId) async {
    DocumentSnapshot game =
        await Firestore.instance.collection('games').document(gameId).get();

    return game;
  }

  static getFriends(String uid) async {
    QuerySnapshot friends = await Firestore.instance.collection('users').document(uid).collection('friends').orderBy('created_at', descending: true).where('status', isEqualTo: 1).getDocuments();

    return friends;
  }

  static setBlock(String uid) async {
    List<dynamic> blockedUsers = new List();
    
    var currentUser = await getDataAuthUser();

    if (currentUser.data['blocked_users'] != null) {
      for(int i = 0; i < currentUser.data['blocked_users'].length; i++) {
        blockedUsers.add(currentUser.data['blocked_users'][i]);
      }
    } else {
      blockedUsers = [];
    }

    blockedUsers.add(uid);


    deleteFriend(uid);
    updateUser({'blocked_users': blockedUsers});
  }

  static setUnblock(String uid) async {
    List<dynamic> blockedUsers = new List();
    
    var currentUser = await getDataAuthUser();

    if (currentUser.data['blocked_users'] != null) {
      for(int i = 0; i < currentUser.data['blocked_users'].length; i++) {
        blockedUsers.add(currentUser.data['blocked_users'][i]);
      }
    }

    blockedUsers.remove(uid);

    updateUser({'blocked_users': blockedUsers});
  }

  static getBlockedUsers() async {
    var currentUser = await getDataAuthUser();

    return currentUser['blocked_users'];
  }

  static changePassword(String password) async {
    var user = await getCurrentAuthUser();

    await user.updatePassword(password);
  } 

  
}
