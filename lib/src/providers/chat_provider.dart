import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gameapp/src/models/Notification.dart' as noti;
import 'package:gameapp/src/pages/user/message/chats/chatscreen_page.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:connectivity/connectivity.dart';

import 'notification_provider.dart';

class ChatProvider {
  static sendMessage(
      String text, String chatId, String uid, String receiverId) async {
    Map<String, dynamic> message;

    DocumentSnapshot userData = await UserProvider.getDataUser(uid);

    if (chatId != null) {
      message = {
        'text': text.trim(),
        'name': userData['name'],
        'senderId': uid,
        'photoUrl': userData['photoUrl'],
        'receiverId': receiverId,
        'created_at': FieldValue.serverTimestamp()
      };

      await Firestore.instance
          .collection('chats')
          .document(chatId)
          .collection('messages')
          .add(message);

      Firestore.instance
          .collection('chats')
          .document(chatId)
          .updateData({'updated_at': message['created_at']});
      Firestore.instance
          .collection('chats')
          .document(chatId)
          .updateData({'last_message': text});
    }
  }

  static sendNotification(String text, String name, String receiverId) async {
    DocumentSnapshot receiverData = await UserProvider.getDataUser(receiverId);

    if (receiverData['oneSignalId'] != null) {
      Os.sendNotification(name, noti.Notification.NEW_MESSAGE, [receiverData['oneSignalId']], text: text);
    }
  }

  static Future<int> getNotSeenFromChatId(String chatId) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    var count = 0;

    await Firestore.instance
        .collection('chats')
        .document(chatId)
        .get()
        .then((chat) {
      for (var i = 0; i < chat.data['members'].length; i++) {
        if (currentUser.uid == chat.data['members'][i]) {
          count = chat.data['notseen'][i];
        }
      }
    });

    return count;
  }

  static Stream<QuerySnapshot> getMessagesFromChatID(String chatId) {
    Stream<QuerySnapshot> messages = Firestore.instance
        .collection('chats')
        .document(chatId)
        .collection('messages')
        .orderBy('created_at', descending: true)
        .limit(20)
        .snapshots();

    return messages;
  }

  static getConversationsFromUserID(String uid) {
    Stream<QuerySnapshot> conversations = Firestore.instance
        .collection('chats')
        .where('members', arrayContains: uid)
        .orderBy('updated_at', descending: true)
        .snapshots();

    return conversations;
  }

  static deleteChat(String chatId) {
    Firestore.instance.collection('chats').document(chatId).delete();
  }

  static getOtherUID(DocumentSnapshot chat, String uid) {
    for (var i = 0; i < chat.data['members'].length; i++) {
      if (chat.data['members'][i] != uid) {
        return chat.data['members'][i];
      }
    }
  }

  static createChatIfNotCreated(
      BuildContext context, DocumentSnapshot user) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    var chat = Firestore.instance.collection('chats').add({
      'members': [currentUser.uid, user.documentID],
      'notseen': [0, 0],
      'online': [true, false]
    });
    chat.then((doc) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatScreen(
                    chatId: doc.documentID,
                    username: user['name'],
                    userId: user.documentID,
                  )));
    });
  }

  static createOrOpenChat(BuildContext context, DocumentSnapshot user) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    bool existChat = false;
    String chatId;
    await Firestore.instance
        .collection('chats')
        .where('members', arrayContains: currentUser.uid)
        .getDocuments()
        .then((data) {
      for (var i = 0; i < data.documents.length; i++) {
        for (var j = 0; j < data.documents[i].data['members'].length; j++) {
          if (data.documents[i].data['members'][j] == user.documentID) {
            existChat = true;
            chatId = data.documents[i].documentID;
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatScreen(
                          chatId: chatId,
                          username: user['name'],
                          userId: user.documentID,
                        )));
            break;
          }
        }
      }

      if (!existChat) {
        createChatIfNotCreated(context, user);
      }
    });
  }

  static checkConnectivity() {
      StreamSubscription subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      // Check the internet connection and then choose the appropriate
      // source for it.
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        await Firestore.instance.settings(source: Source.cache);
      } else {
        await Firestore.instance.settings(source: Source.serverAndCache);
      }
    });

    return subscription;
  }
}
