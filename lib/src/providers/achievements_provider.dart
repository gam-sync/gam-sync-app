import 'package:gameapp/src/providers/user_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AchievementProvider {

  static getAchievementsFromUser(String userId) async {
    return await Firestore.instance.collection('users').document(userId).collection('achievements').getDocuments();
  }
}