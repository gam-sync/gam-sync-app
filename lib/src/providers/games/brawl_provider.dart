import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gameapp/src/models/games/BrawlStars.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class BrawlProvider {

  static String _apiKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkaXNjb3JkX3VzZXJfaWQiOiIxMjk1NTQ2NzA4NzA4NTU2ODAiLCJyZWFzb24iOiI5MTI3Mzg5MGRudTAxYmQxOHUiLCJ2ZXJzaW9uIjoxLCJpYXQiOjE1NjUzODgyNTh9.AOyrEMK_a-A-9H4G-tKb6V6oFLkXYAvwe3SguHUsCUU";


  static getBrawlUser(BuildContext context, String tag) async {
      var url = "https://api.starlist.pro/v1/player?tag=$tag";
      

      Map<String, String> headers = {
        "Authorization": _apiKey
      };

      var res = await http.get(url, headers: headers);
      
      if(res.statusCode == 200) {
        BrawlUser brawlUser = BrawlUser.fromJson(json.decode(res.body));
        if(brawlUser.nameColorCode == "FFFFFF" && brawlUser.avatarId == 28000003) {
          addGameInUser(brawlUser);
          ToastUtil.show("Tu usuario se ha verificado correctamente.", context);
          return true;
        } else {
          ToastUtil.show("Tu usuario no tiene la foto y color asignado.", context);
          return false;
        }
      } else if(res.statusCode == 400) {
        ToastUtil.show("El usuario que has introducido no existe.", context);
        return false;
      } else {
        return false;
      }
    }

  static getDataOfTheUserBrawl() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('brawlstars')
        .get();

    return userGame;
  }

  static addGameInUser(BrawlUser brawluser) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    Map<String, dynamic> data = {
      'dataToShow': [brawluser.tag, brawluser.name],
      'tag': brawluser.tag,
      'username': brawluser.name,
      'avatar': brawluser.avatarUrl,
    };

    List<String> games = [];
    

    for(int i = 0; i < userData['addedGames'].length; i++) {
      games.add(userData['addedGames'][i]);
    }

    games.add('brawlstars');

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});

    Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('brawlstars').setData(data);

    return true;
  }

  static deleteUserBrawl() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
            var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove('brawlstars');

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('brawlstars')
        .delete();
  }

  static checkIfUserHasAccountVerified() async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .collection('games')
        .document('brawlstars')
        .get();

    if (userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static getMineIcon() async {
    DocumentSnapshot brawl = await Firestore.instance.collection('games').document('brawlstars').get();

    return brawl['icon'];
  }
}
