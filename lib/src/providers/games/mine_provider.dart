import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gameapp/src/models/games/Minecraft.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MineProvider {
  
  static getUUIDFromUMine(BuildContext context, String currentUsername) async {
    var url = "https://api.mojang.com/users/profiles/minecraft/$currentUsername";

    var res = await http.get(url);

    if(res.statusCode == 200) {
      UserMine userMine = UserMine.fromJson(json.decode(res.body));
      addGameInUser(userMine);
      return true;
    } else if(res.statusCode == 204) {
      ToastUtil.show("El usuario que has introducido no existe.", context);
      return false;
    }
  }

  static getMineIcon() async {
    DocumentSnapshot mine = await Firestore.instance.collection('games').document('minecraft').get();

    return mine['icon'];
  }

  static getDataOfTheUserMineSnapshot(String uid) {

    Stream<DocumentSnapshot> userGame = Firestore.instance
        .collection('users')
        .document(uid)
        .collection('games')
        .document('minecraft')
        .snapshots();

    return userGame;
  }
  
  static deleteUserMine() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove('minecraft');

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('minecraft')
        .delete();
  }

  static checkIfUserHasAccountVerified() async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('minecraft').get();

    if(userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static addGameInUser(UserMine userMine) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    Map<String, dynamic> data = {
      'dataToShow': [userMine.name],
      'id': userMine.id,
      'username': userMine.name,
      'icon': "https://minotar.net/avatar/" + userMine.id
    };

        
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.add('minecraft');

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});
    
    await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('minecraft').setData(data);

    return true;
  }

  static getDataOfTheUserMine() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('minecraft')
        .get();

    return userGame;
  }
}