import 'dart:convert';
import 'dart:convert' as prefix0;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gameapp/src/models/games/LeagueOfLegends.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';

class LolProvider {

  static String _apikey = "RGAPI-97210322-eabe-445b-ad5f-da2a414cea8a";
  
  static getServerDocuments() async {
    QuerySnapshot servers = await Firestore.instance
        .collection('games')
        .document('lol')
        .collection('servers')
        .getDocuments();

    return servers;
  }

  static checkUsernameExists(String serverId, String username, String serverName, String lane, BuildContext context) async {
    var url =
        "https://$serverId.api.riotgames.com/lol/summoner/v4/summoners/by-name/$username";

    Map<String, String> headers = {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": _apikey,
      "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
      "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
    };

    var res = await http.get(url, headers: headers);

    if (res.statusCode == 200) {
      ProfileLoL profileLoL = ProfileLoL.fromJson(json.decode(res.body));
      if(profileLoL.profileIconId == 0) {
        await addGameInUser(profileLoL, serverId, serverName, lane, context);
        await ToastUtil.show("Tu usuario se ha verificado correctamente.", context);
        return true;
      } else {
        ToastUtil.show("Tu usuario no tiene la foto asignada.", context);
        return false;
      }
    } else if(res.statusCode == 404) {
      ToastUtil.show("No se han encontrado datos de $username. Comprueba si has puesto tu usuario correctamente.", context);
      return false;
    } else if(res.statusCode == 403) {
      ToastUtil.show("La API de Riot esta teniendo problemas actualmente. Prueba de nuevo mas tarde.", context);
      return false;
    } else {
      ToastUtil.show("Ha ocurrido un error desconocido. Contacta con un administrador si el problema persiste", context);
    }
  }

  static checkIfUserHasAccountVerified() async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('lol').get();

    if(userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static getLoLIcon() async {
    DocumentSnapshot lol = await Firestore.instance.collection('games').document('lol').get();

    return lol['icon'];
  }

  static addGameInUser(ProfileLoL profileLoL,  String serverId, String serverName, String lane, BuildContext context) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    List<dynamic> queues = await getRanks(serverId, profileLoL.id);
    var version = await getLastVersion(context);
    
    var now = DateTime.now().add(Duration(days: 1));

    List<Map<String, dynamic>> mapqueues = new List();
    queues.forEach((queue) {
      mapqueues.add(queue.toJson());
    });

    Map<String, dynamic> data = {
      'profile': {
        'id': profileLoL.id,
        'name': profileLoL.name,
        'profileIconId': profileLoL.profileIconId,
        'puuid': profileLoL.puuid,
        'revisionDate': profileLoL.revisionDate,
        'summonerLevel': profileLoL.summonerLevel,
        'accountId': profileLoL.accountId
      },
      'dataToShow': [profileLoL.name, serverName],
      'queues': mapqueues,
      'server' : {
        'id': serverId,
        'name': serverName
      },
      'lane': lane,
      'icon': "http://ddragon.leagueoflegends.com/cdn/$version/img/profileicon/${profileLoL.profileIconId}.png",
      'nextRefresh': now
    };

    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.add('lol');

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});

    await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('lol').setData(data);

    return true;
  }

  static refreshData(BuildContext context) async {
    
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    ProfileLoL profileLoL = await getProfileFromAPI(uid: currentUser.uid);
    DocumentSnapshot profileData = await getDataOfTheUserLoL(currentUser.uid);
    var version = await getLastVersion(context);

    if(profileLoL != null) {
      var iconId = profileLoL.profileIconId;

    
      Map<String, dynamic> data = profileData.data;
      data['icon'] = "http://ddragon.leagueoflegends.com/cdn/$version/img/profileicon/$iconId.png";
      data['profile']['profileIconId'] = profileLoL.profileIconId;
      data['profile']['summonerLevel'] = profileLoL.summonerLevel;
      data['nextRefresh'] = DateTime.now().add(Duration(days: 1));


      Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('lol').setData(data);
      var nextRefresh = DateFormat('dd MMM kk:mm').format(DateTime.now().add(Duration(days: 1)));
      ToastUtil.show("Se ha actualizado correctamente al usuario. Proxima actualizacion manual: " + nextRefresh, context);
      return data['icon'];
    } else {
      ToastUtil.show("No se ha podido actualizar el usuario.", context);
    }
  }

  static getLastVersion(BuildContext context) async {
    var url = "https://ddragon.leagueoflegends.com/api/versions.json";

    var res = await http.get(url);

    if(res.statusCode == 200) {
      return json.decode(res.body)[0];
    } else {
      ToastUtil.show("Ha ocurrido un error al sacar la ultima version de League of Legends.", context);
    }
  }

  static getProfileFromAPI({BuildContext context, String uid}) async {
    

    var userGame = await getDataOfTheUserLoL(uid);

    var encryptedAccountId = userGame.data['profile']['accountId'];
    var username = userGame.data['profile']['name'];
    var server = userGame.data['server']['id'];

    var url =
        "https://$server.api.riotgames.com/lol/summoner/v4/summoners/by-account/$encryptedAccountId";

    await Firestore.instance.collection('games').document('lol').get().then((lol) {
      _apikey = lol.data['apiKey'];
    });



    Map<String, String> headers = {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": _apikey,
      "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
      "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
    };

    var res = await http.get(url, headers: headers);

    if (res.statusCode == 200) {
      return ProfileLoL.fromJson(json.decode(res.body));
    } else if(res.statusCode == 403) {
      ToastUtil.show("La API de Riot esta teniendo problemas actualmente. Prueba de nuevo mas tarde.", context);
    } else if(res.statusCode == 404) {
      ToastUtil.show("No se han encontrado datos de $username", context);
    } else {
      ToastUtil.show("Ha ocurrido un error desconocido. Contacta con un administrador si el problema persiste", context);
    }
  }

  

  static getDataOfTheUserLoL(String uid) async {

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('games')
        .document('lol')
        .get();
    

    return userGame;
  }

  static deleteUserLoL() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove('lol');

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('lol')
        .delete();
  }

  static getRanks(String serverId, String summonerId) async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();


    var url =
        "https://$serverId.api.riotgames.com/lol/league/v4/entries/by-summoner/$summonerId";

    
    Map<String, String> headers = {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": _apikey,
      "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
      "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
    };

    var res = await http.get(url, headers: headers);

    if(res.statusCode == 200) {
      var json = prefix0.json.decode(res.body);
      var queues = json.map((i) => QueueLoL.fromJson(i)).toList();
      return queues;
    }

  }

  static getMatches(String serverId, String accountId) async {

    var url =
        "https://$serverId.api.riotgames.com/lol/match/v4/matchlists/by-account/$accountId";

    

    Map<String, String> headers = {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": _apikey,
      "Accept-Language": "es-ES,es;q=0.9,en;q=0.8",
      "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
    };

    var res = await http.get(url, headers: headers);

    if(res.statusCode == 200) {
      MatchList matchList = MatchList.fromJson(json.decode(res.body));
      return matchList;
    }

  }
}
