import 'package:flutter/widgets.dart';
import 'package:gameapp/src/models/games/Overwatch.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

class OverwatchProvider {

  static getProfileFromBattle(BuildContext context, String battleTag, String platform) async {

    String battleTagReplaced = battleTag.replaceAll(new RegExp('#'), '-');

    String url = "https://ow-api.com/v1/stats/pc/eu/$battleTagReplaced/profile";

    var res = await http.get(url);

    if (res.statusCode == 200) {
      OverwatchUser overwatchUser = OverwatchUser.fromJson(json.decode(res.body));
      addGameInUser(overwatchUser);
      return true;
    } else {
      ToastUtil.show('Ha ocurrido un error a la hora de verificar tu usuario.', context);
    }
  }

    static addGameInUser(OverwatchUser overwatchUser) async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    Map<String, dynamic> data = {
      'dataToShow': [overwatchUser.name],
      'username': overwatchUser.name,
      'icon': overwatchUser.icon,
      'level': overwatchUser.level,
      'gamesWon': overwatchUser.gamesWon,
      'prestige': overwatchUser.prestige
    };

    List<String> games = [];

    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }

    games.add('overwatch');

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});

    Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('overwatch').setData(data);

    return true;
  }

  static checkIfUserHasAccountVerified() async {
    var currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .collection('games')
        .document('overwatch')
        .get();

    if (userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static getIcon() async {
    DocumentSnapshot overwatch = await Firestore.instance.collection('games').document('overwatch').get();

    return overwatch['icon'];
  }

  static getDataOfTheUserOverwatch() async {
    var user = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('overwatch')
        .get();

    return userGame;
  }

  static deleteUserOverwatch() async {
    var user = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove('overwatch');

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('overwatch')
        .delete();
  }
}
