import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/providers/user_provider.dart';

class PokegoProvider {

  static addGame(String codeTrainer, String team) async {

    var currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    Map<String, dynamic> data = {
      'dataToShow': [codeTrainer, team],
      'codeTrainer': codeTrainer,
      'team': team
    };

    List<String> games = [];

    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }

    games.add('pokego');

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});

    await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('pokego').setData(data);

    return true;
  }
  
  static getDataOfTheUserPokeGo(String uid) async {

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('games')
        .document('pokego')
        .get();
    
    return userGame;
  }

  static checkIfUserHasAccountVerified() async {
    var currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document('pokego').get();

    if(userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static deleteUserPokeGo() async {
    var user = await UserProvider.getCurrentAuthUser();

        var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove('pokego');

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document('pokego')
        .delete();
  }
}