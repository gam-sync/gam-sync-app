import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/models/Notification.dart';
import 'package:gameapp/src/providers/notification_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';

class RoomProvider {
  static Stream<QuerySnapshot> getRooms() {
    Stream<QuerySnapshot> rooms =
        Firestore.instance.collection('rooms').snapshots();

    return rooms;
  }

  static sendMessage(String text, String roomId, String uid, String chatRoom) async {
    Map<String, dynamic> message;

    DocumentSnapshot userData = await UserProvider.getDataUser(uid);
    DocumentSnapshot room = await Firestore.instance.collection('rooms').document(roomId).get();

    message = {
      'text': text,
      'userId': uid,
      'name': userData['name'],
      'photoUrl': userData['photoUrl'],
      'isAdmin': userData['isAdmin'],
      'created_at': FieldValue.serverTimestamp(),
    };
    
    if(text.contains("@")) {
      List<String> words = text.split(" ");
      List<String> users = new List();
      List<String> playerList = new List();

      for(int i = 0; i < words.length; i++) {
        if (words[i].contains("@")) {
          users.add(words[i].substring(1));
        }
      }

      users = LinkedHashSet<String>.from(users).toList();
      for(int i = 0; i < users.length; i++) {
        var user = await Firestore.instance.collection('users').where('name', isEqualTo: users[i]).getDocuments();
        if (user.documents.length != 0) {
          if (user.documents[0].data['settings']['allowMentions'] != null &&user.documents[0].data['settings']['allowMentions']) {
            playerList.add(user.documents[0].data['oneSignalId']);
          } 
        }
        
      }

      Os.sendNotification(userData['name'], Notification.MENTION_CHATROOM, playerList, chatRoom: chatRoom, text: text);
    }

    List<String> playerIds = List();
    List<String> usersOnline = room.data['usersOnline'].cast<String>().toList();

    room.data['usersNotifications'].forEach((k, v) => !usersOnline.contains(k) ? playerIds.add(v) : null);
    playerIds.remove(userData.data['oneSignalId']);

    Firestore.instance
        .collection('rooms')
        .document(roomId)
        .collection('messages')
        .add(message);

    if (playerIds.length != 0) {
      await Os.sendNotification(userData['name'], Notification.MESSAGE_CHATROOM, playerIds, chatRoom: chatRoom, text: text);
    }
    
  }

  static Stream<QuerySnapshot> getMessagesFromRoomID(String roomId) {
    var messages = Firestore.instance
        .collection('rooms')
        .document(roomId)
        .collection('messages')
        .limit(50)
        .orderBy('created_at', descending: true)
        .snapshots();
    
    return messages;
  }

  static Future<bool> checkIfIsActive(String roomId) async {
    var currentUser = await UserProvider.getDataAuthUser();

    var room = await Firestore.instance.collection('rooms').document(roomId).get();

    if (room['usersNotifications'].containsKey(currentUser.documentID)) return true;
    else return false;
  }

  static Future<bool> addUserToArrayNotifications(String roomId) async {
    var currentUser = await UserProvider.getDataAuthUser();

    var room = await Firestore.instance.collection('rooms').document(roomId).get();

    Map<String, String> usersNotifications = Map.from(room.data['usersNotifications']);


    usersNotifications.addAll({currentUser.documentID: currentUser.data['oneSignalId']});

    Firestore.instance.collection('rooms').document(roomId).updateData({'usersNotifications': usersNotifications});

    return true;
  }

  static Future<bool> removeUserToArrayNotifications(String roomId) async {
    var currentUser = await UserProvider.getDataAuthUser();

    var room = await Firestore.instance.collection('rooms').document(roomId).get();

    Map<String, String> usersNotifications = Map.from(room.data['usersNotifications']);

    usersNotifications.remove(currentUser.documentID);

    Firestore.instance.collection('rooms').document(roomId).updateData({'usersNotifications': usersNotifications});

    return false;
  }

  static setOnline(String roomId) async {
    DocumentSnapshot room = await Firestore.instance.collection('rooms').document(roomId).get();
    DocumentSnapshot currentUser = await UserProvider.getDataAuthUser();

    List<String> userOnline = room.data['usersOnline'].cast<String>().toList();

    userOnline.add(currentUser.documentID);

    Firestore.instance.collection('rooms').document(roomId).updateData({'usersOnline': userOnline});
  } 

  static unsetOnline(String roomId) async {
    DocumentSnapshot room = await Firestore.instance.collection('rooms').document(roomId).get();
    DocumentSnapshot currentUser = await UserProvider.getDataAuthUser();

    List<String> userOnline = room.data['usersOnline'].cast<String>().toList();

    userOnline.remove(currentUser.documentID);

    Firestore.instance.collection('rooms').document(roomId).updateData({'usersOnline': userOnline});
  } 
}
