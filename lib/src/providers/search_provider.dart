import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gameapp/src/providers/user_provider.dart';

class SearchProvider {
  static checkSeacherSlider() async {
    bool check;
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    await Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .get()
        .then((data) {
      check = data['searcherSlider'];
    });

    return check;
  }

  static setSeenSlider() async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    Firestore.instance
        .collection('users')
        .document(currentUser.uid)
        .updateData({'searcherSlider': true});
  }
}
