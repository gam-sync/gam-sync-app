import 'package:cloud_firestore/cloud_firestore.dart';

class ActivityProvider {
  static addActivity(String text, String userId) async {
    Firestore.instance
        .collection('users')
        .document(userId)
        .collection('activity')
        .add({'text': text});
  }

  static Stream<QuerySnapshot> getActivity(String userId) {
    Stream<QuerySnapshot> activity = Firestore.instance
        .collection('users')
        .document(userId)
        .collection('activity')
        .snapshots();

    return activity;
  }
}
