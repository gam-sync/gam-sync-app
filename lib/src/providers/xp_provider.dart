import 'dart:math' as Math;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/providers/user_provider.dart';

class XProvider {

  static calculateLevel(int xp) async {
    var settings = await Firestore.instance.collection('settings').document('level').get();
    int points = 0;
    for (int level = 0; level < 999; level++) {
      int diff = (level + settings.data['sum'] * Math.pow(2, level.toDouble() / 100)).toInt();
      points += diff;
      if (xp >= points / settings.data['divider']) {
        level = level;
      } else {
        return (level + 1).toString();
      }
    }
  }

  static getXP(String uid) async {
    var user = await UserProvider.getDataUser(uid);

    return user.data['xp'];
  }
}