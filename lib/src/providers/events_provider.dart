import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gameapp/src/models/Notification.dart' as prefix0;
import 'package:gameapp/src/pages/user/events/event_page.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/notification_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:sentry/sentry.dart';
import 'package:timeago/timeago.dart' as timeago;

class EventsProvider {
  static getEvents() async {
    var events = await Firestore.instance
        .collection('events')
        .where('status', isEqualTo: true)
        .orderBy('created_at', descending: true)
        .getDocuments();

    return events;
  }

  static createEvent(event) async {
    Firestore.instance.collection('events').add(event);
  }

  static getComments(eventId) {
    return Firestore.instance.collection('events').document(eventId).collection('comments').orderBy('created_at', descending: true).snapshots();
  }

  static addComent(String text, String tablonId, {String userTablonId}) async {
    var currentUser = await UserProvider.getDataAuthUser();
    var userTablon = await UserProvider.getDataUser(userTablonId);

    Map<String, dynamic> data = {
      'text': text,
      'created_at': FieldValue.serverTimestamp(),
      'user': {
        'name': currentUser.data['name'],
        'uid': currentUser.documentID,
        'photoUrl': currentUser.data['photoUrl'],
      }
    };

    if (currentUser.documentID != userTablon.documentID) {
      Os.sendNotification(currentUser.data['name'], prefix0.Notification.NEW_COMMENT, [userTablon.data['oneSignalId']], text: text);
    }
    

    Firestore.instance.collection('events').document(tablonId).collection('comments').document().setData(data);
  }

  static getEvent(eventId) async {
    return await Firestore.instance.collection('events').document(eventId).get();
  }

  static buildEventGame(BuildContext context, event) {
    var date =
        DateTime.fromMillisecondsSinceEpoch(event['created_at'].seconds * 1000);

    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.transparent,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.black26,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0))),
              child: Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: ListTile(
                  onTap: () {},
                  isThreeLine: true,
                  title: Text(event['user']['name']),
                  subtitle: Text(timeago.format(date, locale: 'es') +
                      "\n\n${event['description']}"),
                  leading: GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => ProfilePage(
                                  uid: event['user']['uid'],
                                  username: event['user']['name'],
                                ))),
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage: CachedNetworkImageProvider(event['user']['photoUrl'])),
                  ),
                  trailing: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          backgroundImage: event['gameAsset'] != null ? AssetImage(event['gameAsset']) : CachedNetworkImageProvider(event['gameIcon']),
                        ),
                ),
              ),
            ),
          ),
          event['enableComment'] != null && event['enableComment']
              ? Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  height: 45,
                  decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8.0),
                          bottomRight: Radius.circular(8.0))),
                  child: Column(
                    children: <Widget>[
                      Divider(
                        height: 1,
                      ),
                      GestureDetector(
                        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EventPage(tablonId: event.documentID, user: event['user']['name'], userId: event['user']['uid'],))),
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.insert_comment),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 3.0),
                                child: Text('Comentar'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8.0),
                          bottomRight: Radius.circular(8.0))),
              )
        ],
      ),
    );
  }
}
