import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/default_page.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:firebase_auth/firebase_auth.dart';


class GameProvider {
  static Widget getGameWidget(DocumentSnapshot game, DocumentSnapshot userGame,
      BuildContext context, String uid) {
    
    String subtitle = "";

    for (int i = 0; i < userGame['dataToShow'].length; i++) {
      if (i < userGame['dataToShow'].length - 1) {
        subtitle += userGame['dataToShow'][i] + " - ";
      } else {
        subtitle += userGame['dataToShow'][i];
      }
    }

    return Column(
      children: <Widget>[
        Material(
          type: MaterialType.transparency,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.transparent,
              backgroundImage: game['asset'] != null
                  ? AssetImage(game['asset'])
                  : CachedNetworkImageProvider(game['icon']),
            ),
            title: Text(game['name']),
            subtitle: Text(subtitle),
            /*trailing: Icon(
              Icons.arrow_forward_ios,
              size: 16,
            ), */
            onTap: () {
              
            },
          ),
        ),
        Divider(height: 1,),
      ],
    );
  }

  static sendToGame(String gameRoute, String gameId, BuildContext context, {String gameName}) {
    if (gameRoute != null) {
      Navigator.pushNamed(context, gameRoute);
    } else {
      Navigator.push(context, new MaterialPageRoute(builder: (context) => DefaultAddPage(gameId: gameId, gameName: gameName,)));
    }
  }

  static getAllGames() {
    Stream<QuerySnapshot> games = Firestore.instance
        .collection('games')
        .where('status', isEqualTo: true)
        .snapshots();

    return games;
  }

  static getAllGamesDocuments() async {
    var games = await Firestore.instance
        .collection('games')
        .where('status', isEqualTo: true)
        .getDocuments();

    return games;
  }

  static getGamesUser() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    var games = [];

    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .getDocuments()
        .then((game) {
      for (int i = 0; i < game.documents.length; i++) {
        games[i] = game.documents[i].data;
      }
    });

    return games;
  }

  static getGame(String gameId) async {
    return await Firestore.instance.collection('games').document(gameId).get();
  }

  static checkIfUserHasAccountVerified(String gameId) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance.collection('users').document(currentUser.uid).collection('games').document(gameId).get();

    if(userGame.exists) {
      return true;
    } else {
      return false;
    }
  }

  static getIconGame(String gameId) async {
    DocumentSnapshot game = await Firestore.instance.collection('games').document(gameId).get();

    return game['icon'];
  }

  static getDataOfTheUserGame(String gameId) async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();

    DocumentSnapshot userGame = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document(gameId)
        .get();

    return userGame;
  }

  static deleteUserGame(String gameId) async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();
    
    List<String> games = [];


    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }
    

    games.remove(gameId);

    Firestore.instance.collection('users').document(user.uid).updateData({'addedGames': games});

    Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('games')
        .document(gameId)
        .delete();
  }

  static addGameInUser(String username, String gameId) async {
    FirebaseUser currentUser = await UserProvider.getCurrentAuthUser();
    var userData = await UserProvider.getDataAuthUser();

    Map<String, dynamic> data = {
      'dataToShow': [username],
    };

    List<String> games = [];

    if (userData['addedGames'] != null) {
      for(int i = 0; i < userData['addedGames'].length; i++) {
            games.add(userData['addedGames'][i]);
      }
    }

    games.add(gameId);

    Firestore.instance.collection('users').document(currentUser.uid).updateData({'addedGames': games});

    Firestore.instance.collection('users').document(currentUser.uid).collection('games').document(gameId).setData(data);

    return true;
  }

}
