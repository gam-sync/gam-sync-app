import 'package:gameapp/src/models/Notification.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Os {
  static sendNotification(String username, Notification type, List<String> playerIds,
      {String text, String game, String gameId, String userId, String chatRoom}) async {
    var notification = OSCreateNotification(
        playerIds: playerIds,
        content: null,
        languageCode: 'es',
        contentAvailable: true,
        mutableContent: true);

    switch (type) {
      case Notification.FRIEND_REQUEST:
        notification.content =
            "$username te ha mandado una solicitud de amistad.";
        notification.heading = "¡Nueva solicitud de amistad!";
        notification.subtitle =
            "$username te ha mandado una solicitud de amistad.";
        notification.collapseId = "1";
        break;
      case Notification.NEW_MESSAGE:
        notification.content = text;
        notification.heading = "$username te ha mandado un mensaje.";
        notification.subtitle = text;
        notification.collapseId = "2";
        break;
      case Notification.ACCEPTED_REQUEST:
        notification.content = "$username ha aceptado tu solicitud de amistad.";
        notification.heading = "Tu solicitud de amistad ha sido aceptada.";
        notification.subtitle =
            "$username ha aceptado tu solicitud de amistad.";
        notification.collapseId = "3";
        notification.buttons = [];
        break;
      case Notification.ADDED_GAME:
        playerIds = await getPlayerIdsFriends(userId, gameId);
        notification.content = "$username ha añadido $game en su cuenta. ¿Porque no juegas con el?";
        notification.heading = "¿Porque no juegas con el?";
        notification.subtitle = "$username ha añadido $game en su cuenta. ¿Porque no juegas con el?";
        notification.collapseId = "4";
        notification.playerIds = playerIds;
        break;
      case Notification.MENTION_CHATROOM:
        notification.content = text;
        notification.heading = "$username te ha mencionado en $chatRoom.";
        notification.subtitle =
            text;
        notification.collapseId = "4";
        notification.buttons = [];
        notification.additionalData = {
          
        };
        break;
      case Notification.EVENT_ACCEPT:
        break;
      case Notification.MESSAGE_CHATROOM:
        notification.content = "$username: $text";
        notification.heading = "Nuevo mensaje en $chatRoom";
        notification.subtitle = "$username: $text";
        notification.collapseId = "5";
        notification.playerIds = playerIds;
        break;
      case Notification.NEW_COMMENT:
        notification.content = "$username: $text";
        notification.heading = "Un usuario ha comentado tu tablón";
        notification.subtitle = "$username: $text";
        notification.collapseId = "6";
        notification.playerIds = playerIds;

    }

    OneSignal.shared.postNotification(notification);
  }

  static getPlayerIdsFriends(String userId, String game) async {
    List<String> playerIds = new List();
    await Firestore.instance.collection('users').document(userId).collection('friends').getDocuments().then((friends) async {
      for(var i = 0; i < friends.documents.length; i++) {
        await Firestore.instance.collection('users').document(friends.documents[i].documentID).get().then((friend) {
          if (friend.data['oneSignalId'] != null) {
            playerIds.add(friend.data['oneSignalId'].toString());
          }
          
        });
      }
    });
    return playerIds;
  }
}
