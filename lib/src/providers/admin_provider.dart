import 'package:cloud_firestore/cloud_firestore.dart';

class AdminProvider {

  static getUsersCount() async {
    String countUsers = "0";


    await Firestore.instance.collection('users').getDocuments().then((users) {
      countUsers = users.documents.length.toString();
    });

    return countUsers;
  } 

  static getEventsCount() async {
    String countEvents = "0";

    await Firestore.instance.collection('events').getDocuments().then((events) {
      countEvents = events.documents.length.toString();
    });

    return countEvents;
  } 

  static getGamesCount() async {
    String countGames = "0";

    await Firestore.instance.collection('games').getDocuments().then((games) {
      countGames = games.documents.length.toString();
    });

    return countGames;
  }

  static setBan(String userId) {
    Firestore.instance.collection('users').document(userId).updateData({'banned': true});
  }

  static setUnban(String userId) {
    Firestore.instance.collection('users').document(userId).updateData({'banned': false});
  }

  static setAdmin(String userId) {
    Firestore.instance.collection('users').document(userId).updateData({'isAdmin': true});
  }

  static setUnadmin(String userId) {
    Firestore.instance.collection('users').document(userId).updateData({'isAdmin': false});
  }

  static acceptEvent(String eventId) {
    Firestore.instance.collection('events').document(eventId).updateData({'status': true});
  }

  static declineEvent(String eventId) {
    Firestore.instance.collection('events').document(eventId).delete();
  }
}