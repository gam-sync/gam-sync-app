import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class MatchProvider {
  static getMatch(String game, BuildContext context) async {
    DocumentSnapshot userData = await UserProvider.getDataAuthUser();
    var randomNumber = new Random();
    int random = randomNumber.nextInt(999999);
    List<List<DocumentSnapshot>> listUser = new List();

    var users = await Firestore.instance
        .collection('users')
        .where('addedGames', arrayContains: game)
        .getDocuments();

    for (int i = 0; i < users.documents.length; i++) {
      await Firestore.instance
          .collection('users')
          .document(users.documents[i].documentID)
          .collection('games')
          .document(game)
          .get()
          .then((doc) async {
        await Firestore.instance
            .collection('users')
            .document(users.documents[i].documentID)
            .collection('friends')
            .document(userData.documentID)
            .get()
            .then((friend) {
          if (doc.exists &&
              !friend.exists &&
              userData.documentID != users.documents[i].documentID)
            listUser.add([users.documents[i], doc]);
        });
      });
    }

    if (listUser.length == 0) {
      ToastUtil.show(
          "No se han encontrado usuarios. Vuelve a probar mas tarde.", context);
    }

    return listUser;
  }

  static buildMatchPage(DocumentSnapshot game, BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 5,
      width: MediaQuery.of(context).size.width,
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            image: game['match_asset'] != null
                ? AssetImage(game['match_asset']) :
                game['match_network'] != null ? CachedNetworkImageProvider(game['match_network'])
                : CachedNetworkImageProvider(game['icon']),
            fit: BoxFit.cover,
            alignment: Alignment.center,
          )),
          child: Container(
            padding: EdgeInsets.all(16),
            alignment: Alignment.topLeft,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              decoration: BoxDecoration(
                  color: HexColor('#100c08'),
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              child: Text(
                game['name'],
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
      ),
    );
  }

  static Widget buildUserGame(
      String gameId, String uid, DocumentSnapshot userGame) {
    switch (gameId) {
      case "lol":
        String division;

        if (userGame.data['queues'] != null) {
          for (int i = 0; i < userGame.data['queues'].length; i++) {
            if (userGame.data['queues'][i]['queueType'] == "RANKED_SOLO_5x5") {
              division = userGame.data['queues'][i]['tier'] +
                  " " +
                  userGame.data['queues'][i]['rank'];
            }
          }
        }
        if (division == null) division = "Unranked";
        return Column(
          children: <Widget>[
            Container(
              color: Colors.black12,
              child: ListTile(
                title: Text(
                    '${userGame.data['profile']['name']} - (${userGame.data['server']['name']})'),
                subtitle: Text("Nivel " +
                    userGame.data['profile']['summonerLevel'].toString()),
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/games/lol.png'),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(division),
                    Text(' - ${userGame.data['lane']}')
                  ],
                ),
              ),
            ),
            Divider(
              height: 1,
              color: Colors.white10,
            )
          ],
        );
        break;
      case "minecraft":
        return Column(
          children: <Widget>[
            Container(
              color: Colors.black12,
              child: ListTile(
                title: Text('Usuario: '),
                subtitle: Text('${userGame.data['username']}'),
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: CachedNetworkImageProvider(userGame.data['icon']),
                ),
                trailing: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/games/mine.png'),
                ),
              ),
            ),
            Divider(
              height: 1,
              color: Colors.white10,
            )
          ],
        );
      case "brawlstars":
        return Column(
          children: <Widget>[
            Container(
              color: Colors.black12,
              child: ListTile(
                subtitle: Text('#${userGame.data['tag']}'),
                title: Text('${userGame.data['username']}'),
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: CachedNetworkImageProvider(userGame.data['avatar']),
                ),
                trailing: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage('assets/games/brawlstars.png'),
                ),
              ),
            ),
            Divider(
              height: 1,
              color: Colors.white10,
            )
          ],
        );
      default:
        return Column(
          children: <Widget>[
            Container(
              color: Colors.black12,
              child: ListTile(
                subtitle: Text('${userGame.data['dataToShow'][0]}'),
                title: Text('Nombre de usuario:'),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0, left: 25.0, right: 10.0),
              child: Text(
                'Este juego aún no ha sido implementado de forma definitiva pero te damos la opcion de añadirlo. Es posible que puedan ocurrir errores.',
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
    }
  }
}
