// Flutter
import 'package:flutter/material.dart';

// Providers
import 'package:gameapp/src/providers/user_provider.dart';

// Utils
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ScreenPage extends StatefulWidget {
  ScreenPage({Key key}) : super(key: key);

  _ScreenPageState createState() => _ScreenPageState();
}

class _ScreenPageState extends State<ScreenPage> {
  @override
  void initState() {
    super.initState();
    UserProvider.checkToken(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: SpinKitDoubleBounce(
        color: Theme.of(context).accentColor,
        size: 50.0,
      )),
      color: Theme.of(context).primaryColor,
    );
  }
}
