import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gameapp/src/models/Country.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class RegisterPageTwo extends StatefulWidget {
  RegisterPageTwo({Key key}) : super(key: key);

  @override
  _RegisterPageTwoState createState() => _RegisterPageTwoState();
}

class _RegisterPageTwoState extends State<RegisterPageTwo> {
  // Fields for registration
  String _username;
  String _email;
  String _password;
  String _genre;
  File _image;
  String _country;
  String _city;
  String _bio;

  // currentCountry: To know if a country has been chosen and to show the cities
  Country currentCountry;

  // buildCountryList: Create the drop-down to choose the countries
  _buildCountryList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
              height: ScreenUtil.instance.setHeight(300.0),
              color: HexColor('23395d'),
              child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('assets/countries.json'),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Container();
                  List<Country> countries = parseJson(snapshot.data.toString());
                  if (countries == null) {
                    return Container();
                  }

                  return ListView.builder(
                    itemCount: countries.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(countries[index].countryName),
                        onTap: () {
                          setState(() {
                            _country = countries[index].countryName;
                            currentCountry = countries[index];
                          });
                          Navigator.pop(context);
                        },
                      );
                    },
                  );
                },
              ));
        });
  }

  // buildCityList: Create the drop-down to choose the cities
  _buildCityList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
              height: ScreenUtil.instance.setHeight(300.0),
              color: HexColor('23395d'),
              child: ListView.builder(
                itemCount: currentCountry.regions.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(currentCountry.regions[index].name),
                    onTap: () {
                      setState(() {
                        _city = currentCountry.regions[index].name;
                      });
                      Navigator.pop(context);
                    },
                  );
                },
              ));
        });
  }

  // buildGenreList: Create the drop-down to choose the genres
  _buildGenreList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: ListView(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 20.0, left: 10.0),
                        child: Text(
                          'Genero',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Column(children: [
                        ListTile(
                          title: Text('Hombre'),
                          onTap: () {
                            setState(() {
                              _genre = "Hombre";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                        Divider(
                          height: 1,
                          color: Colors.white10,
                        ),
                        ListTile(
                          title: Text('Mujer'),
                          onTap: () {
                            setState(() {
                              _genre = "Mujer";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                        ListTile(
                          title: Text('Otro'),
                          onTap: () {
                            setState(() {
                              _genre = "Otro";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                      ])
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  // parseJson: Map the list of countries to display them in a drop-down
  List<Country> parseJson(String response) {
    if (response == null) {
      return [];
    }
    final parsed =
        json.decode(response.toString()).cast<Map<String, dynamic>>();
    return parsed.map<Country>((json) => new Country.fromJson(json)).toList();
  }

  // Keys of the forms to know with what type of form I am validating
  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form1 = formKey.currentState;
    if (form1.validate()) {
      form1.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {

      // User Data
      Map<String, dynamic> userToRegister = {
        'name': _username,
        'city': _city,
        'country': _country,
        'bio': _bio,
        'genre': _genre,
      };

      // Register the user and if there is a possible error it shows it on the screen.
      await UserProvider.register(
          userToRegister, _email, _password, context, _image);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Registrate en Gam Sync',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: ListView(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil.instance.width / 24,
                    vertical: ScreenUtil.instance.height / 100),
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 12.0, bottom: 1.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Con correo electronico',
                              style: TextStyle(
                                fontSize: ScreenUtil.instance.setSp(11.0),
                              )),
                        ),
                      ),
                      Form(
                        key: formKey,
                        child: Column(
                          children: <Widget>[
                            Divider(
                              height: 30,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TextFormField(
                                  keyboardType: TextInputType.text,
                                  validator: (value) => value.isEmpty
                                      ? 'El usuario no puede estar vacio'
                                      : value.contains(' ')
                                          ? 'El usuario no puede tener espacios'
                                          : null,
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(color: Colors.red),
                                    filled: true,
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 10.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    fillColor: Colors.black38,
                                    hintText: 'Nombre de usuario',
                                  ),
                                  onSaved: (value) => _username = value),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TextFormField(
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) => value.isEmpty
                                      ? 'El email no puede estar vacio'
                                      : null,
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(color: Colors.red),
                                    filled: true,
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 10.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    fillColor: Colors.black38,
                                    hintText: 'Correo electronico',
                                  ),
                                  onSaved: (value) => _email = value),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 20.0),
                              child: TextFormField(
                                  obscureText: true,
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'La contraseña no puede estar vacia';
                                    }
                                    if (value.length < 6) {
                                      return 'La contraseña tiene que tener mas de 6 caracteres';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(color: Colors.red),
                                    filled: true,
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 10.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    fillColor: Colors.black38,
                                    hintText: 'Contraseña',
                                  ),
                                  onSaved: (value) => _password = value),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TextFormField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 5,
                                  validator: (value) => value.isEmpty
                                      ? 'La biografía no puede estar vacia'
                                      : null,
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(color: Colors.red),
                                    filled: true,
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: 10.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0),
                                    ),
                                    fillColor: Colors.black38,
                                    hintText: 'Biografía',
                                  ),
                                  onSaved: (value) => _bio = value),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TextFormField(
                                keyboardType: null,
                                validator: (value) => _genre == null
                                    ? 'El genero no puede estar vacio'
                                    : null,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText:
                                      _genre == null ? 'Genero' : _genre,
                                ),
                                onTap: () => _buildGenreList(),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TextFormField(
                                keyboardType: null,
                                validator: (value) => _country == null
                                    ? 'El país no puede estar vacio'
                                    : null,
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText:
                                      _country == null ? 'Pais' : _country,
                                ),
                                onTap: () => _buildCountryList(),
                              ),
                            ),
                            currentCountry != null
                                ? Container(
                                    margin: EdgeInsets.only(bottom: 15),
                                    child: TextFormField(
                                      keyboardType: null,
                                      validator: (value) => _city == null
                                          ? 'La ciudad no puede estar vacia'
                                          : null,
                                      decoration: InputDecoration(
                                        errorStyle:
                                            TextStyle(color: Colors.red),
                                        filled: true,
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 12.0,
                                                horizontal: 10.0),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent,
                                              width: 0.0),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent,
                                              width: 0.0),
                                        ),
                                        fillColor: Colors.black38,
                                        hintText:
                                            _city == null ? 'Ciudad' : _city,
                                      ),
                                      onTap: () => _buildCityList(),
                                    ),
                                  )
                                : Container(),
                            Container(
                              height: ScreenUtil.instance.setHeight(45),
                              child: MaterialButton(
                                child: Container(
                                    child: Text(
                                  ' Registrar',
                                  textAlign: TextAlign.center,
                                )),
                                color: HexColor('3f704d'),
                                onPressed: validateAndSubmit,
                                elevation: 4.0,
                                minWidth: double.infinity,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
