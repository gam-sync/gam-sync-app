// Flutter
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// Providers
import 'package:gameapp/src/providers/user_provider.dart';

// Utils
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gameapp/src/utils/screen_util.dart';

class StartPage extends StatefulWidget {
  StartPage({Key key}) : super(key: key);

  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  // Variables for the login form
  String _email;
  String _password;

  // error: In case there is a possible errors
  Map<String, dynamic> error = {
    'mail_not_found': null,
    'fail_password': null,
    'unknown': null,
    'user_disabled': null
  };

  // formKey: Key of the form
  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var errors =
          await UserProvider.login(context, email: _email, password: _password);

      setState(() {
        if (errors['user_banned'] != null) {
          error['user_banned'] = errors['user_banned'];
          return null;
        }
        if (errors['mail_not_found'] != null) {
          error['mail_not_found'] = errors['mail_not_found'];
        }
        if (errors['fail_password'] != null) {
          error['fail_password'] = errors['fail_password'];
        }
        if (errors['unknown'] != null) {
          error['unknown'] = errors['unknown'];
        }
        if (errors['user_disabled'] != null) {
          error['user_disabled'] = errors['user_disabled'];
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Make the application adapt to all elements.
    ScreenUtils(context);

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
          padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil.instance.width / 24,
              vertical: ScreenUtil.instance.height / 100),
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.3), BlendMode.dstATop),
                image: AssetImage('assets/start/start.jpg')),
          ),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Center(
                  child: SvgPicture.asset(
                    'assets/logo.svg'
                  )
                ),
                Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Text('Hola! 👋🏼',
                          style: TextStyle(
                              fontSize: ScreenUtil.instance.setSp(20.0),
                              fontWeight: FontWeight.bold,
                              color: Colors.white60)),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      'Adentrate al mundo gaming. Para comenzar simplemente inicia sesión en tu cuenta o registra una nueva.',
                      style: TextStyle(color: Colors.white60),
                    )
                  ],
                ),
                Container(
                  height: 45,
                  child: MaterialButton(
                    child: Text('REGISTRO'),
                    color: HexColor('3f704d'),
                    onPressed: () => Navigator.pushNamed(context, 'register'),
                    elevation: 3.0,
                    minWidth: double.infinity,
                  ),
                ),
                GestureDetector(
                  child: Text(
                    'Ya tengo cuenta Game Sync',
                    style: TextStyle(color: Colors.redAccent),
                  ),
                  onTap: () => _buildLoginButton(),
                )
              ],
            ),
          ),
        ));
  }

  // buildLoginButton: Show a drop-down with the possible options to log in
  _buildLoginButton() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            color: HexColor('23395d'),
            height: ScreenUtil.instance.setHeight(300.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20.0, left: 10.0),
                  child: Text(
                    'Inicia sesión en Game Sync',
                    style: TextStyle(
                        color: Colors.white54,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: ButtonTheme(
                    buttonColor: HexColor('00a6e9'),
                    minWidth: 500,
                    height: 50.0,
                    child: RaisedButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Icon(
                                FontAwesomeIcons.twitter,
                                color: HexColor('00a6e9'),
                              )),
                          Container(
                              child: Text(
                            ' Accede a tu cuenta con Twitter',
                            textAlign: TextAlign.center,
                          )),
                          Container()
                        ],
                      ),
                      onPressed: () {
                        UserProvider.login(context, providerTwitter: true);
                      },
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: ButtonTheme(
                    buttonColor: Colors.blueGrey,
                    minWidth: 500,
                    height: 50.0,
                    child: RaisedButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: 30,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Icon(
                                Icons.email,
                                color: Colors.blueGrey,
                              )),
                          Container(
                              child: Text(
                            ' Accede a tu cuenta con email ',
                            textAlign: TextAlign.center,
                          )),
                          Container()
                        ],
                      ),
                      onPressed: () => Navigator.pushNamed(context, 'login'),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
