// Fluter & Dart
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Providers
import 'package:gameapp/src/providers/user_provider.dart';

// Utils
import 'package:gameapp/src/utils/provinces_util.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:image_picker/image_picker.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  // paddingSized: To avoid duplicate code
  final paddingSized = 10.0;

  // secondPage: It is used to access the following registration page if the fields are correct
  bool secondPage = false;

  // Variables for the registration form
  String _email;
  String _password;
  String _username;
  // String _repeatPassword; // Disabled until add repeat password
  String _genre;
  String _bio;
  String _city;
  String _country;

  // image: Profile image to upload
  File _image;

  // error: In case there is a possible error
  String _error;

  // Empty provinces list, initialized when opening the registration screen in init
  List<Map> _provinces = List<Map>();

  // Keys of the forms to know with what type of form I am validating
  final formKey = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _getProvinces();
  }

  // Get provinces for the province dropdown
  _getProvinces() {
    setState(() {
      _provinces = Provinces.getProvinces();
    });
  }

  // Check if the password and repeat password are the same.
  // Disabled until add repeat password

  /*bool checkRepeatPassword() {
    if (_repeatPassword != _password) {
      return true;
    }
    return false;
  } */

  bool validateAndSave() {
    final form1 = formKey2.currentState;
    if (form1.validate()) {
      form1.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      // User Data
      Map<String, dynamic> userToRegister = {
        'name': _username,
        'city': _city,
        'country': _country,
        'bio': _bio,
        'genre': _genre,
      };

      // Register the user and if there is a possible error it shows it on the screen.
      var error = await UserProvider.register(
          userToRegister, _email, _password, context, _image);

      setState(() {
        _error = error;
        secondPage = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Make the application adapt to all elements.
    ScreenUtils(context);

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body:
            !secondPage ? _buildFirstPageProfile() : _buildSecondPageProfile());
  }

  _buildFirstPageProfile() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [
            0.2,
            0.2,
            0.81,
            0.1
          ],
              colors: [
            Theme.of(context).primaryColor,
            HexColor('303030'),
            HexColor('303030'),
            Theme.of(context).primaryColor
          ])),
      child: Stack(
        children: <Widget>[
          AppBar(
            backgroundColor: Colors.transparent,
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: ScreenUtil.instance.height / 100,
                horizontal: ScreenUtil.instance.width / 6),
            child: Form(
              key: formKey,
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil.instance.setHeight(60.0)),
                  Center(
                    child: FlutterLogo(size: 100.0),
                  ),
                  Text('Regístrate',
                      style:
                          TextStyle(fontSize: ScreenUtil.instance.setSp(30.0))),
                  SizedBox(
                    height: ScreenUtil.instance.setWidth(24),
                  ),
                  TextFormField(
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.sentences,
                      validator: (value) => value.isEmpty
                          ? 'El nombre de usuario es un campo requerido'
                          : null,
                      initialValue: _username,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                              ScreenUtil.instance.setWidth(24)),
                        ),
                        hintText: 'Nombre de usuario',
                      ),
                      onSaved: (value) => _username = value),
                  SizedBox(height: paddingSized),
                  TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'El email no puede estar vacio';
                        }
                        if (!EmailValidator.validate(value)) {
                          return 'Introduce un correo valido';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                              ScreenUtil.instance.setWidth(24)),
                        ),
                        hintText: 'Correo electrónico',
                      ),
                      onSaved: (value) => _email = value),
                  _error != null
                      ? Text(
                          _error,
                          style: TextStyle(color: Colors.red),
                        )
                      : Container(),
                  SizedBox(height: paddingSized),
                  DropdownButtonFormField(
                    items: [
                      DropdownMenuItem(
                        child: Text('Hombre'),
                        value: "Hombre",
                      ),
                      DropdownMenuItem(
                        child: Text('Mujer'),
                        value: "Mujer",
                      ),
                      DropdownMenuItem(
                        child: Text('Otro'),
                        value: "Otro",
                      ),
                    ],
                    validator: (value) => value == null
                        ? 'Tienes que introducir un genero'
                        : null,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            ScreenUtil.instance.setWidth(24)),
                      ),
                    ),
                    hint: Text('Genero'),
                    value: _genre,
                    onChanged: (value) {
                      setState(() {
                        _genre = value;
                      });
                    },
                  ),
                  SizedBox(height: paddingSized),
                  TextFormField(
                      obscureText: true,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'La contraseña no puede estar vacia';
                        }
                        if (value.length < 6) {
                          return 'La contraseña tiene que tener mas de 6 caracteres';
                        }
                        return null;
                      },
                      initialValue: _password,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                              ScreenUtil.instance.setWidth(24)),
                        ),
                        hintText: 'Contraseña',
                      ),
                      onSaved: (value) => _password = value),
                  SizedBox(height: paddingSized),
                  Container(
                    width: ScreenUtil.instance.setWidth(200),
                    child: MaterialButton(
                      child: Text('Siguiente'),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        setState(() {
                          final form = formKey.currentState;

                          if (form.validate()) {
                            form.save();
                            secondPage = true;
                          }
                        });
                      },
                      elevation: 7.0,
                      height: ScreenUtil.instance.setHeight(42.0),
                      minWidth: double.infinity,
                      shape: StadiumBorder(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildSecondPageProfile() {
    Future getImage() async {
      var image = await ImagePicker.pickImage(
          source: ImageSource.gallery,
          imageQuality: 80,
          maxHeight: 720,
          maxWidth: 1280);
      setState(() {
        _image = image;
      });
    }

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              stops: [
            0.2,
            0.2,
            0.81,
            0.1
          ],
              colors: [
            Theme.of(context).primaryColor,
            HexColor('303030'),
            HexColor('303030'),
            Theme.of(context).primaryColor
          ])),
      child: Stack(
        children: <Widget>[
          AppBar(
            backgroundColor: Colors.transparent,
          ),
          Container(
            padding: EdgeInsets.symmetric(
                vertical: ScreenUtil.instance.height / 100,
                horizontal: ScreenUtil.instance.width / 6),
            child: Form(
              key: formKey2,
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil.instance.setHeight(100.0)),
                  Card(
                    child: GestureDetector(
                      onTap: () => getImage(),
                      child: Container(
                        width: ScreenUtil.instance.setWidth(90.0),
                        height: ScreenUtil.instance.setHeight(100.0),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: _image != null
                                    ? FileImage(_image)
                                    : AssetImage('assets/profile/profile.png'),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.all(Radius.circular(
                                ScreenUtil.instance.setWidth(75))),
                            border: Border.all(
                                color: Theme.of(context).accentColor,
                                width: ScreenUtil.instance.setWidth(3.0))),
                      ),
                    ),
                    shape: CircleBorder(),
                  ),
                  SizedBox(
                    height: ScreenUtil.instance.setWidth(24),
                  ),
                  TextFormField(
                      maxLines: 4,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.sentences,
                      initialValue: _bio,
                      validator: (value) => value.isEmpty
                          ? 'Tienes que introducir una biografia'
                          : null,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                              ScreenUtil.instance.setWidth(24)),
                        ),
                        hintText: 'Biografia',
                      ),
                      onSaved: (value) => _bio = value),
                  SizedBox(height: paddingSized),
                  Container(
                    child: Container(
                      child: DropdownButtonFormField(
                        items: _provinces.map((province) {
                          return DropdownMenuItem(
                            child: Container(child: Text(province['name'])),
                            value: province['name'],
                          );
                        }).toList(),
                        validator: (value) => value == null
                            ? 'Tienes que introducir una provincia'
                            : null,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(
                                ScreenUtil.instance.setWidth(24)),
                          ),
                        ),
                        hint: Text('Provincia'),
                        value: _city,
                        onChanged: (value) {
                          setState(() {
                            _city = value;
                          });
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: paddingSized),
                  TextFormField(
                      keyboardType: TextInputType.text,
                      validator: (value) => value.isEmpty
                          ? 'Tienes que introducir un pais'
                          : null,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                              ScreenUtil.instance.setWidth(24)),
                        ),
                        hintText: 'Pais',
                      ),
                      initialValue: _country,
                      onSaved: (value) => _country = value),
                  SizedBox(height: paddingSized),
                  Container(
                    width: ScreenUtil.instance.setWidth(200),
                    child: MaterialButton(
                      child: Text('Crear cuenta'),
                      color: Theme.of(context).primaryColor,
                      onPressed: () => validateAndSubmit(),
                      elevation: 7.0,
                      height: ScreenUtil.instance.setHeight(42.0),
                      minWidth: double.infinity,
                      shape: StadiumBorder(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
