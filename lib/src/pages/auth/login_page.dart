import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Variables for the login form
  String _email;
  String _password;

  // error: In case there is a possible errors
  Map<String, dynamic> error = {
    'mail_not_found': null,
    'fail_password': null,
    'unknown': null,
    'user_disabled': null,
    'user_banned': null,
  };

  // formKey: Key of the form
  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var errors =
          await UserProvider.login(context, email: _email, password: _password);
      setState(() {
        if (errors['mail_not_found'] != null) {
          error['mail_not_found'] = errors['mail_not_found'];
        }
        if (errors['fail_password'] != null) {
          error['fail_password'] = errors['fail_password'];
        }
        if (errors['unknown'] != null) {
          error['unknown'] = errors['unknown'];
        }
        if (errors['user_disabled'] != null) {
          error['user_disabled'] = errors['user_disabled'];
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Iniciar sesion en Game Sync',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        child: Center(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil.instance.width / 24,
                vertical: ScreenUtil.instance.height / 100),
            color: HexColor('203454'),
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 12.0, bottom: 1.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Con correo electronico',
                          style: TextStyle(
                            fontSize: ScreenUtil.instance.setSp(11.0),
                          )),
                    ),
                  ),
                  Form(
                    key: formKey,
                    child: Column(
                      children: <Widget>[
                        Divider(
                          height: 30,
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 15),
                          child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) => value.isEmpty
                                  ? 'El email no puede estar vacio'
                                  : null,
                              decoration: InputDecoration(
                                errorText: error['mail_not_found'] != null
                                    ? error['mail_not_found']
                                    : null,
                                errorStyle: TextStyle(color: Colors.red),
                                filled: true,
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                fillColor: Colors.black38,
                                hintText: 'Correo electronico',
                              ),
                              onChanged: (val) {
                                setState(() {
                                  error['mail_not_found'] = null;
                                });
                              },
                              onSaved: (value) => _email = value),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20.0),
                          child: TextFormField(
                              obscureText: true,
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'La contraseña no puede estar vacia';
                                }
                                if (value.length < 6) {
                                  return 'La contraseña tiene que tener mas de 6 caracteres';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                errorStyle: TextStyle(color: Colors.red),
                                errorText: error['fail_password'] != null
                                    ? error['fail_password']
                                    : null,
                                filled: true,
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                fillColor: Colors.black38,
                                hintText: 'Contraseña',
                              ),
                              onChanged: (val) {
                                setState(() {
                                  error['fail_password'] = null;
                                });
                              },
                              onSaved: (value) => _password = value),
                        ),
                        Container(
                          height: ScreenUtil.instance.setHeight(45),
                          child: MaterialButton(
                            child: Container(
                                child: Text(
                              ' Iniciar sesión',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: validateAndSubmit,
                            elevation: 4.0,
                            minWidth: double.infinity,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: ScreenUtil.instance.setHeight(100.0)),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Divider(),
                        ),
                        Container(
                          child: Text(
                            'Otros metodos',
                            style: TextStyle(color: Colors.white38),
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                        ),
                        Expanded(
                          child: Divider(),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 12.0, bottom: 10.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Redes sociales',
                          style: TextStyle(
                            fontSize: ScreenUtil.instance.setSp(11.0),
                          )),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        margin:
                            EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        child: GestureDetector(
                          child: ButtonTheme(
                            child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(100.0)),
                                child: Icon(
                                  FontAwesomeIcons.google,
                                  color: HexColor('#EA4335'),
                                  size: 30,
                                )),
                          ),
                          onTap: () =>
                              ToastUtil.show("No funciono aún :(", context),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        child: GestureDetector(
                          child: ButtonTheme(
                            child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(100.0)),
                                child: Icon(
                                  FontAwesomeIcons.twitter,
                                  color: HexColor('#00a6e9'),
                                  size: 30,
                                )),
                          ),
                          onTap: () =>
                              ToastUtil.show("No funciono aún :(", context),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
