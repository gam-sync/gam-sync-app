import 'package:flutter/material.dart';
import 'package:gameapp/src/pages/user/message/rooms/rooms_page.dart';
import 'package:gameapp/src/utils/hex_util.dart';

import 'message/messages_page.dart';
import 'message/rooms/rooms_page.dart';

class MessagesPage extends StatelessWidget {
  const MessagesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(100.0),
          child: AppBar(
            elevation: 1,
            centerTitle: true,
            backgroundColor: HexColor('23395d'),
            title: Text(
              'Mensajes',
              style: TextStyle(fontSize: 16),
            ),
            bottom: TabBar(
              tabs: <Widget>[
                Tab(child: Text('Chat')),
                Tab(
                  child: Text('Mensajes'),
                )
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: <Widget>[RoomsPage(), MessageUserPage()],
        ),
      ),
    );
  }
}
