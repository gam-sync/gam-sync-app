import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gameapp/src/providers/achievements_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class AchievementsPage extends StatefulWidget {
  final String uid;
  AchievementsPage({Key key, this.uid}) : super(key: key);

  @override
  _AchievementsPageState createState() => _AchievementsPageState();
}

class _AchievementsPageState extends State<AchievementsPage> {
  var achievements;

  _getAchievements() async {
    var achievements = await AchievementProvider.getAchievementsFromUser(widget.uid);

    setState(() {
      this.achievements = achievements;
    });
  }

  List<Widget> _buildContainer() {
    List<Widget> achievementContainer = new List();
    
    for (int i = 0; i < this.achievements.documents.length; i++) {
      var achievement = this.achievements.documents[i];
      
      achievementContainer.add(
        Container(
          decoration: BoxDecoration(
              color: HexColor('#1A2B46'),
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          margin:
              EdgeInsets.only(left: 25.0, right: 25.0, top: 20.0),
          padding: EdgeInsets.all(10.0),
          child: ListTile(
            title: Text(achievement['title']),
            leading: SvgPicture.asset(achievement['asset']),
            subtitle: Container(
              child: Text(achievement['description']),
            ),
          ),
        ),
      );
    }


    return achievementContainer;
  }

  @override
  void initState() {
    super.initState();
    _getAchievements();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          title: Text(
            "Logros",
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: this.achievements != null && this.achievements.documents.length >= 1
          ? Container(
              color: HexColor('203454'),
              child: ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Center(
                        child: Container(
                          margin: EdgeInsets.only(top: 25.0),
                          child: CircleAvatar(
                              backgroundColor: HexColor('#1A2B46'),
                              maxRadius: 125.0,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'assets/achievements/friends/friends_gold.svg',
                                    width: 100.0,
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(top: 20.0),
                                      child: Text(
                                        'Por 50 amigos',
                                        style: TextStyle(color: Colors.white),
                                      ))
                                ],
                              )),
                        ),
                      ),
                      Column(
                        children: _buildContainer(),
                      )
                    ],
                  ),
                ],
              ))
          : Container(),
    );
  }
}
