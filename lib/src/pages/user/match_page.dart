import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/profile/profile_image_page.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/providers/match_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';

class MatchPlayers extends StatefulWidget {
  MatchPlayers({Key key}) : super(key: key);

  _MatchPlayersState createState() => _MatchPlayersState();
}

class _MatchPlayersState extends State<MatchPlayers> {
  bool searching = false;
  List users;
  int indexUser = 0;
  var games;
  String _uid;
  String currentGame;

  @override
  void initState() {
    _getGames();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _getGames() async {
    var user = await UserProvider.getDataAuthUser();
    setState(() {
      _uid = user.documentID;
    });
  }

  _getUsers(String gameId) async {
    searching = true;
    users = await MatchProvider.getMatch(gameId, context);
    indexUser = 0;
    setState(() {
      currentGame = gameId;
      searching = false;
      if (users.length == 0) {
        searching = false;
        users = null;
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Emparejar jugadores',
            style: TextStyle(fontSize: 16),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add_circle_outline),
              onPressed: () => Navigator.pushNamed(context, 'addgame'),
            )
          ],
        ),
      ),
      body: !searching && users == null
          ? Container(
              color: HexColor('203454'),
              child: FutureBuilder(
                future: UserProvider.getGamesFromUser(_uid),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  }
                  var userGame = snapshot.data.documents;

                  if (userGame.length > 0) {
                    return ListView.builder(
                      itemExtent: 200.0,
                      itemCount: userGame.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                            onTap: () {
                              _getUsers(userGame[index].documentID);
                            },
                            child: FutureBuilder(
                              future: GameProvider.getGame(
                                  userGame[index].documentID),
                              builder: (context, snapshot1) {
                                if (!snapshot1.hasData) {
                                  return Container();
                                }
                                var game = snapshot1.data;

                                return Container(
                                    child: MatchProvider.buildMatchPage(
                                        game, context));
                              },
                            ));
                      },
                    );
                  } else if (userGame.length == 0) {
                    return Container(
                      child: Center(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: Text('No tienes ningún juego añadido. ¿Porque no añades uno?')),
                        MaterialButton(
                                child: Container(
                                    child: Text(
                                  ' Añadir juegos',
                                  textAlign: TextAlign.center,
                                )),
                                color: HexColor('3f704d'),
                                elevation: 4.0,
                                onPressed: () => Navigator.pushNamed(context, 'addgame'),
                                minWidth: 100,
                              ),
                        ],
                      )),
                    );
                  } else {
                    return Container();
                  }
                },
              ))
          : searching
              ? Container(
                  color: HexColor('203454'),
                  child: Center(
                    child: SpinKitRipple(
                      color: Colors.greenAccent,
                      size: 70,
                      duration: Duration(seconds: 1),
                    ),
                  ),
                )
              : !searching && users != null
                  ? Container(
                      color: HexColor('203454'),
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image: AssetImage(
                                      'assets/profile/background.jpg'),
                                  fit: BoxFit.cover,
                                  colorFilter: new ColorFilter.mode(
                                      Colors.black.withOpacity(0.5),
                                      BlendMode.dstOut),
                                )),
                                height: ScreenUtil.instance.setHeight(130),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: double.infinity),
                                child: Icon(Icons.camera_alt),
                              ),
                              Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.only(
                                  top: ScreenUtil.instance.setHeight(78),
                                ),
                                child: Card(
                                  child: GestureDetector(
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ProfileImagePage(
                                                  profileUrl: users[indexUser]
                                                      [0]['photoUrl'],
                                                ))),
                                    child: Card(
                                      child: GestureDetector(
                                        onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ProfileImagePage(
                                                      profileUrl:
                                                          users[indexUser][0]
                                                              ['photoUrl'],
                                                    ))),
                                        child: Container(
                                          width: ScreenUtil.instance
                                              .setWidth(70.0),
                                          height: ScreenUtil.instance
                                              .setHeight(76.0),
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: CachedNetworkImageProvider(
                                                      users[indexUser][0]
                                                          ['photoUrl']),
                                                  fit: BoxFit.cover),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(ScreenUtil
                                                      .instance
                                                      .setWidth(75))),
                                              border: Border.all(
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                  width: ScreenUtil.instance
                                                      .setWidth(3.0))),
                                        ),
                                      ),
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                  shape: CircleBorder(),
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(
                                    bottom: 10.0,
                                    top: ScreenUtil.instance.setHeight(170.0)),
                                child: Text(
                                  users[indexUser][0]['name'],
                                  style: TextStyle(
                                      fontSize: ScreenUtil.instance.setSp(20.0),
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          Divider(
                            height: 1,
                            color: Colors.white10,
                          ),
                          Container(
                            color: Colors.black12,
                            child: ListTile(
                              title: Text(
                                'Ciudad',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              trailing: Text(
                                users[indexUser][0]['city'] +
                                    " - " +
                                    users[indexUser][0]['country'],
                              ),
                            ),
                          ),
                          Divider(
                            height: 1,
                            color: Colors.white10,
                          ),
                          MatchProvider.buildUserGame(
                              currentGame,
                              users[indexUser][0].documentID,
                              users[indexUser][1]),
                          Divider(
                            height: 50,
                            endIndent: 20,
                            indent: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Align(
                                  alignment: FractionalOffset.bottomRight,
                                  child: MaterialButton(
                                    color: HexColor('960000'),
                                    height: 50,
                                    minWidth:
                                        MediaQuery.of(context).size.width / 2,
                                    onPressed: () {
                                      setState(() {
                                        if (indexUser < users.length - 1) {
                                          indexUser += 1;
                                        } else {
                                          _getUsers(currentGame);
                                        }
                                      });
                                    },
                                    child: Text('Rechazar'),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: FractionalOffset.bottomLeft,
                                  child: MaterialButton(
                                    color: HexColor('006727'),
                                    height: 50,
                                    minWidth:
                                        MediaQuery.of(context).size.width / 2,
                                    onPressed: () {
                                      UserProvider.addFriend(
                                          users[indexUser][0].documentID);
                                      setState(() {
                                        if (indexUser < users.length - 1) {
                                          indexUser += 1;
                                        } else {
                                          _getUsers(currentGame);
                                        }
                                      });
                                    },
                                    child: Text('Enviar solicitud de amistad'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ))
                  : Container(),
    );
  }
}
