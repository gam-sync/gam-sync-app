import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:gameapp/src/pages/user/admin/tablones/admin_tablones_page.dart';
import 'package:gameapp/src/pages/user/admin/users/admin_users_page.dart';
import 'package:gameapp/src/utils/hex_util.dart';


class AdminStartPage extends StatefulWidget {
  AdminStartPage({Key key}) : super(key: key);

  @override
  _AdminStartPageState createState() => _AdminStartPageState();
}

class _AdminStartPageState extends State<AdminStartPage> {
  int _currentIndex = 0;
  int count = 0;
  CircularBottomNavigationController _navigationController;

  final List<Widget> _children = [
    AdminUsersPage(),
    AdminTablonesPage(),
    Text('asd'),
    Text('asd')
  ];

  @override
  void initState() {
    _navigationController = CircularBottomNavigationController(_currentIndex);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Admin',
            style: TextStyle(fontSize: 16),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () => Navigator.pushNamed(context, 'adminSearchUsers'),
            )
          ],
        ),
      ),
      body: Stack(
            children: <Widget>[
              Padding(child: _children[_currentIndex], padding: EdgeInsets.only(bottom: 60.0),),
              Align(
                alignment: Alignment.bottomCenter,
                child: CircularBottomNavigation(
                  List.of([
                    TabItem(Icons.people, "Usuarios", HexColor('#375d99'), false),
                    TabItem(Icons.table_chart, "Tablones", HexColor('#375d99'), false),
                    TabItem(Icons.supervised_user_circle, "Emparejar",
                        HexColor('#375d99'), false),
                    TabItem(Icons.chat, "Chat", HexColor('375d99'), true, countBadge: count),
                  ]),
                  selectedCallback: (int selectedPost) {
                    setState(() {
                      this._currentIndex = selectedPost;
                    });
                  },
                  barHeight: 60.0,
                  circleSize: 60.0,
                  barBackgroundColor: HexColor('23395d'),
                  animationDuration: Duration(milliseconds: 300),
                  selectedPos: 0,
                  controller: _navigationController,
                ),
              ),
            ],
          ),
    );
  }
}