import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/admin_provider.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:gameapp/src/utils/hex_util.dart';

class AdminTablonesPage extends StatefulWidget {
  AdminTablonesPage({Key key}) : super(key: key);

  @override
  _AdminTablonesPageState createState() => _AdminTablonesPageState();
}

class _AdminTablonesPageState extends State<AdminTablonesPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        color: HexColor('23395d'),
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Tablones',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Divider(
              height: 5.0,
            ),
            StreamBuilder(
              stream: Firestore.instance
                  .collection('events')
                  .orderBy('created_at', descending: true)
                  .where('status', isEqualTo: false)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return CircularProgressIndicator();
                }

                return Container(
                  height: ScreenUtil.instance.height / 1.24,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      var event = snapshot.data.documents[index];
                      var date = DateTime.fromMillisecondsSinceEpoch(
                          event['created_at'].seconds * 1000);
                      return Column(
                        children: <Widget>[
                          Container(
                              color: Colors.transparent,
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 10.0),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.black26,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0))),
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  child: ListTile(
                                      onTap: () {},
                                      isThreeLine: true,
                                      title: Text(event['user']['name']),
                                      subtitle: Text(
                                          timeago.format(date, locale: 'es') +
                                              "\n\n${event['description']}"),
                                      leading: GestureDetector(
                                        onTap: () => Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    ProfilePage(
                                                      uid: event['user']['uid'],
                                                      username: event['user']
                                                          ['name'],
                                                    ))),
                                        child: CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage:
                                                CachedNetworkImageProvider(
                                                    event['user']['photoUrl'])),
                                      ),
                                      trailing: Column(
                                        children: <Widget>[
                                          GestureDetector(
                                              child: Icon(
                                                  FontAwesomeIcons.checkCircle),
                                              onLongPress: () =>
                                                  AdminProvider.acceptEvent(
                                                      event.documentID)),
                                          SizedBox(height: 5,),
                                          GestureDetector(
                                              child: Icon(
                                                  FontAwesomeIcons.trash),
                                              onLongPress: () =>
                                                  AdminProvider.declineEvent(
                                                      event.documentID))
                                        ],
                                      )),
                                ),
                              )),
                          SizedBox(
                            height: 20.0,
                          )
                        ],
                      );
                    },
                    itemCount: snapshot.data.documents.length,
                  ),
                );
              },
            ),
            Divider(
              height: 1.0,
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
          ],
        ),
      ),
    );
  }
}
