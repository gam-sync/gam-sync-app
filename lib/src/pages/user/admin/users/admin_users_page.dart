import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/admin_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AdminUsersPage extends StatefulWidget {
  AdminUsersPage({Key key}) : super(key: key);

  @override
  _AdminUsersPageState createState() => _AdminUsersPageState();
}

class _AdminUsersPageState extends State<AdminUsersPage> {
  // countUsers: User Counter
  String countUsers = "0";

  @override
  void initState() {
    _getCountUser();
    super.initState();
  }

  // getCountUser: Get count users from database
  _getCountUser() async {
    String countUsers = await AdminProvider.getUsersCount();
    setState(() {
      this.countUsers = countUsers;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: HexColor('23395d'),
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Usuarios (${countUsers.toString()})',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Divider(
              height: 5.0,
            ),
            StreamBuilder(
              stream: Firestore.instance
                  .collection('users')
                  .orderBy('created_at', descending: true)
                  .limit(50)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return CircularProgressIndicator();
                }

                return Container(
                  height: ScreenUtil.instance.height / 1.24,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      var user = snapshot.data.documents[index];
                      return Column(
                        children: <Widget>[
                          ListTile(
                            title: Text(user['name']),
                            subtitle: user['email'] != null
                                ? Text(user['email'])
                                : Text('No tiene email'),
                            leading: CircleAvatar(
                              backgroundImage:
                                  CachedNetworkImageProvider(user['photoUrl']),
                            ),
                            trailing: GestureDetector(
                              child: user['banned'] != null && !user['banned']
                                  ? Icon(FontAwesomeIcons.ban)
                                  : Icon(FontAwesomeIcons.unlock),
                              onLongPress: () {
                                if (!user['banned']) {
                                  AdminProvider.setBan(user.documentID);
                                } else {
                                  AdminProvider.setUnban(user.documentID);
                                }
                              },
                            ),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage(
                                          uid: user.documentID,
                                          username: user['name'],
                                        ))),
                          ),
                          Divider(
                            height: 1.0,
                          )
                        ],
                      );
                    },
                    itemCount: snapshot.data.documents.length,
                  ),
                );
              },
            ),
            Divider(
              height: 1.0,
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
          ],
        ),
      ),
    );
  }
}
