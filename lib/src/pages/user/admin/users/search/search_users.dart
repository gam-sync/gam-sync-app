import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/admin_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AdminSearchUsersPage extends StatefulWidget {
  AdminSearchUsersPage({Key key}) : super(key: key);

  _AdminSearchUsersPageState createState() => _AdminSearchUsersPageState();
}

class _AdminSearchUsersPageState extends State<AdminSearchUsersPage> {
  String _query;
  Stream<QuerySnapshot> stream;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Container(
            margin: EdgeInsets.only(right: 75, left: 10),
            child: TextFormField(
              keyboardType: TextInputType.text,
              validator: (value) {
                if (value.isEmpty) {
                  return 'El nombre de usuario esta vacio';
                }
                return null;
              },
              decoration: InputDecoration(
                errorStyle: TextStyle(color: Colors.red),
                filled: true,
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 12.0, horizontal: 10.0),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent, width: 0.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent, width: 0.0),
                ),
                fillColor: Colors.black38,
                hintText: 'Usuario (minimo 2 letras)',
              ),
              onEditingComplete: () {
                var users = Firestore.instance
                    .collection('users')
                    .where('name', isGreaterThanOrEqualTo: _query)
                    .where('name', isLessThanOrEqualTo: _query + '\uf8ff')
                    .snapshots();
                setState(() {
                  stream = users;
                });
              },
              onChanged: (value) {
                setState(() {
                  _query = value;
                });
              },
            ),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
            _query != null
                ? Container(
                    height: 20.0,
                    color: Colors.black26,
                  )
                : Container(),
            Container(
              height: ScreenUtil.instance.setHeight(600),
              child: stream != null
                  ? StreamBuilder(
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return Container();
                        return ListView.builder(
                          itemBuilder: (context, index) {
                            var user = snapshot.data.documents[index];
                            return Column(
                              children: <Widget>[
                                Container(
                                  height: 70,
                                  color: HexColor('23395d'),
                                  child: ListTile(
                                    title: Text(user['name']),
                                    subtitle: user['email'] != null
                                        ? Text(user['email'])
                                        : Text('No tiene email'),
                                    leading: CircleAvatar(
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              user['photoUrl']),
                                    ),
                                    trailing: GestureDetector(
                                      child: user['banned'] != null &&
                                              !user['banned']
                                          ? Icon(FontAwesomeIcons.ban)
                                          : Icon(FontAwesomeIcons.unlock),
                                          onLongPress: () {
                                            if (!user['banned']) {
                                              AdminProvider.setBan(user.documentID);
                                            } else {
                                              AdminProvider.setUnban(user.documentID);
                                            }
                                          },
                                    ),
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ProfilePage(
                                                  uid: user.documentID,
                                                  username: user['name'],
                                                ))),
                                  ),
                                ),
                                Divider(
                                  height: 1,
                                  color: Colors.white10,
                                )
                              ],
                            );
                          },
                          itemCount: snapshot.data.documents.length,
                        );
                      },
                      stream: stream,
                    )
                  : Container(),
            )
          ],
        ),
      ),
    );
  }
}
