import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/message/rooms/roomscreen_page.dart';
import 'package:gameapp/src/providers/room_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class RoomsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder(
      stream: RoomProvider.getRooms(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData)
          return Container(
                                 
              child: Center(
            child: Center(
                child: SpinKitDoubleBounce(
              color: Theme.of(context).accentColor,
              size: 50.0,
            )),
          ));
        return Container(color: HexColor('203454'), child: ListView(children: _buildRooms(snapshot, context)));
      },
    ));
  }

  _buildRooms(AsyncSnapshot<QuerySnapshot> snapshot, BuildContext context) {
    return snapshot.data.documents
        .map((room) => ListTile(
              title: Text(room['title']),
              leading: CircleAvatar(
                backgroundImage: CachedNetworkImageProvider(room['icon']),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RoomScreenPage(
                              roomId: room.documentID,
                              roomName: room['title'],
                            )));
              },
            ))
        .toList();
  }
}
