// Flutter
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:gameapp/src/pages/user/message/rooms/roomoptions_page.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';

// Firebase
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gameapp/src/providers/chat_provider.dart';

// Providers
import 'package:gameapp/src/providers/room_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

// Utils
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher.dart';

class RoomScreenPage extends StatefulWidget {
  RoomScreenPage({this.roomId, this.roomName});
  final String roomId;
  final String roomName;

  _RoomScreenPageState createState() => _RoomScreenPageState();
}

class _RoomScreenPageState extends State<RoomScreenPage>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  // Controllers
  final TextEditingController _textController = TextEditingController();
  final ScrollController listScrollController = new ScrollController();

  // uid: Uid of auth current user
  String _uid;
  List<dynamic> blockedUsers;
  bool mounted = false;
  Color sendButton;

  List<Color> randomColors = [
    Colors.orange,
    Colors.redAccent,
    Colors.pink,
    Colors.yellowAccent,
    Colors.green,
    Colors.black
  ];

  // listMessage: Chat message list
  var listMessage;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    ChatProvider.checkConnectivity();
    RoomProvider.setOnline(widget.roomId);
    _getBlockedUsers();
    _getID();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.paused:
        RoomProvider.unsetOnline(widget.roomId);
        break;
      case AppLifecycleState.resumed:
        RoomProvider.setOnline(widget.roomId);
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.suspending:
        RoomProvider.unsetOnline(widget.roomId);
        break;
    }
  }

  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  _getID() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = user.uid;
    });
  }

  _getBlockedUsers() async {
    var currentUser = await UserProvider.getDataAuthUser();
    setState(() {
      blockedUsers = currentUser.data['blocked_users'];
      mounted = true;
    });
  }

  Future<bool> _setOffline() async {
    RoomProvider.unsetOnline(widget.roomId);
    return true;
  }

  void _handleSubmitted(String text) async {
    String textTrim = text.trim();
    if (textTrim.isEmpty) return null;
    _textController.clear();

    RoomProvider.sendMessage(text, widget.roomId, _uid, widget.roomName);
    listScrollController.animateTo(0.0,
        duration: Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return WillPopScope(
      onWillPop: _setOffline,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            actions: <Widget>[
              IconButton(
                onPressed: () => Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => RoomOptionsPage(
                            roomId: widget.roomId, roomName: widget.roomName))),
                icon: Icon(Icons.more_vert),
              )
            ],
            title: Text(
              widget.roomName,
              style: TextStyle(fontSize: 16),
            ),
            centerTitle: true,
          ),
        ),
        body: Container(
          color: HexColor('203454'),
          child: Column(
            children: <Widget>[
              mounted
                  ? Flexible(
                      child: StreamBuilder(
                        stream:
                            RoomProvider.getMessagesFromRoomID(widget.roomId),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) return Container();
                          listMessage = snapshot.data.documents;
                          return ListView.builder(
                            physics: BouncingScrollPhysics(),
                            padding: new EdgeInsets.all(8.0),
                            reverse: true,
                            itemBuilder: (context, index) => buildItem(
                                index, snapshot.data.documents[index]),
                            itemCount: snapshot.data.documents.length,
                            controller: listScrollController,
                          );
                        },
                      ),
                    )
                  : Flexible(
                      child: Container(),
                    ),
              Divider(height: 1.0),
              Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index, document) {
    if (blockedUsers != null && blockedUsers.contains(document['userId'])) {
      return Container();
    }
    if (_uid == document['userId']) {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                child: !document['text'].contains('http')
                    ? SelectableText(
                        document['text'],
                        style: TextStyle(color: Colors.black),
                      )
                    : Linkify(
                        text: document['text'],
                        style: TextStyle(color: Colors.black),
                        onOpen: (link) async {
                          if (await canLaunch(link.url)) {
                            await launch(link.url);
                          }
                        },
                      ),
                padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                width: 200.0,
                decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(8.0)),
                margin: EdgeInsets.only(
                    bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                    right: 10.0),
              ),
            ],
          ),
        ],
      );
    } else {
      return GestureDetector(
        onTap: () {
          _textController.text = "@" + document['name'] + " ";
          final val =
              TextSelection.collapsed(offset: _textController.text.length);
          _textController.selection = val;
        },
        child: Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    child: Material(
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Container(
                          child: SpinKitDoubleBounce(
                              color: Theme.of(context).accentColor, size: 50.0),
                          width: 35.0,
                          height: 35.0,
                          padding: EdgeInsets.all(10.0),
                        ),
                        imageUrl: document['photoUrl'],
                        width: 35.5,
                        height: 35.5,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(18.0),
                      ),
                      clipBehavior: Clip.hardEdge,
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfilePage(
                                    uid: document['userId'],
                                    username: document['name'],
                                  )));
                    },
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(bottom: 1.0),
                          child: Container(
                            child: Text(
                              document['name'],
                              style: TextStyle(
                                  fontSize: 12.5,
                                  color: document['isAdmin'] != null &&
                                          document['isAdmin']
                                      ? HexColor('FF0000')
                                      : Colors.white70,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        !document['text'].contains('http')
                            ? SelectableText(
                                document['text'],
                                style: TextStyle(color: Colors.white),
                              )
                            : Linkify(
                                text: document['text'],
                                style: TextStyle(color: Colors.white),
                                onOpen: (link) async {
                                  if (await canLaunch(link.url)) {
                                    await launch(link.url);
                                  }
                                },
                              ),
                      ],
                    ),
                    padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                    width: 200.0,
                    decoration: BoxDecoration(
                        color: HexColor('3d5a8a'),
                        borderRadius: BorderRadius.circular(8.0)),
                    margin: EdgeInsets.only(left: 10.0, bottom: 10.0),
                  )
                ],
              ),
              Container(
                child: Text(
                  DateFormat('kk:mm - dd/M/yy')
                      .format(document['created_at'].toDate()),
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12.0,
                      fontStyle: FontStyle.italic),
                ),
                margin: EdgeInsets.only(left: 50.0, top: 2.0, bottom: 2.0),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
          margin: EdgeInsets.only(bottom: 10.0),
        ),
      );
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] == _uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] != _uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        color: HexColor('3d5a8a'),
        child: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () => ToastUtil.show('Proximamente 😊', context),
              child: Container(
                margin: EdgeInsets.only(left: 5.0),
                child: Icon(Icons.camera_alt),
              ),
            ),
            Flexible(
              child: Container(
                margin: EdgeInsets.only(left: 10.0, top: 10, bottom: 10),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  onChanged: (value) {
                    setState(() {
                      value.trim().isEmpty
                          ? sendButton = Theme.of(context).accentColor
                          : sendButton = Colors.green;
                    });
                  },
                  controller: _textController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration.collapsed(
                      hintText: "Envia un mensaje",
                      focusColor: Colors.black,
                      fillColor: Colors.black),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    color: sendButton,
                    icon: Icon(Icons.send),
                    onPressed: () => _handleSubmitted(_textController.text))),
          ],
        ),
      ),
    );
  }
}
