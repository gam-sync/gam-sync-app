import 'package:flutter/material.dart';
import 'package:gameapp/src/providers/room_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';


class RoomOptionsPage extends StatefulWidget {
  final String roomName;
  final String roomId;
  RoomOptionsPage({Key key, this.roomId, this.roomName}) : super(key: key);

  @override
  _RoomOptionsPageState createState() => _RoomOptionsPageState();
}

class _RoomOptionsPageState extends State<RoomOptionsPage> {
  bool isActive = false;


  _getUserNotification() async {
    bool isActive = await RoomProvider.checkIfIsActive(widget.roomId);
    setState(() {
      this.isActive = isActive;
    });
  }

  @override
  void initState() { 
    super.initState();
    _getUserNotification();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Opciones del chat ${widget.roomName}',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
                        Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Notificaciones',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: ListTile(
                title: Text('Recibir notificaciones del chat ' + widget.roomName),
                subtitle: Text(
                    'Recibiras una notificación cuando un usuario mande un mensaje.'),
                trailing: Switch(
                  onChanged: (value) async {
                    if (isActive) {
                      value = await RoomProvider.removeUserToArrayNotifications(widget.roomId);
                    } else {
                      value = await RoomProvider.addUserToArrayNotifications(widget.roomId);
                    }
                    setState(() {
                      this.isActive = value;
                    });
                  },
                  value: isActive,
                ),
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
          ],
        ),
      ),
    );
  }
}