import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/providers/chat_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:intl/intl.dart';

class ChatScreen extends StatefulWidget {
  ChatScreen({this.chatId, this.username, this.userId});
  final String chatId;
  final String username;
  final String userId;

  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with TickerProviderStateMixin, WidgetsBindingObserver {
  // Controllers
  final TextEditingController _textController = TextEditingController();
  final ScrollController listScrollController = new ScrollController();

  // uid: Uid of auth current user
  String _uid;
  String _username;

  // listMessage: Chat message list
  var listMessage;

  @override
  void initState() {
    super.initState();
    ChatProvider.checkConnectivity();
    WidgetsBinding.instance.addObserver(this);
    _getID();
    _setSeen();
    _setOnline(1);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch(state){
      case AppLifecycleState.paused:
        _setOnline(2);
        break;
      case AppLifecycleState.resumed:
        _setSeen();
        _setOnline(1);
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.suspending:
        _setOnline(2);
        break;
    }
  }

  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  _getID() async {
    var userData = await UserProvider.getDataAuthUser();
    setState(() {
      _uid = userData.documentID;
      _username = userData['name'];
    });
  }

  Future<bool> _setOffline() async {
    _setOnline(2);
    return true;
  }

  _setSeen() async {
    await UserProvider.setSeen(widget.chatId, 1);
  }

  _setOnline(int type) async {
    await UserProvider.setOnline(widget.chatId, type);
  }

  Future<bool> _getOnline() async {
    bool isOnline = await UserProvider.getOnline(widget.chatId);
    return isOnline;
  }

  void _handleSubmitted(String text) async {
    if (text.isEmpty) return null;
    _textController.clear();
    ChatProvider.sendMessage(text, widget.chatId, _uid, widget.userId);

    bool isOnline = await _getOnline();

    if (!isOnline) {
      ChatProvider.sendNotification(text, _username, widget.userId);
      UserProvider.setSeen(widget.chatId, 2);
    } else {
      _setSeen();
    }
  }



  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);

    return WillPopScope(
      onWillPop: _setOffline,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              widget.username,
              style: TextStyle(fontSize: 16),
            ),
            centerTitle: true,
          ),
        ),
        body: Container(
          color: HexColor('203454'),
          child: Column(
            children: <Widget>[
              Flexible(
                child: StreamBuilder<QuerySnapshot>(
                  stream: ChatProvider.getMessagesFromChatID(widget.chatId),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();
                    listMessage = snapshot.data.documents;
                    return ListView.builder(
                      padding: new EdgeInsets.all(8.0),
                      reverse: true,
                      itemBuilder: (context, index) =>
                          buildItem(index, snapshot.data.documents[index]),
                      itemCount: snapshot.data.documents.length,
                      controller: listScrollController,
                    );
                  },
                ),
              ),
              Divider(height: 1.0),
              Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index, DocumentSnapshot message) {
    if (_uid == message['senderId']) {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                child: Text(
                  message['text'],
                  style: TextStyle(color: Colors.black),
                ),
                padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                width: 200.0,
                decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(8.0)),
                margin: EdgeInsets.only(
                    bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                    right: 10.0),
              ),
            ],
          ),
        ],
      );
    } else {
      return Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                GestureDetector(
                  child: Material(
                    child: CachedNetworkImage(
                      placeholder: (context, url) => Container(
                        child: SpinKitDoubleBounce(
                            color: Theme.of(context).accentColor, size: 50.0),
                        width: 35.0,
                        height: 35.0,
                        padding: EdgeInsets.all(10.0),
                      ),
                      imageUrl: message['photoUrl'],
                      width: 35.5,
                      height: 35.5,
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(18.0),
                    ),
                    clipBehavior: Clip.hardEdge,
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfilePage(
                                  uid: message['senderId'],
                                  username: message['name'],
                                )));
                  },
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(bottom: 1.0),
                        child: Container(
                          child: Text(
                            message['name'],
                            style: TextStyle(
                                fontSize: 12.5,
                                color: Colors.white,
                                fontWeight: FontWeight.w100),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        message['text'],
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  width: 200.0,
                  decoration: BoxDecoration(
                      color: HexColor('3d5a8a'),
                      borderRadius: BorderRadius.circular(8.0)),
                  margin: EdgeInsets.only(left: 10.0, bottom: 10.0),
                )
              ],
            ),
            Container(
              child: Text(
                 DateFormat('kk:mm - dd/M/yy').format(message['created_at'].toDate()),
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12.0,
                    fontStyle: FontStyle.italic),
              ),
              margin: EdgeInsets.only(left: 50.0, top: 2.0, bottom: 2.0),
            ),
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
        margin: EdgeInsets.only(bottom: 10.0),
      );
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] == _uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] != _uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        color: HexColor('3d5a8a'),

        child: Row(
          children: <Widget>[
            Flexible(
              child: Container(
                margin: EdgeInsets.only(left: 20.0),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,

                  controller: _textController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration:
                      InputDecoration.collapsed(hintText: "Envia un mensaje", focusColor: Colors.black, fillColor: Colors.black),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    color: Theme.of(context).accentColor,
                    icon: Icon(Icons.send),
                    onPressed: () => _handleSubmitted(_textController.text))),
          ],
        ),
      ),
    );
  }
}
