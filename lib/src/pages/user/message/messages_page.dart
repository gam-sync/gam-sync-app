import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/message/chats/chatscreen_page.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/chat_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:intl/intl.dart';

class MessageUserPage extends StatefulWidget {
  MessageUserPage({Key key}) : super(key: key);

  _MessageUserPageState createState() => _MessageUserPageState();
}

class _MessageUserPageState extends State<MessageUserPage> {
  String _uid;

  @override
  void initState() {
    super.initState();
    _getUID();
  }

  _getUID() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = user.uid;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: HexColor('203454'),
        child: StreamBuilder(
          stream: ChatProvider.getConversationsFromUserID(_uid),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return SpinKitDoubleBounce(
                color: Theme.of(context).accentColor,
                size: 50.0,
              );
            }

            return Container(

              child: ListView.builder(
                  itemBuilder: (context, index) {
                    DocumentSnapshot chat = snapshot.data.documents[index];
                    int notseen = 0;

                    var otherUid = ChatProvider.getOtherUID(chat, _uid);

                    return StreamBuilder(
                      stream: UserProvider.getDataUserSnapshots(otherUid),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Container();
                        }

                        DocumentSnapshot user = snapshot.data;

                        return Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          actionExtentRatio: 0.25,
                          child: Container(
                            child: FutureBuilder(
                              future: ChatProvider.getNotSeenFromChatId(
                                  chat.documentID),
                              builder: (context, snapshot) {
                                notseen = snapshot.data;
                                return Container(
                                    decoration: BoxDecoration(
                                      color: HexColor('##243e66')
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        Material(
                                          type: MaterialType.transparency,
                                          child: ListTile(
                                            title: Text(user['name']),
                                            subtitle: Text(chat['last_message']),
                                            trailing: chat['updated_at'] == null
                                                ? Text('')
                                                : Text(DateFormat('dd MMM kk:mm')
                                                    .format(chat['updated_at']
                                                        .toDate())),
                                            leading: GestureDetector(
                                              child: notseen == 0 ||
                                                      notseen == null
                                                  ? _buildPhotoUser(user)
                                                  : Badge(
                                                      child:
                                                          _buildPhotoUser(user),
                                                      badgeColor: Colors.red,
                                                      badgeContent: Text(
                                                          notseen.toString()),
                                                    ),
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ProfilePage(
                                                              uid:
                                                                  user.documentID,
                                                              username:
                                                                  user['name'],
                                                            )));
                                              },
                                            ),
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ChatScreen(
                                                            chatId:
                                                                chat.documentID,
                                                            username:
                                                                user['name'],
                                                            userId:
                                                                user.documentID,
                                                          )));
                                            },
                                          ),
                                        ),
                                        Divider(color: Colors.white12, height: 1,)
                                      ],
                                    ));
                              },
                            ),
                          ),
                          secondaryActions: <Widget>[
                            IconSlideAction(

                              caption: 'Eliminar',
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () =>
                                  ChatProvider.deleteChat(chat.documentID),
                            )
                          ],
                        );
                      },
                    );
                  },
                  itemCount: snapshot.data.documents.length),
            );
          },
        ));
  }

  _buildPhotoUser(DocumentSnapshot user) {
    return Material(
      child: Container(
        child: CachedNetworkImage(
          placeholder: (context, url) => Container(
            child: SpinKitDoubleBounce(
                color: Theme.of(context).accentColor, size: 50.0),
            width: 50.0,
            height: 50.0,
            padding: EdgeInsets.all(10.0),
          ),
          imageUrl: user['photoUrl'],
          width: 40.5,
          height: 40.5,
          fit: BoxFit.cover,
        ),
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(18.0),
      ),
      clipBehavior: Clip.hardEdge,
    );
  }
}
