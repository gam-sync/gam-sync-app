import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:gameapp/src/pages/user/events/event_home_page.dart';
import 'package:gameapp/src/pages/user/message_page.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/pages/user/match_page.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:badges/badges.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class StartUserPage extends StatefulWidget {
  final int index;
  StartUserPage({Key key, this.index}) : super(key: key);
  _StartUserPageState createState() => _StartUserPageState();
}

class _StartUserPageState extends State<StartUserPage> {
  int _currentIndex = 0;
  int count = 0;
  DateTime currentBackPressTime;
  bool isAdmin = false;
  CircularBottomNavigationController _navigationController;

  @override
  void initState() {
    _getCount();
    _navigationController = CircularBottomNavigationController(_currentIndex);
    _isBanned();
    super.initState();
  }

  int subindex;
  StreamSubscription _messageSubs;
  StreamSubscription _bannedSubs;

  final List<Widget> _children = [
    ProfilePage(),
    EventHomePage(),
    MatchPlayers(),
    MessagesPage(),
  ];

  _isBanned() async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    _bannedSubs = Firestore.instance.collection('users').document(currentUser.uid).snapshots().listen((data) {
      if (data.data['banned']) {
        UserProvider.isBanned(context);
      }
    });
    
  }
  

  _getCount() async {
    var currentUser = await UserProvider.getCurrentAuthUser();

    _messageSubs = Firestore.instance
        .collection('chats')
        .where('members', arrayContains: currentUser.uid)
        .snapshots()
        .listen((data) {
      count = 0;
      for (var i = 0; i < data.documents.length; i++) {
        for (var j = 0; j < data.documents[i].data['members'].length; j++) {
          if (currentUser.uid == data.documents[i].data['members'][j]) {
            setState(() {
              count += data.documents[i].data['notseen'][j];
            });
          }
        }
      }
    });
  }

  List<TabItem> tabItems = [];

  Future<bool> _onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      ToastUtil.show("Toca de nuevo para salir", context);
      return Future.value(false);
    }
    return Future.value(true);
  }

  void onTabTapped(int index) async {
    setState(() {
      _isBanned();
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Padding(child: _children[_currentIndex], padding: EdgeInsets.only(bottom: 60.0),),
              Align(
                alignment: Alignment.bottomCenter,
                child: CircularBottomNavigation(
                  List.of([
                    TabItem(Icons.person, "Perfil", HexColor('#375d99'), false),
                    TabItem(Icons.table_chart, "Tablones", HexColor('#375d99'), false),
                    TabItem(Icons.supervised_user_circle, "Emparejar",
                        HexColor('#375d99'), false),
                    TabItem(Icons.chat, "Chat", HexColor('375d99'), true, countBadge: count),
                  ]),
                  selectedCallback: (int selectedPost) {
                    setState(() {
                      this._currentIndex = selectedPost;
                    });
                  },
                  barHeight: 60.0,
                  circleSize: 60.0,
                  barBackgroundColor: HexColor('23395d'),
                  animationDuration: Duration(milliseconds: 300),
                  selectedPos: 1,
                  controller: _navigationController,
                ),
              ),
            ],
          ),
        ));
  }
}
