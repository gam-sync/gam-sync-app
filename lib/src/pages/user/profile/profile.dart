import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/achievements_page.dart';
import 'package:gameapp/src/pages/user/profile/options_page.dart';
import 'package:gameapp/src/pages/user/profile/profile_image_page.dart';
import 'package:gameapp/src/pages/user/profile/seeAllGames_page.dart';
import 'package:gameapp/src/pages/user/profile/tabs/friends_page.dart';
import 'package:gameapp/src/providers/chat_provider.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/providers/xp_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gameapp/src/utils/toast_util.dart';

import '../activity_page.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({this.uid, this.username});
  final String uid;
  final String username;

  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String _uid;
  int friendStatus;
  int _pendingRequest = 0;
  List<Widget> games = [];
  String level = "0";
  int levell = 0;

  @override
  void initState() {
    _initializeProfile();
    if (mounted) {
      super.initState();
    }
  }

  _checkIfFriends() async {
    DocumentSnapshot data = await Firestore.instance
        .collection('users')
        .document(_uid)
        .collection('friends')
        .document(widget.uid)
        .get();
    if (data.exists) {
      if (data['status'] == 0) {
        setState(() {
          friendStatus = 0;
        });
      } else if (data['status'] == 1) {
        setState(() {
          friendStatus = 1;
        });
      }
    }
  }

  _generateGames() async {
    var gamesFromUser = await UserProvider.getGamesFromUser(
        widget.uid == null ? _uid : widget.uid);

    for (int i = 0; i < gamesFromUser.documents.length; i++) {
      if (i >= 3) {
        break;
      }
      var game =
          await UserProvider.getGame(gamesFromUser.documents[i].documentID);

      var userGame = gamesFromUser.documents[i];

      setState(() {
        this.games.add(Container(
            child: GameProvider.getGameWidget(game, userGame, context,
                widget.uid == null ? _uid : widget.uid)));
      });
    }
  }

  _getPendingRequest() async {
    int requestsPending = await UserProvider.getRequestPendingCount();

    setState(() {
      _pendingRequest = requestsPending;
    });
  }

  _getLevel() async {
    var xp = await XProvider.getXP(widget.uid == null ? _uid : widget.uid);
    var level = await XProvider.calculateLevel(xp);
    setState(() {
      this.level = level;
      this.levell = int.parse(level);
    });
  }

  _initializeProfile() async {
    var user = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = user.uid;
      _getLevel();
      if (widget.uid != _uid) {
        _checkIfFriends();
        _getPendingRequest();
        _generateGames();
      }
    });
  }

  _buildAppBar() {
    return _uid == widget.uid || widget.username == null
        ? PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              elevation: 1,
              backgroundColor: HexColor('23395d'),
              actions: <Widget>[
                IconButton(
                  icon: _pendingRequest >= 1
                      ? Badge(
                          child: Icon(Icons.notifications),
                          badgeContent: Text(_pendingRequest.toString()),
                        )
                      : Icon(Icons.notifications),
                  onPressed: () => Navigator.push(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => RequestPage(
                                uid: _uid,
                              ))),
                ),
                IconButton(
                  icon: Icon(Icons.more_vert),
                  onPressed: () => Navigator.pushNamed(context, 'settings'),
                ),
              ],
              title: Text(
                'Mi perfil',
                style: TextStyle(fontSize: 16),
              ),
            ),
          )
        : PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.more_vert),
                    onPressed: () => Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => OptionsPage(
                                  uid: widget.uid,
                                  username: widget.username,
                                )))),
              ],
              elevation: 1,
              backgroundColor: HexColor('23395d'),
              title: Text(
                widget.username,
                style: TextStyle(fontSize: 16),
              ),
            ),
          );
  }

  _buildOptionUser(DocumentSnapshot user) {
    return _uid != user.documentID
        ? Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 10.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    color: Colors.black38,
                    margin: EdgeInsets.only(
                        top: ScreenUtil.instance.setHeight(30.0),
                        left: ScreenUtil.instance.setWidth(28),
                        right: ScreenUtil.instance.setWidth(70)),
                    height: ScreenUtil.instance.setHeight(35),
                    width: 200.0,
                    child: OutlineButton(
                        borderSide: BorderSide(
                          color: Colors.white38, //Color of the border
                          style: BorderStyle.solid, //Style of the border
                          width: 0.8, //width of the border
                        ),
                        child: Container(
                            child: Text(
                          friendStatus == null
                              ? 'Añadir como amigo'
                              : friendStatus == 1
                                  ? 'Eliminar como amigo'
                                  : friendStatus == 0 ? 'Pendiente' : 'Error',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white60),
                        )),
                        onPressed: friendStatus == null
                            ? () {
                                UserProvider.addFriend(user.documentID);
                                setState(() {
                                  friendStatus = 0;
                                });
                              }
                            : friendStatus == 1
                                ? () {
                                    UserProvider.deleteFriend(user.documentID);
                                    setState(() {
                                      friendStatus = null;
                                    });
                                  }
                                : friendStatus == 0 ? null : null),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: EdgeInsets.only(
                      top: ScreenUtil.instance.setHeight(30.0),
                      left: ScreenUtil.instance.setWidth(10.0)),
                  color: Colors.black38,
                  height: ScreenUtil.instance.setHeight(35),
                  width: ScreenUtil.instance.setWidth(60),
                  child: OutlineButton(
                    borderSide: BorderSide(
                      color: Colors.white38, //Color of the border
                      style: BorderStyle.solid, //Style of the border
                      width: 0.8, //width of the border
                    ),
                    child: Icon(
                      Icons.chat,
                      color: (friendStatus == null || friendStatus == 0) &&
                              !user['settings']['allowMessages']
                          ? Colors.grey
                          : Colors.green,
                    ),
                    onPressed: (friendStatus == null || friendStatus == 0) &&
                            !user['settings']['allowMessages']
                        ? () => ToastUtil.show(
                            "Este usuario solo permite mensajes de amigos",
                            context)
                        : () => ChatProvider.createOrOpenChat(context, user),
                  ),
                ),
              ),
            ],
          )
        : Container();
  }

  _navigateToGame() {
    return Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SeeAllGames(
                  username: widget.username,
                  uid: widget.uid == null ? _uid : widget.uid,
                )));
  }

  _buildHeader(DocumentSnapshot user) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/profile/background.jpg'),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.5), BlendMode.dstOut),
          )),
          height: ScreenUtil.instance.setHeight(130),
        ),
        Container(
          padding:
              EdgeInsets.only(top: ScreenUtil.instance.setHeight(80), left: 17),
          child: Container(
            child: GestureDetector(
              child: Card(
                child: GestureDetector(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProfileImagePage(
                                profileUrl: user['photoUrl'],
                              ))),
                  child: Container(
                    width: 90.0,
                    height: 90.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: CachedNetworkImageProvider(user['photoUrl']),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(
                            Radius.circular(ScreenUtil.instance.setWidth(75))),
                        border: Border.all(
                            color: Theme.of(context).accentColor,
                            width: ScreenUtil.instance.setWidth(3.0))),
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            width: 27.0,
                            height: 25.0,
                            decoration: BoxDecoration(
                                color: levell == 1 ? Colors.blueAccent : levell <= 5 ? Colors.green : levell <= 10 ? Colors.green : levell <= 20 ? Colors.deepOrange : levell <= 35 ? Colors.purple : Colors.redAccent,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0))),
                            child: Center(
                                child: user['xp'] != null
                                    ? Text(level)
                                    : Text('1')),
                          ),
                        ),
                      ],
                    )),
                  ),
                ),
                shape: CircleBorder(),
              ),
            ),
          ),
        ),
        Container(
            margin: EdgeInsets.only(
                left: ScreenUtil.instance.setWidth(120),
                top: ScreenUtil.instance.setHeight(140.0)),
            child: Row(
              children: <Widget>[
                Text(
                  user['name'],
                  style: TextStyle(
                      fontSize: ScreenUtil.instance.setSp(20.0),
                      fontWeight: FontWeight.bold,
                      color: user['isAdmin'] != null && user['isAdmin']
                          ? HexColor('ED2939')
                          : Colors.white70),
                ),
                user['isAdmin'] != null && user['isAdmin']
                    ? Container(
                        margin: EdgeInsets.only(top: 10, left: 2),
                        child: Text(
                          '(Admin)',
                          style: TextStyle(
                              fontSize: 12.0, color: HexColor('CD5C5C')),
                        ),
                      )
                    : Container()
              ],
            )),
        Container(
          margin: EdgeInsets.only(
              left: ScreenUtil.instance.setWidth(120),
              top: ScreenUtil.instance.setHeight(170.0)),
          child: Text(
            user['city'] + " - " + user['country'],
          ),
        ),
      ],
    );
  }

  _buildBoxes(DocumentSnapshot user) {
    return Row(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            margin: EdgeInsets.only(
                top: ScreenUtil.instance.setHeight(30.0),
                left: ScreenUtil.instance.setWidth(30.0)),
            color: HexColor('23395d'),
            height: ScreenUtil.instance.setHeight(50),
            width: ScreenUtil.instance.setWidth(100),
            child: OutlineButton(
              borderSide: BorderSide(
                color: Colors.white38, //Color of the border
                style: BorderStyle.solid, //Style of the border
                width: 0.8, //width of the border
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FutureBuilder(
                      future: UserProvider.getAchievementsCount(
                          widget.uid == null ? _uid : widget.uid),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return Text('0');
                        return Container(
                          child: Text(snapshot.data.toString()),
                        );
                      }),
                  Text('Logros')
                ],
              ),
              onPressed:
                  () {} /*=> Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => AchievementsPage(
                            uid: widget.uid == null ? _uid : widget.uid,
                          ))) */
              ,
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            margin: EdgeInsets.only(
                top: ScreenUtil.instance.setHeight(30.0),
                left: ScreenUtil.instance.setWidth(20.0)),
            color: HexColor('23395d'),
            height: ScreenUtil.instance.setHeight(50),
            width: ScreenUtil.instance.setWidth(100),
            child: OutlineButton(
              borderSide: BorderSide(
                color: Colors.white38, //Color of the border
                style: BorderStyle.solid, //Style of the border
                width: 0.8, //width of the border
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FutureBuilder(
                      future: UserProvider.getFriendsCount(
                          widget.uid == null ? _uid : widget.uid),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return Text('0');
                        return Container(
                          child: Text(snapshot.data.toString()),
                        );
                      }),
                  Text('Amigos')
                ],
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FriendsPage(
                              uid: user.documentID,
                              username: user['name'],
                            )));
              },
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            margin: EdgeInsets.only(
              top: ScreenUtil.instance.setHeight(30.0),
              left: ScreenUtil.instance.setWidth(20.0),
            ),
            color: HexColor('23395d'),
            height: ScreenUtil.instance.setHeight(50),
            width: ScreenUtil.instance.setWidth(100),
            child: OutlineButton(
              borderSide: BorderSide(
                color: Colors.white38, //Color of the border
                style: BorderStyle.solid, //Style of the border
                width: 0.8, //width of the border
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FutureBuilder(
                      future: UserProvider.getGamesCount(
                          widget.uid == null ? _uid : widget.uid),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return Text('0');
                        return Container(
                          child: Text(snapshot.data.toString()),
                        );
                      }),
                  Text('Juegos')
                ],
              ),
              onPressed: () => widget.uid == null || friendStatus == 1
                  ? _navigateToGame()
                  : user['settings']['showGames'] != null &&
                          user['settings']['showGames']
                      ? _navigateToGame()
                      : ToastUtil.show(
                          'Este usuario tiene en oculto el apartado Juegos',
                          context),
            ),
          ),
        ),
      ],
    );
  }

  _buildTabBar(DocumentSnapshot user) {
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      child: DefaultTabController(
        length: 2,
        child: Container(
          height: ScreenUtil.instance.setHeight(450.0),
          child: Column(
            children: <Widget>[
              TabBar(
                isScrollable: true,
                indicatorColor: HexColor('50c878'),
                labelColor: Theme.of(context).accentColor,
                tabs: <Widget>[
                  Tab(
                    text: "Información",
                  ),
                  Tab(
                    text: "Juegos",
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                    ListView(
                      scrollDirection: Axis.vertical,
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 10.0,
                                color: Colors.black12,
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    left: 16, top: 16, right: 16),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'Biografía',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              ListTile(
                                title: Text(user['bio']),
                              ),
                              Container(
                                height: 10.0,
                                color: Colors.black12,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 16, top: 16),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'Genero',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              ListTile(
                                title: Text(user['genre']),
                              ),
                              Container(
                                height: 10.0,
                                color: Colors.black12,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    widget.uid == null || friendStatus == 1
                        ? _buildTabGames()
                        : user['settings']['showGames'] != null &&
                                user['settings']['showGames']
                            ? _buildTabGames()
                            : Column(
                                children: <Widget>[
                                  Container(
                                    height: 10.0,
                                    color: Colors.black12,
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 45.0, vertical: 40.0),
                                    child: Text(
                                        'Este usuario tiene en oculto el apartado "Juegos"'),
                                  ),
                                  Container(
                                    height: 10.0,
                                    color: Colors.black12,
                                  ),
                                ],
                              ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildTabGames() {
    return Container(
        child: Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 10.0,
            color: Colors.black12,
          ),
          Container(
            margin: EdgeInsets.all(16),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Juegos',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                for (var game in this.games) game,
                Divider(
                  height: 1.0,
                ),
                Container(
                  color: Colors.black12,
                  child: ListTile(
                    title: Center(child: Text("Ver todos los juegos")),
                    onTap: () => Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => SeeAllGames(
                                  username: widget.username,
                                  uid: widget.uid == null ? _uid : widget.uid,
                                ))),
                  ),
                ),
                Divider(
                  height: 1,
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return Scaffold(
        appBar: _buildAppBar(),
        body: StreamBuilder(
            stream: Firestore.instance
                .collection('users')
                .document(widget.uid == null ? _uid : widget.uid)
                .snapshots(),
            builder: (context, snapshot) {
              var user = snapshot.data;
              if (!snapshot.hasData) {
                return Container(
                    child: Container(
                        child: Center(
                            child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ))));
              }

              return Container(
                color: HexColor('203454'),
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        _buildHeader(user),
                        _buildOptionUser(user),
                        _buildBoxes(user),
                        _buildTabBar(user)
                      ],
                    ),
                  ],
                ),
              );
            }));
  }
}
