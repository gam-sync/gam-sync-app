import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class SeeAllGames extends StatefulWidget {
  final String username;
  final String uid;
  SeeAllGames({Key key, this.username, this.uid}) : super(key: key);

  @override
  _SeeAllGamesState createState() => _SeeAllGamesState();
}

class _SeeAllGamesState extends State<SeeAllGames> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              widget.username != null
                  ? "Juegos de ${widget.username}"
                  : "Mis juegos",
              style: TextStyle(fontSize: 16),
            ),
            actions: <Widget>[
              widget.username == null ? IconButton(
                icon: Icon(Icons.add_circle_outline),
                onPressed: () => Navigator.pushNamed(context, 'addgame'),
              ) : Container()
            ],
          ),
        ),
        body: Container(
          color: HexColor('203454'),
          child: FutureBuilder(
            future: UserProvider.getGamesFromUser(widget.uid),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                    child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ));
              }

              return ListView.builder(
                itemExtent: 75.0,
                itemBuilder: (context, index) {
                  String subtitle = "";
                  var userGame = snapshot.data.documents[index].data;
                  if (userGame['dataToShow'] != null) {
                    for (int i = 0; i < userGame['dataToShow'].length; i++) {
                      if (i < userGame['dataToShow'].length - 1) {
                        subtitle += userGame['dataToShow'][i] + " - ";
                      } else {
                        subtitle += userGame['dataToShow'][i];
                      }
                    }
                  } else {
                    subtitle = "No hay información disponible.";
                  }

                  return FutureBuilder(
                    future: GameProvider.getGame(
                        snapshot.data.documents[index].documentID),
                    builder: (context, snapshot1) {
                      if (!snapshot1.hasData) {
                        return Container();
                      }
                      var game = snapshot1.data;
                      return Column(
                        children: <Widget>[
                          ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: game['asset'] != null
                                  ? AssetImage(game['asset'])
                                  : CachedNetworkImageProvider(game['icon']),
                            ),
                            title: Text(game['name']),
                            subtitle: Text(subtitle),
                          ),
                          Divider(
                            height: 1.0,
                          )
                        ],
                      );
                    },
                  );
                },
                itemCount: snapshot.data.documents.length,
              );
            },
          ),
        ));
  }
}
