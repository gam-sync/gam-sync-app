import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gameapp/src/utils/screen_util.dart';

class ProfileImagePage extends StatefulWidget {
  ProfileImagePage({this.profileUrl});
  final String profileUrl;

  _ProfileImagePageState createState() => _ProfileImagePageState();
}

class _ProfileImagePageState extends State<ProfileImagePage> {
  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return Scaffold(
      body: Center(
        child: Container(
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: CachedNetworkImageProvider(widget.profileUrl),
            fit: BoxFit.fitWidth,
          )),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
