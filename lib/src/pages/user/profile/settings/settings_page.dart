import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gameapp/src/pages/user/admin/admin_start_page.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class SettingsPage extends StatefulWidget {
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool isEmailVerified;
  bool allowMessages = false;
  bool isAdmin = false;

  @override
  void initState() {
    _isAdmin();
    super.initState();
  }

  _isAdmin() async {
    bool isAdmin = await UserProvider.isAdmin();
    if (isAdmin) {
      setState(() {
        this.isAdmin = isAdmin;
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Ajustes de cuenta',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Cuenta',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ))),
              FutureBuilder(
                future: UserProvider.getDataAuthUser(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Container();

                  return Column(
                    children: <Widget>[
                      Material(
                        type: MaterialType.transparency,
                        child: ListTile(
                          title: Row(
                            children: <Widget>[
                              Text(snapshot.data['name'] + " "),
                              snapshot.data['isEmailVerified']
                                  ? Container(
                                      child: Icon(
                                      FontAwesomeIcons.checkCircle,
                                      color: Colors.green,
                                      size: 20.0,
                                    ))
                                  : Container(),
                            ],
                          ),
                          subtitle: RichText(
                            text: TextSpan(
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.white70),
                                children: [
                                  TextSpan(text: snapshot.data['email']),
                                  !snapshot.data['isEmailVerified']
                                      ? TextSpan(
                                          text: " (No verificado)",
                                          style: TextStyle(
                                              color: Colors.redAccent))
                                      : TextSpan(
                                          text: '',
                                        )
                                ]),
                          ),
                          leading: CircleAvatar(
                            backgroundImage: CachedNetworkImageProvider(
                                snapshot.data['photoUrl']),
                          ),
                          trailing: Icon(
                            Icons.arrow_forward_ios,
                            size: 12,
                          ),
                          onTap: () =>
                              Navigator.pushNamed(context, 'editaccount'),
                        ),
                      ),
                      !snapshot.data['isEmailVerified']
                          ? Container(
                              child: Material(
                                type: MaterialType.transparency,
                                child: ListTile(
                                  title: Text(
                                    'Verificar correo electronico',
                                    style: TextStyle(fontSize: 15.0),
                                  ),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 12,
                                  ),
                                  onTap: () => Navigator.pushNamed(
                                      context, 'verifyEmail'),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  );
                },
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: ListTile(
                    title: Text(
                      'Añadir juegos',
                      style: TextStyle(fontSize: 15.0),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                    ),
                    onTap: () => Navigator.pushNamed(context, 'addgame'),
                  ),
                ),
              ),
              Container(
                height: 10.0,
                color: Colors.black26,
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Privacidad y seguridad',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ))),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: ListTile(
                    title: Text(
                      'Privacidad',
                      style: TextStyle(fontSize: 15.0),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                    ),
                    onTap: () => Navigator.pushNamed(context, 'privacy'),
                  ),
                ),
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: ListTile(
                    title: Text(
                      'Seguridad',
                      style: TextStyle(fontSize: 15.0),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                    ),
                    onTap: () {},
                  ),
                ),
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                height: 10.0,
                color: Colors.black26,
              ),
              Container(
                  margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Politicas de términos y condiciones',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ))),
              Material(
                type: MaterialType.transparency,
                child: ListTile(
                  title: Text(
                    'Politicas de privacidad',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 12,
                  ),
                  onTap: () => Navigator.pushNamed(context, 'terminos'),
                ),
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: ListTile(
                    title: Text(
                      'Terminos de servicios',
                      style: TextStyle(fontSize: 15.0),
                    ),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                    ),
                    onTap: () => Navigator.pushNamed(context, 'services'),
                  ),
                ),
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Container(
                height: 10.0,
                color: Colors.black26,
              ),
              isAdmin ? Column(
                children: <Widget>[
                  Container(
                      margin:
                          EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                      child: Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Panel Admin',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ))),
                  Container(
                    child: Material(
                      type: MaterialType.transparency,
                      child: ListTile(
                        title: Text(
                          'Admin',
                          style: TextStyle(fontSize: 15.0),
                        ),
                        trailing: Icon(
                          Icons.arrow_forward_ios,
                          size: 12,
                        ),
                        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AdminStartPage())),
                      ),
                    ),
                  ),
                  Divider(
                    height: 2,
                    color: Colors.white10,
                  ),
                  Container(
                    height: 10.0,
                    color: Colors.black26,
                  ),
                ],
              ) : Container(),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: ListTile(
                    title: Center(
                      child: Text(
                        'Cerrar sesión',
                        style: TextStyle(fontSize: 15.0),
                      ),
                    ),
                    onTap: () => UserProvider.logout(context),
                  ),
                ),
              ),
              Divider(
                height: 2,
                color: Colors.white10,
              ),
              Expanded(
                child: Container(
                  color: Colors.black26,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
