import 'package:flutter/material.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class PrivacyPage extends StatefulWidget {
  PrivacyPage({Key key}) : super(key: key);

  _PrivacyPageState createState() => _PrivacyPageState();
}

class _PrivacyPageState extends State<PrivacyPage> {

  var settings;

  @override
  void initState() { 
    _loadSettings();
    super.initState();
    
  }

  _loadSettings() async {
    var currentUser = await UserProvider.getDataAuthUser();
    setState(() {
      settings = currentUser.data['settings'];
    });
  }

  _allowMessages(bool value) {
    setState(() {
      settings['allowMessages'] = value;
    });
    
    UserProvider.updateUser({'settings.allowMessages': settings['allowMessages']});
  }

  _allowMentions(bool value) {
    setState(() {
      settings['allowMentions'] = value;
    });
    
    UserProvider.updateUser({'settings.allowMentions': settings['allowMentions']});
  }

  _showGames(bool value) {
    setState(() {
      settings['showGames'] = value;
    });
    
    UserProvider.updateUser({'settings.showGames': settings['showGames']});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Privacidad',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Mensajes',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: ListTile(
                title: Text('Recibir mensajes de cualquiera'),
                subtitle: Text(
                    'Recibiras mensajes de cualquier usuario, incluso si no lo tienes añadido.'),
                trailing: Switch(
                  onChanged: (value) => _allowMessages(value),
                  value: settings['allowMessages'] == null ? false : settings['allowMessages'],
                ),
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Juegos',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: ListTile(
                title: Text('Mostrar juegos a cualquiera'),
                subtitle: Text(
                    'Cualquier usuario de Gam Sync podra ver tus juegos, incluso si no lo tienes añadido.'),
                trailing: Switch(
                  onChanged: (value) => _showGames(value),
                  value: settings['showGames'] == null ? false : settings['showGames'],
                ),
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Salas de chat',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: ListTile(
                title: Text('Recibir menciones de cualquiera'),
                subtitle: Text(
                    'Los usuarios podran mencionarte en las salas de chat, incluso si no lo tienes añadido.'),
                trailing: Switch(
                  onChanged: (value) => _allowMentions(value),
                  value: settings['allowMentions'] == null ? false : settings['allowMentions'],
                ),
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
            Container(
              child: ListTile(
                onTap: () => Navigator.pushNamed(context, 'blockedUsers'),
                title: Text('Usuarios bloqueados'),
                trailing: Icon(Icons.arrow_right)
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
          ],
        ),
      ),
    );
  }
}
