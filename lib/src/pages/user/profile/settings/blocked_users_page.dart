import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class BlockedUsersPage extends StatefulWidget {
  BlockedUsersPage({Key key}) : super(key: key);

  @override
  _BlockedUsersPageState createState() => _BlockedUsersPageState();
}

class _BlockedUsersPageState extends State<BlockedUsersPage> {
  var blockedUsers = [];

  @override
  void initState() {
    _getBlockedUsers();
    super.initState();
  }

  _getBlockedUsers() async {
    List<dynamic> blockedUsers1 = await UserProvider.getBlockedUsers();
    setState(() {
      for(int i = 0; i < blockedUsers1.length; i++) {
        blockedUsers.add(blockedUsers1[i]);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Usuarios bloqueados',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
          color: HexColor('203454'),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0, top: 10),
                child: Text('Para desbloquear un usuario simplemente manten pulsado.', style: TextStyle(color: Colors.grey),),
              ),
              ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return FutureBuilder(
                          future:
                              UserProvider.getDataUser(blockedUsers[index]),
                          builder: (context, snapshot1) {
                            if (!snapshot1.hasData) {
                              return SpinKitCircle(
                                color: Colors.white,
                              );
                            }
                            return Container(
                              color: Colors.black12,
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    leading: CircleAvatar(
                                      backgroundImage: CachedNetworkImageProvider(
                                          snapshot1.data['photoUrl']),
                                    ),
                                    title: Text(snapshot1.data['name']),
                                    trailing: Icon(Icons.lock_open),
                                    onLongPress: () {
                                      UserProvider.setUnblock(snapshot1.data.documentID);
                                      ToastUtil.show('Has desbloqueado a ${snapshot1.data['name']}', context);
                                      setState(() {
                                        blockedUsers.remove(blockedUsers[index]);
                                      });
                                    },
                                  ),
                                  Divider(
                                    height: 1,
                                  ),
                                ],
                              ),
                            );
                          });
                    },
                    itemCount: blockedUsers.length,
                  )
            ],
          )),
    );
  }
}

