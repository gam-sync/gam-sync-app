import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class VerifyEmail extends StatefulWidget {
  VerifyEmail({Key key}) : super(key: key);

  _VerifyEmailState createState() => _VerifyEmailState();
}

class _VerifyEmailState extends State<VerifyEmail> {
  bool loading = false;
  bool sended = false;
  bool verified = true;

  int step = 1;

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('¿Quieres salir de la verificación?'),
            content:
                new Text('Aún no has verificado tu cuenta, ¿quieres salir?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Si'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  void initState() {
    super.initState();
  }

  _sendEmail() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    setState(() {
      sended = true;
      step = 2;
    });
    user.sendEmailVerification();
  }

  _verifyEmail() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();

    await user.reload();
    FirebaseUser user1 = await FirebaseAuth.instance.currentUser();
    if (user1.isEmailVerified || user.isEmailVerified) {
      UserProvider.updateUser({'isEmailVerified': true});
      Navigator.pop(context);
      ToastUtil.show(
          "Se ha verificado correctamente tu correo electronico.", context);
    } else {
      ToastUtil.show(
          "No se ha podido verificar tu correo electronico. Comprueba si has dado correctamente al link",
          context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Verificación por correo'),
            centerTitle: true,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Container(
                    padding: EdgeInsets.only(top: 20.0),
                    margin:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10.0),
                    child: Text(
                      'Verificación por correo (Paso $step de 2)',
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    )),
              ),
              sended == true
                  ? Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
                      child: Text(
                        'Ya hemos enviado el email a tu bandeja de correo electronico. Tras darle al link dale al boton de Verificar.',
                        style: TextStyle(color: Colors.white54),
                      ),
                    )
                  : Container(
                      child: Text(
                          'Dale a enviar correo para enviar la verificación por email a tu correo electronico.',
                          style: TextStyle(color: Colors.white54)),
                      margin:
                          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
                    ),
              loading == true
                  ? Container(
                      margin: EdgeInsets.only(top: 20),
                      child: SpinKitDoubleBounce(
                        color: Theme.of(context).accentColor,
                        size: 50.0,
                      ),
                    )
                  : Container(),
              Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
                  width: ScreenUtil.instance.setWidth(150),
                  child: MaterialButton(
                    child: sended == false
                        ? Text('Enviar correo')
                        : Text('Verificar'),
                    color: Theme.of(context).primaryColor,
                    onPressed: sended == false
                        ? () => _sendEmail()
                        : () => _verifyEmail(),
                    elevation: 7.0,
                    height: ScreenUtil.instance.setHeight(42.0),
                    minWidth: double.infinity,
                    shape: StadiumBorder(),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
