import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gameapp/src/models/Country.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/provinces_util.dart';
import 'package:image_picker/image_picker.dart';

class EditAccountPage extends StatefulWidget {
  @override
  _EditAccountPageState createState() => _EditAccountPageState();
}

class _EditAccountPageState extends State<EditAccountPage> {
  String _photoUrl;
  File _image;
  String _uid;
  String _genre;
  String _city;
  String _bio;
  String _country;
  List<Country> countries;

  // currentCountry: To know if a country has been chosen and to show the cities
  Country currentCountry;

  _uploadImage() async {
    if (_image != null) {
      var urlPhotoProfile = await UserProvider.editPhoto(context, _image, _uid);
      UserProvider.updateUser({'photoUrl': urlPhotoProfile});
    }
  }

  _loadCountries() async {
    var countries = await DefaultAssetBundle.of(context).loadString('assets/countries.json');
    List<Country> countriesList = parseJson(countries.toString());
    setState(() {
      this.countries = countriesList; 
    });
  }

  @override
  void initState() {
    _loadCountries();
    _getData();
    super.initState();
  }

  _getData() async {
    var user = await UserProvider.getDataAuthUser();
    setState(() {
      _uid = user.documentID;
      _photoUrl = user.data['photoUrl'];
      _genre = user.data['genre'];
      _city = user.data['city'];
      _bio = user.data['bio'];

      _country = user.data['country'];
      currentCountry = countries.singleWhere((country) => country.countryName == user.data['country']);
    });
  }

  

  Future getImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 75,
        maxHeight: 1080,
        maxWidth: 1920);
    setState(() {
      _image = image;
    });
    await _uploadImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Ajustes de la cuenta',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(12),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Foto de perfil',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Center(
                child: Card(
                  child: GestureDetector(
                    onTap: () => getImage(),
                    child: Container(
                      width: ScreenUtil.instance.setWidth(80.0),
                      height: ScreenUtil.instance.setHeight(90.0),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: _image != null
                                  ? FileImage(_image)
                                  : _photoUrl != null
                                      ? CachedNetworkImageProvider(_photoUrl)
                                      : null,
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.all(Radius.circular(
                              ScreenUtil.instance.setWidth(75))),
                          border: Border.all(
                              color: Theme.of(context).accentColor,
                              width: ScreenUtil.instance.setWidth(3.0))),
                    ),
                  ),
                  shape: CircleBorder(),
                ),
              ),
            ),
            Container(
                child: Row(
              children: <Widget>[],
            )),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Genero y ciudad',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              child: ListTile(
                title: Text(
                  'Genero',
                  style: TextStyle(fontSize: 15.0),
                ),
                trailing: Text(
                  _genre,
                  style: TextStyle(color: HexColor('50c878')),
                ),
                onTap: _buildGenreList,
              ),
            ),
            Divider(
              height: 1,
              color: Colors.white10,
            ),
                        Container(
              child: ListTile(
                title: Text(
                  'País',
                  style: TextStyle(fontSize: 15.0),
                ),
                trailing: Text(
                  _country,
                  style: TextStyle(color: HexColor('50c878')),
                ),
                onTap: _buildCountryList,
              ),
            ),
            Divider(height: 1,
              color: Colors.white10,
            ),
            Container(
              child: ListTile(
                title: Text(
                  'Ciudad',
                  style: TextStyle(fontSize: 15.0),
                ),
                trailing: Text(
                  _city,
                  style: TextStyle(color: HexColor('50c878')),
                ),
                onTap: _buildCityList,
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 10.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Biografia',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              child: ListTile(
                title: Text(
                  'Biografia',
                  style: TextStyle(fontSize: 15.0),
                ),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 10,
                ),
                onTap: _buildBiografia,
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.black26,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildGenreList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: ListView(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 20.0, left: 10.0),
                        child: Text(
                          'Genero',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Column(children: [
                        ListTile(
                          title: Text('Hombre'),
                          onTap: () {
                            setState(() {
                              _genre = "Hombre";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                        Divider(
                          height: 1,
                          color: Colors.white10,
                        ),
                        ListTile(
                          title: Text('Mujer'),
                          onTap: () {
                            setState(() {
                              _genre = "Mujer";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                        ListTile(
                          title: Text('Otro'),
                          onTap: () {
                            setState(() {
                              _genre = "Otro";
                            });
                            UserProvider.updateUser({'genre': _genre});
                            Navigator.pop(context);
                          },
                        ),
                      ])
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  // parseJson: Map the list of countries to display them in a drop-down
  List<Country> parseJson(String response) {
    if (response == null) {
      return [];
    }
    final parsed =
        json.decode(response.toString()).cast<Map<String, dynamic>>();
    return parsed.map<Country>((json) => new Country.fromJson(json)).toList();
  }

  _buildCountryList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
              height: ScreenUtil.instance.setHeight(300.0),
              color: HexColor('23395d'),
              child: FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('assets/countries.json'),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Container();
                  List<Country> countries = parseJson(snapshot.data.toString());
                  if (countries == null) {
                    return Container();
                  }

                  return ListView.builder(
                    itemCount: countries.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(countries[index].countryName),
                        onTap: () {
                          setState(() {
                            _country = countries[index].countryName;
                            UserProvider.updateUser({'country': _country});
                            currentCountry = countries[index];
                          });
                          Navigator.pop(context);
                        },
                      );
                    },
                  );
                },
              ));
        });
  }

  _buildCityList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
              height: ScreenUtil.instance.setHeight(300.0),
              color: HexColor('23395d'),
              child: ListView.builder(
                itemCount: currentCountry.regions.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(currentCountry.regions[index].name),
                    onTap: () {
                      setState(() {
                        _city = currentCountry.regions[index].name;
                        UserProvider.updateUser({'city': _city});
                      });
                      Navigator.pop(context);
                    },
                  );
                },
              ));
        });
  }

  _buildBiografia() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(900.0),
            color: HexColor('23395d'),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(
                              top: 20.0, left: 10.0, bottom: 10),
                          child: Text(
                            'Biografia',
                            style: TextStyle(
                                color: Colors.white54,
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: SingleChildScrollView(
                              child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 4,
                            initialValue: _bio,
                            validator: (value) => value.isEmpty
                                ? 'La biografia no puede estar vacia'
                                : null,
                            decoration: InputDecoration(
                              errorStyle: TextStyle(color: Colors.red),
                              filled: true,
                              contentPadding: new EdgeInsets.symmetric(
                                  vertical: 12.0, horizontal: 10.0),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.transparent, width: 0.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.transparent, width: 0.0),
                              ),
                              fillColor: Colors.black38,
                              hintText: 'Biografia',
                            ),
                            onChanged: (value) => _bio = value,
                          )),
                        ),
                        Center(
                          child: MaterialButton(
                            child: Container(
                                child: Text(
                              ' Guardar nueva biografía',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: () {
                              Navigator.pop(context);
                              UserProvider.updateUser({'bio': _bio});
                            },
                            elevation: 4.0,
                            minWidth: 150,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
