import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class EditUser extends StatefulWidget {
  EditUser({Key key}) : super(key: key);

  _EditUserState createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  var _namecontroller = TextEditingController();

  final formKey = new GlobalKey<FormState>();

  bool namechange = false;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate() && !namechange) {
      form.save();
      Map<String, dynamic> data = {
        'name': _namecontroller.text,
        'settings.nameChanged': true
      };
      UserProvider.updateUser(data);

      setState(() {
        namechange = true;
      });

      ToastUtil.show("Se ha actualizado correctamente", context);
    }
    return false;
  }

  @override
  void initState() {
    _getName();
    super.initState();
  }

  _getName() async {
    var user = await UserProvider.getDataAuthUser();
    _namecontroller.text = user.data['name'];

    if (user.data['settings']['nameChanged'] == null) {
      UserProvider.updateUser({'settings.nameChanged': false});
      setState(() {
        namechange = false;
      });
    } else {
      setState(() {
        namechange = user.data['settings']['nameChanged'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nombre'),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(left: 17.0, top: 20.0),
              child: Text(
                'Nombre',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              )),
          Container(
            margin: EdgeInsets.only(left: 17.0, top: 8.0),
            child: Text(
              'Escribe el nombre que veran los demas usuarios.',
              style: TextStyle(color: Colors.white38),
            ),
          ),
          Form(
            key: formKey,
            child: Container(
                margin: EdgeInsets.only(
                    top: 15.0, right: 15.0, left: 15.0, bottom: 10.0),
                child: TextFormField(
                    style: namechange == true
                        ? Theme.of(context)
                            .textTheme
                            .subhead
                            .copyWith(color: Theme.of(context).disabledColor)
                        : null,
                    controller: _namecontroller,
                    maxLines: null,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    enabled: namechange == false ? true : false,
                    validator: (value) => value.isEmpty
                        ? 'Tienes que introducir un nombre'
                        : null,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.black26,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(
                            ScreenUtil.instance.setWidth(5)),
                      ),
                      hintText: 'Biografia',
                    ),
                    onSaved: (value) {
                      _namecontroller.text = value;
                    })),
          ),
          Container(
            margin: EdgeInsets.only(left: 17.0, bottom: 20.0),
            child: Text('* Solo puedes cambiarlo una vez.',
                style: TextStyle(color: Colors.white54)),
          ),
          namechange == false
              ? Center(
                  child: Container(
                    width: ScreenUtil.instance.setWidth(150),
                    child: MaterialButton(
                      child: Text('Actualizar nombre'),
                      color: Theme.of(context).primaryColor,
                      onPressed: () => validateAndSave(),
                      elevation: 7.0,
                      height: ScreenUtil.instance.setHeight(42.0),
                      minWidth: double.infinity,
                      shape: StadiumBorder(),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
