import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/games/Lol_provider.dart';
import 'package:gameapp/src/providers/games/mine_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class MinePage extends StatefulWidget {
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {
  String _server;
  String _serverId;
  String _username;
  bool _isVerified;
  String _gameIcon;
  String _uid;

  @override
  void initState() {
    _getIsVerified();
    _getGameIcon();
    super.initState();
  }

  _getIsVerified() async {
    bool isVerified = await MineProvider.checkIfUserHasAccountVerified();
    setState(() {
      _isVerified = isVerified;
    });
    if (isVerified) {
      _getUid();
    }
  }

  _getGameIcon() async {
    String gameIcon = await MineProvider.getMineIcon();
    setState(() {
      _gameIcon = gameIcon;
    });
  }

  _getUid() async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = currentUser.uid;
    });
  }

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var verify = await MineProvider.getUUIDFromUMine(context, _username);
      bool isVerified = await verify;
      if (isVerified) {
        setState(() {
          _isVerified = true;
        });
      }
    }
  }

  Widget _isVerifiedWidget() {
    return Container(
      color: HexColor('23395d'),
      child: Column(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20.0),
              width: ScreenUtil.instance.setWidth(75.0),
              height: ScreenUtil.instance.setHeight(75.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(_gameIcon), fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Minecraft',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Aqui podras actualizar los datos o borrar el usuario.',
              style: TextStyle(color: Colors.white54),
            ),
          ),
          Container(
            height: 10,
            color: Colors.black12,
          ),
          FutureBuilder(
            future: MineProvider.getDataOfTheUserMine(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              return ListTile(
                title: Text(snapshot.data['username']),
                subtitle: Text('Cuenta vinculada'),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  onTap: () {
                    setState(() {
                      _isVerified = false;
                    });
                    MineProvider.deleteUserMine();
                  },
                ),
                leading: CachedNetworkImage(
                  placeholder: (context, url) => Container(
                    child: SpinKitDoubleBounce(
                        color: Theme.of(context).accentColor, size: 50.0),
                    width: 35.0,
                    height: 35.0,
                    padding: EdgeInsets.all(10.0),
                  ),
                  imageUrl: snapshot.data['icon'],
                  imageBuilder: (context, imageProvider) => CircleAvatar(
                    backgroundImage: CachedNetworkImageProvider(snapshot.data['icon']),
                  ),
                ),
              );
            },
          ),
          Expanded(
            child: Container(
              color: Colors.black12,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              'Minecraft',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
        body: _isVerified == null
            ? Center(
                child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ),
              )
            : !_isVerified
                ? Container(
                    color: HexColor('203454'),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(12),
                          child: Text(
                            'Para conectar tu cuenta de Minecraft solo tienes que introducir tu nombre de usuario actual.',
                            style: TextStyle(color: Colors.white70),
                          ),
                        ),
                        Divider(
                          height: 0.1,
                          color: Colors.white10,
                          endIndent: 10.0,
                          indent: 10.0,
                        ),
                        Form(
                          key: formKey,
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'El nombre de usuario esta vacio';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText: 'Nombre de usuario',
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    _username = value;
                                  });
                                },
                                onSaved: (value) => _username = value),
                          ),
                        ),
                        Container(
                          color: Colors.black26,
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          height: ScreenUtil.instance.setHeight(45),
                          child: OutlineButton(
                            child: Container(
                                child: Text(
                              ' Conectar cuenta',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: _username != null && _username.isNotEmpty
                                ? validateAndSubmit
                                : null,
                          ),
                        ),
                      ],
                    ),
                  )
                : _isVerifiedWidget());
  }
}
