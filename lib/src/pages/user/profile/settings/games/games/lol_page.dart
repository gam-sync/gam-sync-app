import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/games/Lol_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:timeago/timeago.dart' as timeago;

class LolPage extends StatefulWidget {
  _LolPageState createState() => _LolPageState();
}

class _LolPageState extends State<LolPage> {
  String _server;
  String _serverId;
  String _username;
  bool _isVerified;
  String _gameIcon;
  String _uid;
  String _icon;
  String lane;
  String username;
  String userId;

  @override
  void initState() {
    _getIsVerified();
    _getGameIcon();
    _getDataUser();
    super.initState();
  }

  _getDataUser() async {
   var user = await UserProvider.getDataAuthUser();
   setState(() {
    userId = user.documentID;
    username = user.data['name'];
   });

  }

  _getIsVerified() async {
    bool isVerified = await LolProvider.checkIfUserHasAccountVerified();
    setState(() {
      _isVerified = isVerified;
    });
    if (isVerified) {
      _getUid();
    }
  }

  _getGameIcon() async {
    String gameIcon = await LolProvider.getLoLIcon();
    setState(() {
      _gameIcon = gameIcon;
    });
  }

  _getUid() async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = currentUser.uid;
    });
  }

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var verify = await LolProvider.checkUsernameExists(_serverId, _username, _server, lane, context);
      bool isVerified = await verify;
      if(isVerified) {
        List<String> playerIds;
        //Os.sendNotification(username, 4, playerIds, game: 'League of Legends', gameId: 'lol', userId: userId);
        Navigator.of(context).pop();
      }
      
    }
  }

  Widget _isVerifiedWidget() {
    return Container(
      color: HexColor('23395d'),
      child: Column(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20.0),
              width: ScreenUtil.instance.setWidth(75.0),
              height: ScreenUtil.instance.setHeight(75.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(_gameIcon), fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'League of Legends',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Aqui podras actualizar los datos o borrar el usuario.',
              style: TextStyle(color: Colors.white54),
            ),
          ),
          Container(
            height: 10,
            color: Colors.black12,
          ),
          FutureBuilder(
            future: LolProvider.getDataOfTheUserLoL(_uid),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              var nextRef = snapshot.data['nextRefresh'];
              var nextRefresh = DateTime.now();
              if(nextRef != null) {
                nextRefresh = DateTime.fromMillisecondsSinceEpoch(nextRef.seconds * 1000);
              } else {
                nextRefresh = DateTime.fromMillisecondsSinceEpoch(1000);
              }
              var now = DateTime.now();
              return ListTile(
                title: Text(snapshot.data['profile']['name']),
                subtitle: Text('Cuenta vinculada'),
                trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(Icons.delete, color: Colors.redAccent,),
                      onTap: () {
                        setState(() {
                          _isVerified = false;
                        });
                        LolProvider.deleteUserLoL();
                      },
                    ),
                     GestureDetector(
                      child: Icon(Icons.refresh, color: nextRefresh.millisecondsSinceEpoch < now.millisecondsSinceEpoch ? Colors.greenAccent : Colors.grey),
                      onTap: nextRefresh.millisecondsSinceEpoch < now.millisecondsSinceEpoch ? () async {
                        var icon = await LolProvider.refreshData(context);
                        setState(() {
                          _icon = icon;
                        });
                      } : () => ToastUtil.show("Podras actualizar tu usuario " + timeago.format(nextRefresh, locale: 'es', allowFromNow: true), context)
                    )
                  ],
                ),
                leading: CachedNetworkImage(
                  placeholder: (context, url) => Container(
                    child: SpinKitDoubleBounce(
                        color: Theme.of(context).accentColor, size: 50.0),
                    width: 35.0,
                    height: 35.0,
                    padding: EdgeInsets.all(10.0),
                  ),
                  imageUrl: _icon == null ? snapshot.data['icon'] : _icon,
                  imageBuilder: (context, imageProvider) => CircleAvatar(
                    backgroundImage: CachedNetworkImageProvider(_icon == null ? snapshot.data['icon'] : _icon),
                  ),
                ),
              );
            },
          ),
          Expanded(
            child: Container(
              color: Colors.black12,
            ),
          )
        ],
      ),
    );
  }

  _buildLaneList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: ListView(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 20.0, left: 10.0),
                        child: Text(
                          'Lineas',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Column(
                        children: [
                          ListTile(
                            title: Text('Toplaner'),
                            onTap: () {
                             setState(() {
                                lane = "Toplaner";
                              });
                              Navigator.pop(context);
                            },
                          ),
                          Divider(height: 1, color: Colors.white10,),
                          ListTile(
                            title: Text('Jungle'),
                            onTap: () {
                             setState(() {
                                lane = "Jungle";
                              });
                              Navigator.pop(context);
                            },
                          ),
                          ListTile(
                            title: Text('Midlaner'),
                            onTap: () {
                             setState(() {
                                lane = "Midlaner";
                              });
                              Navigator.pop(context);
                            },
                          ),
                          ListTile(
                            title: Text('ADC'),
                            onTap: () {
                             setState(() {
                                lane = "ADC";
                              });
                              Navigator.pop(context);
                            },
                          ),
                          ListTile(
                            title: Text('Support'),
                            onTap: () {
                             setState(() {
                                lane = "Suport";
                              });
                              Navigator.pop(context);
                            },
                          ),
                        ]

                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  _buildListServer(BuildContext context) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: FutureBuilder(
              future: LolProvider.getServerDocuments(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                      child: SpinKitDoubleBounce(
                    color: Theme.of(context).accentColor,
                    size: 50.0,
                  ));

                return ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    var server = snapshot.data.documents[index];
                    return Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(
                            server['name'],
                            style: TextStyle(fontSize: 14.0),
                          ),
                          onTap: () {
                            setState(() {
                              _serverId = server.documentID;
                              _server = server['name'];
                            });
                            Navigator.pop(context);
                          },
                        ),
                        Divider(
                          height: 1.0,
                          color: Colors.white10,
                        )
                      ],
                    );
                  },
                );
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              'League of Legends',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
        body: _isVerified == null
            ? Center(
                child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ),
              )
            : !_isVerified
                ? Container(
                    color: HexColor('203454'),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 12.0),
                          width: ScreenUtil.instance.setWidth(325.0),
                          height: ScreenUtil.instance.setHeight(75.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8.0)),
                            image: DecorationImage(
                                image: CachedNetworkImageProvider(
                                    'https://firebasestorage.googleapis.com/v0/b/gameapp-99e0b.appspot.com/o/games%2Flol%2Fverification_user.png?alt=media&token=dc7a6988-6dbe-4186-aff4-08fcb7cd4f0e'),
                                fit: BoxFit.cover),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          child: Text(
                            'Para conectar tu cuenta de League of Legends tendras ponerte la foto de perfil que hay asignada arriba. Despues rellenas los campos inferiores y tu cuenta se validara en segundos.',
                            style: TextStyle(color: Colors.white70),
                          ),
                        ),
                        ListTile(
                          title: Text('Seleciona un servidor'),
                          trailing: _server == null
                              ? Text(
                                  "No has seleccionado ningun servidor",
                                  style: TextStyle(color: Colors.red),
                                )
                              : Text(
                                  _server,
                                  style: TextStyle(color: Colors.green),
                                ),
                          onTap: () => _buildListServer(context),
                        ),
                        Divider(
                          height: 0.1,
                          color: Colors.white10,
                          endIndent: 10.0,
                          indent: 10.0,
                        ),
                        Form(
                          key: formKey,
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'El nombre de usuario esta vacio';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText: 'Nombre de usuario',
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    _username = value;
                                  });
                                },
                                onSaved: (value) => _username = value),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 5, right: 20, left: 20, bottom: 20),
                            child: TextFormField(
                                initialValue: lane,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'La linea no puede estar vacia';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText: lane == null ? 'Linea' : lane,
                                ),
                                onTap: () => _buildLaneList(),
                                onSaved: (value) => _username = value),
                          ),
                        Container(
                          color: Colors.black26,
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          height: ScreenUtil.instance.setHeight(45),
                          child: OutlineButton(
                            child: Container(
                                child: Text(
                              ' Conectar cuenta',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: _server != null && _username != null && _username.isNotEmpty && lane != null
                                ? validateAndSubmit
                                : null,
                          ),
                        ),
                      ],
                    ),
                  )
                : _isVerifiedWidget());
  }
}
