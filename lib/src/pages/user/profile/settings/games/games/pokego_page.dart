import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/games/Lol_provider.dart';
import 'package:gameapp/src/providers/games/pokego_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:cloud_firestore/cloud_firestore.dart';

class PokegoPage extends StatefulWidget {
  _PokegoPageState createState() => _PokegoPageState();
}

class _PokegoPageState extends State<PokegoPage> {
  String codeTrainer;
  String team;
  String _uid;
  bool _isVerified;

  @override
  void initState() {
    _getIsVerified();
    super.initState();
  }

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  _getUid() async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = currentUser.uid;
    });
  }

  _getIsVerified() async {
    bool isVerified = await PokegoProvider.checkIfUserHasAccountVerified();
    setState(() {
      _isVerified = isVerified;
    });
    if (isVerified) {
      _getUid();
    }
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var verify = await PokegoProvider.addGame(codeTrainer, team);
      bool isVerified = await verify;
      if (isVerified) {
        Navigator.of(context).pop();
      }
    }
  }

  Widget _isVerifiedWidget() {
    return Container(
      color: HexColor('23395d'),
      child: Column(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20.0),
              width: ScreenUtil.instance.setWidth(75.0),
              height: ScreenUtil.instance.setHeight(75.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/games/pokego.jpg'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Pokemon GO',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Aqui puedes borrar tu usuario de Pokemon GO',
              style: TextStyle(color: Colors.white54),
            ),
          ),
          Container(
            height: 10,
            color: Colors.black12,
          ),
          FutureBuilder(
            future: PokegoProvider.getDataOfTheUserPokeGo(_uid),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              return ListTile(
                  title: Text(snapshot.data['codeTrainer']),
                  subtitle: Text('Cuenta vinculada'),
                  trailing: GestureDetector(
                    child: Icon(
                      Icons.delete,
                      color: Colors.redAccent,
                    ),
                    onTap: () {
                      setState(() {
                        _isVerified = false;
                      });
                      PokegoProvider.deleteUserPokeGo();
                    },
                  ),
                  leading: Container(
                    width: 35,
                    height: 35,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: snapshot.data['team']
                              .toString()
                              .contains('Valor')
                          ? AssetImage('assets/games/pokego/teams/valor.png')
                          : snapshot.data['team'].toString().contains('Mystic')
                              ? AssetImage(
                                  'assets/games/pokego/teams/mystic.jpg')
                              : AssetImage(
                                  'assets/games/pokego/teams/instinct.png'),
                    ),
                  ));
            },
          ),
          Expanded(
            child: Container(
              color: Colors.black12,
            ),
          )
        ],
      ),
    );
  }

  _buildLaneList() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: ListView(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 20.0, left: 10.0),
                        child: Text(
                          'Equipos',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Column(children: [
                        ListTile(
                          title: Text('Team Valor'),
                          onTap: () {
                            setState(() {
                              team = "Team Valor";
                            });
                            Navigator.pop(context);
                          },
                        ),
                        Divider(
                          height: 1,
                          color: Colors.white10,
                        ),
                        ListTile(
                          title: Text('Team Mystic'),
                          onTap: () {
                            setState(() {
                              team = "Team Mystic";
                            });
                            Navigator.pop(context);
                          },
                        ),
                        ListTile(
                          title: Text('Team Instinct'),
                          onTap: () {
                            setState(() {
                              team = "Team Instinct";
                            });
                            Navigator.pop(context);
                          },
                        ),
                      ])
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              'Pokemon GO',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
        body: _isVerified == null
            ? Center(
                child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ),
              )
            : !_isVerified
                ? Container(
                    color: HexColor('203454'),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 12.0),
                          width: ScreenUtil.instance.setWidth(325.0),
                          height: ScreenUtil.instance.setHeight(75.0),
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            image: DecorationImage(
                                image: CachedNetworkImageProvider(
                                    'https://i.gyazo.com/0f4c83a9b4c4f4a8f8594cb0e0103308.png'),
                                fit: BoxFit.cover),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          child: Text(
                            'Para conectar tu cuenta de Pokemon GO solo tienes que introducir tu codigo de entrenador y tu equipo.',
                            style: TextStyle(color: Colors.white70),
                          ),
                        ),
                        Divider(
                          height: 0.1,
                          color: Colors.white10,
                          endIndent: 10.0,
                          indent: 10.0,
                        ),
                        Form(
                          key: formKey,
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'El codigo de entrenador esta vacio';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText: 'Codigo de entrenador',
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    codeTrainer = value;
                                  });
                                },
                                onSaved: (value) => codeTrainer = value),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 5, right: 20, left: 20, bottom: 20),
                          child: TextFormField(
                              initialValue: team,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'El team no puede estar vacio';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                errorStyle: TextStyle(color: Colors.red),
                                filled: true,
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                fillColor: Colors.black38,
                                hintText: team == null ? 'Team' : team,
                              ),
                              onTap: () => _buildLaneList(),
                              onSaved: (value) => codeTrainer = value),
                        ),
                        Container(
                          color: Colors.black26,
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          height: ScreenUtil.instance.setHeight(45),
                          child: OutlineButton(
                            child: Container(
                                child: Text(
                              ' Conectar cuenta',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: codeTrainer != null &&
                                    codeTrainer.isNotEmpty &&
                                    team != null
                                ? validateAndSubmit
                                : null,
                          ),
                        ),
                      ],
                    ),
                  )
                : _isVerifiedWidget());
  }
}
