import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/providers/games/mine_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class DefaultAddPage extends StatefulWidget {
  final String gameId;
  final String gameName;
  DefaultAddPage({Key key, this.gameId, this.gameName}) : super(key: key);

  @override
  _DefaultAddPageState createState() => _DefaultAddPageState();
}

class _DefaultAddPageState extends State<DefaultAddPage> {
  bool _isVerified;
  String _uid;
  String _username;
  String _gameIcon;

  @override
  void initState() {
    super.initState();
    _getIsVerified();
    _getGameIcon();
  }

  _getIsVerified() async {
    bool isVerified =
        await GameProvider.checkIfUserHasAccountVerified(widget.gameId);
    setState(() {
      _isVerified = isVerified;
    });
    if (isVerified) {
      _getUid();
    }
  }

  _getGameIcon() async {
    String gameIcon = await GameProvider.getIconGame(widget.gameId);
    setState(() {
      _gameIcon = gameIcon;
    });
  }

  _getUid() async {
    var currentUser = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = currentUser.uid;
    });
  }

  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      var verify = await GameProvider.addGameInUser(_username, widget.gameId);
      bool isVerified = await verify;
      if (isVerified) {
        setState(() {
          _isVerified = true;
        });
      }
    }
  }

  _isVerifiedWidget() {
    return Container(
      color: HexColor('23395d'),
      child: Column(
        children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20.0),
              width: ScreenUtil.instance.setWidth(75.0),
              height: ScreenUtil.instance.setHeight(75.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(_gameIcon), fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              widget.gameName,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10.0),
            child: Text(
              'Aqui podras actualizar los datos o borrar el usuario.',
              style: TextStyle(color: Colors.white54),
            ),
          ),
          Container(
            margin:
                EdgeInsets.only(top: 10, bottom: 10.0, left: 20.0, right: 20.0),
            child: Text(
              '${widget.gameName} aún no ha sido implementado de forma definitiva pero te damos la opcion de añadirlo. Es posible que puedan ocurrir errores.',
              style: TextStyle(color: Colors.red),
            ),
          ),
          Container(
            height: 10,
            color: Colors.black12,
          ),
          FutureBuilder(
            future: GameProvider.getDataOfTheUserGame(widget.gameId),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              return ListTile(
                title: Text(snapshot.data['dataToShow'][0]),
                subtitle: Text('Cuenta vinculada'),
                trailing: GestureDetector(
                  child: Icon(
                    Icons.delete,
                    color: Colors.redAccent,
                  ),
                  onTap: () {
                    setState(() {
                      _isVerified = false;
                    });
                    GameProvider.deleteUserGame(widget.gameId);
                  },
                ),
              );
            },
          ),
          Expanded(
            child: Container(
              color: Colors.black12,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            backgroundColor: HexColor('23395d'),
            title: Text(
              widget.gameName,
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
        body: _isVerified == null
            ? Center(
                child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ),
              )
            : !_isVerified
                ? Container(
                    color: HexColor('203454'),
                    child: Column(
                      children: <Widget>[
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(top: 20.0),
                            width: ScreenUtil.instance.setWidth(75.0),
                            height: ScreenUtil.instance.setHeight(75.0),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: CachedNetworkImageProvider(_gameIcon),
                                  fit: BoxFit.cover),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12),
                          child: Text(
                            'Para conectar tu cuenta de ${widget.gameName} solo tienes que introducir tu nombre de usuario.',
                            style: TextStyle(color: Colors.white70),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 10, bottom: 10.0, left: 12.0, right: 12.0),
                          child: Text(
                            '${widget.gameName} aún no ha sido implementado de forma definitiva pero te damos la opcion de añadirlo. Es posible que puedan ocurrir errores.',
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Divider(
                          height: 0.1,
                          color: Colors.white10,
                          endIndent: 10.0,
                          indent: 10.0,
                        ),
                        Form(
                          key: formKey,
                          child: Container(
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'El nombre de usuario esta vacio';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle: TextStyle(color: Colors.red),
                                  filled: true,
                                  contentPadding: new EdgeInsets.symmetric(
                                      vertical: 12.0, horizontal: 10.0),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 0.0),
                                  ),
                                  fillColor: Colors.black38,
                                  hintText: 'Nombre de usuario',
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    _username = value;
                                  });
                                },
                                onSaved: (value) => _username = value),
                          ),
                        ),
                        Container(
                          color: Colors.black26,
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          height: ScreenUtil.instance.setHeight(45),
                          child: OutlineButton(
                            child: Container(
                                child: Text(
                              ' Conectar cuenta',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: _username != null && _username.isNotEmpty
                                ? validateAndSubmit
                                : null,
                          ),
                        ),
                      ],
                    ),
                  )
                : _isVerifiedWidget());
  }
}
