import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class AddGamePage extends StatefulWidget {
  AddGamePage({Key key}) : super(key: key);

  _AddGamePageState createState() => _AddGamePageState();
}

class _AddGamePageState extends State<AddGamePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Añadir juegos',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: StreamBuilder(
              stream: GameProvider.getAllGames(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                      child: SpinKitDoubleBounce(
                    color: Theme.of(context).accentColor,
                    size: 50.0,
                  ));

                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    var game = snapshot.data.documents[index];
                    return Column(
                      children: <Widget>[
                        Material(
                          type: MaterialType.transparency,
                          child: ListTile(
                            title: Text(
                              game['name'],
                              style: TextStyle(fontSize: 14.0),
                            ),
                            leading: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: game['asset'] != null ? AssetImage(game['asset']) : CachedNetworkImageProvider(game['icon'])
                            ),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              size: 12.0,
                            ),
                            onTap: () => GameProvider.sendToGame(game['route'], game.documentID, context, gameName: game['name']),
                          ),
                        ),
                        Divider(
                          height: 1.0,
                          color: Colors.white10,
                        )
                      ],
                    );
                  },
                );
              },
            ),
      ),
    );
  }
}
