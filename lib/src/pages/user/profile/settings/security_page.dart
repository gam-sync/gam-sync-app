import 'package:flutter/material.dart';
import 'package:gameapp/src/utils/hex_util.dart';


class SecurityPage extends StatefulWidget {
  SecurityPage({Key key}) : super(key: key);

  @override
  _SecurityPageState createState() => _SecurityPageState();
}

class _SecurityPageState extends State<SecurityPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Cambiar contraseña',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        
      ),
    );
  }
}