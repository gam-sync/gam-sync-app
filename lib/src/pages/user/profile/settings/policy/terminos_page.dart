import 'package:flutter/material.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TerminosPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Politicas de privacidad',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        child: WebView(
          initialUrl: "https://anicdevelopment.com/privacy_policy.html",
        ),
      ),
    );
  }
}
