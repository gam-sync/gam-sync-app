import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/providers/xp_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

import '../profile.dart';

class FriendsPage extends StatefulWidget {
  FriendsPage({this.uid, this.username});
  final String uid;
  final String username;

  _FriendsPageState createState() => _FriendsPageState();
}

class _FriendsPageState extends State<FriendsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Amigos de ${widget.username}',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: FutureBuilder(
          future: UserProvider.getFriends(widget.uid),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(
                  child: SpinKitDoubleBounce(
                color: Theme.of(context).accentColor,
                size: 50.0,
              ));

            if (snapshot.data.documents.length == 0) {
              return Center(child: Text(widget.username + " no tiene amigos.", style: TextStyle(fontSize: 16.0),));
            }

            return ListView.builder(
              itemExtent: 60.0,
              shrinkWrap: true,
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) {
                var friend = snapshot.data.documents[index];
                return FutureBuilder(
                  future: UserProvider.getDataUser(friend.documentID),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();                    
                    return Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(snapshot.data['name']),

                          leading: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: CachedNetworkImageProvider(
                                  snapshot.data['photoUrl'])),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage(
                                          uid: snapshot.data.documentID,
                                          username: snapshot.data['name'],
                                        )));
                          },
                        ),
                        Divider(
                          height: 1,
                          color: Colors.white10,
                        )
                      ],
                    );
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }
}
