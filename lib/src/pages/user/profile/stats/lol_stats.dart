import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/models/games/LeagueOfLegends.dart';
import 'package:gameapp/src/providers/games/Lol_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

import '../profile_image_page.dart';

class LolStats extends StatefulWidget {
  final String uid;

  LolStats({Key key, @required this.uid}) : super(key: key);

  @override
  _LolStatsState createState() => _LolStatsState();
}

class _LolStatsState extends State<LolStats> {
  bool loaded = false;

  ProfileLoL profileLoL;
  MatchList matchList;
  var userGame;
  var ranked5;

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  _loadDivision() {
        if (userGame['queues'] != null) {
          for (int i = 0; i < userGame['queues'].length; i++) {
            if (userGame['queues'][i]['queueType'] == "RANKED_SOLO_5x5") {
              this.ranked5 = userGame['queues'][i];
            }
          }
        }
  }

  _loadData() async {
    ProfileLoL profileLoL =
        await LolProvider.getProfileFromAPI(uid: widget.uid);
    
    var userGame = await LolProvider.getDataOfTheUserLoL(widget.uid);

    if (profileLoL != null) {
      if (userGame != null) {
        this.userGame = userGame;
        await _loadDivision();
        MatchList matchListLoL = await LolProvider.getMatches(
            userGame['server']['id'], profileLoL.accountId);
        if (matchListLoL != null) {
          
          setState(() {
            this.profileLoL = profileLoL;
            this.matchList = matchList;
            this.userGame = userGame;
            loaded = true;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize:
                loaded ? Size.fromHeight(100.0) : Size.fromHeight(50.0),
            child: AppBar(
              bottom: loaded
                  ? TabBar(
                      indicatorColor: Colors.yellow,
                      tabs: [
                        Tab(text: 'Inicio'),
                        Tab(text: 'Partidas'),
                        Tab(text: 'Campeones'),
                      ],
                    )
                  : null,
              elevation: 1,
              centerTitle: true,
              backgroundColor: HexColor('23395d'),
              title: Text(
                'Estadisticas',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
          body: !loaded
              ? Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: CachedNetworkImageProvider(
                        'http://lol-wallpapers.com/wp-content/uploads/2017/01/Snowdown-Background-League-of-Legends-Artwork-Wallpaper-lol.jpg'),
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.2), BlendMode.dstATop),
                  )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Image.network(
                          'https://lolstatic-a.akamaihd.net/frontpage/apps/prod/playnow-global/es_ES/fc1312b6f05f6bbf465d387efbc5860767f01f30/assets/img/lol-logo.png',
                          width: 300,
                        ),
                      ),
                      SpinKitRing(
                        size: 50.0,
                        color: Colors.white,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(
                            'Gam Sync isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.'),
                      )
                    ],
                  ),
                )
              : Container(
                  width: MediaQuery.of(context).size.width,
                  color: HexColor('0D2533'),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 15.0, top: 15.0),
                            child: Card(
                              child: GestureDetector(
                                child: Card(
                                  child: GestureDetector(
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ProfileImagePage(
                                                  profileUrl: userGame['icon'],
                                                ))),
                                    child: Container(
                                        width:
                                            ScreenUtil.instance.setWidth(70.0),
                                        height:
                                            ScreenUtil.instance.setHeight(76.0),
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: CachedNetworkImageProvider(
                                                  userGame['icon']),
                                              fit: BoxFit.cover),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(ScreenUtil
                                                  .instance
                                                  .setWidth(75))),
                                        ),
                                        child: Container(
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.black,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              8.0))),
                                              child: Text(" " +
                                                  profileLoL.summonerLevel
                                                      .toString() +
                                                  " "),
                                            ),
                                          ),
                                        )),
                                  ),
                                  shape: CircleBorder(),
                                ),
                              ),
                              shape: CircleBorder(),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 15.0),
                            child: Text(
                              userGame['profile']['name'],
                              style: TextStyle(
                                  fontSize: ScreenUtil.instance.setSp(20.0),
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                      Divider(height: 1, color: Colors.yellow),
                      ranked5 != null ? Container(
                        color: HexColor('203454'),
                        child: ListTile(
                          leading: Image.asset('assets/games/lol/ranked/${ranked5['tier'].toLowerCase()}.png'),
                          title: Text(ranked5['tier']),
                          subtitle: Text(ranked5['leaguePoints'].toString() + " puntos"),
                          
                          trailing: Text('${ranked5['losses']} L - ${ranked5['wins']} W'),
                        ),
                      ) : Container(
                        color: HexColor('203454'),
                        child: ListTile(
                          leading: Image.network('http://apollo-uploads-las.s3.amazonaws.com/1434386084/unranked-season-rewards-lol.png'),
                          title: Text('Unranked'),
                        ),
                      ),
                      Divider(height: 1, color: Colors.yellow),
                    ],
                  ),
                )),
    );
  }
}
