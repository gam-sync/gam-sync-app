import 'package:flutter/material.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class OptionsPage extends StatefulWidget {
  final String uid;
  final String username;
  OptionsPage({Key key, this.uid, this.username}) : super(key: key);

  _OptionsPageState createState() => _OptionsPageState();
}

class _OptionsPageState extends State<OptionsPage> {

  var settings;
  bool isBlock = false;

  @override
  void initState() {
    _isBlock();
    super.initState();
    
  }

  _isBlock() async {
    List<dynamic> blockedUsers = await UserProvider.getBlockedUsers();

    setState(() {
     isBlock = blockedUsers.contains(widget.uid); 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Opciones del usuario ${widget.username}',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
            Container(
              child: ListTile(
                leading: Icon(Icons.block),
                title: !isBlock ? Text('Bloquear usuario') : Text('Desbloquear usuario'),
                onTap: () {
                  if (!isBlock) {
                    UserProvider.setBlock(widget.uid);
                    setState(() {
                      isBlock = true; 
                    });
                  } else {
                    UserProvider.setUnblock(widget.uid);
                    setState(() {
                      isBlock = false; 
                    });
                  }
                  
                },
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
            Container(
              child: ListTile(
                title: Text('Reportar usuario'),
                leading: Icon(Icons.report),
                onTap: () {

                },
              ),
            ),
            
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
          ],
        ),
      ),
    );
  }
}
