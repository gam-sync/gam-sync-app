import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:intl/intl.dart';

class ActivityPage extends StatefulWidget {
  ActivityPage({Key key}) : super(key: key);

  _ActivityPageState createState() => _ActivityPageState();
}

class _ActivityPageState extends State<ActivityPage> {
  int _pendingRequest = 0;
  String _uid;

  @override
  void initState() {
    _init();

    super.initState();
  }

  _init() {
    _getUserId();
    _getPendingRequest();
  }

  _getUserId() async {
    FirebaseUser user = await UserProvider.getCurrentAuthUser();
    setState(() {
      _uid = user.uid;
    });
  }

  _getPendingRequest() async {
    int requestsPending = await UserProvider.getRequestPendingCount();

    setState(() {
      _pendingRequest = requestsPending;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            elevation: 1,
            centerTitle: true,
            backgroundColor: HexColor('23395d'),
            title: Text(
              'Actividad',
              style: TextStyle(fontSize: 16),
            ),
          ),
        ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(
              'Lista de solicitudes de amistad',
              style: TextStyle(color: Colors.yellow),
            ),
            subtitle: Text('Tienes $_pendingRequest solicitudes'),
            trailing: Icon(Icons.arrow_right),
            onTap: () {
              Navigator.push(
                  context,
                  CupertinoPageRoute(
                      builder: (context) => RequestPage(
                            uid: _uid,
                          )));
            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            title: RichText(
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: 'Nicolr3',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).accentColor),
                ),
                TextSpan(text: ' te ha mencionado')
              ]),
            ),
            subtitle: Text('30/07/2019 12:25'),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://www.elcomercio.com/files/article_main/uploads/2016/09/19/57e07354c70c4.jpeg'),
            ),
            trailing: Icon(Icons.arrow_right),
          ),
          Divider()
        ],
      ),
    );
  }
}

class RequestPage extends StatefulWidget {
  final String uid;

  RequestPage({this.uid});

  _RequestPageState createState() => _RequestPageState();
}

class _RequestPageState extends State<RequestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              elevation: 1,
              centerTitle: true,
              backgroundColor: HexColor('23395d'),
              title: Text(
                'Solicitudes de amistad',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
      body: Container(
        color: HexColor('203454'),
        child: StreamBuilder(
          stream: UserProvider.getRequestPending(widget.uid),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return SpinKitDoubleBounce(
                color: Theme.of(context).accentColor,
                size: 50.0,
              );
            }
            if (snapshot.data.documents.length == 0)
              return Container(
                
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'No tienes ninguna solicitud de amistad.',
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                          '¡No te desanimes y busca a tu jugador ideal en nuestras salas de chat!')
                    ],
                  ),
                ),
              );
            return ListView.builder(
                padding: new EdgeInsets.all(8.0),
                itemBuilder: (context, index) {
                  DocumentSnapshot request = snapshot.data.documents[index];
                  return FutureBuilder(
                    future: UserProvider.getDataUser(request.documentID),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return SpinKitDoubleBounce(
                          color: Theme.of(context).accentColor,
                          size: 50.0,
                        );
                      }
                      DocumentSnapshot user = snapshot.data;
                      return Card(
                        color: HexColor('#384c6d'),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              onTap: () {
                                Navigator.push(context, new MaterialPageRoute(builder: (context) => ProfilePage(uid: request.documentID, username: user['name'],)));
                              },
                              leading: Material(
                                child: CachedNetworkImage(
                                  placeholder: (context, url) => Container(
                                    child: SpinKitDoubleBounce(
                                        color: Theme.of(context).accentColor,
                                        size: 50.0),
                                    width: 35.0,
                                    height: 35.0,
                                    padding: EdgeInsets.all(10.0),
                                  ),
                                  imageUrl: user['photoUrl'],
                                  width: 50.5,
                                  height: 50.5,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(20.0),
                                ),
                                clipBehavior: Clip.hardEdge,
                              ),
                              title: Text(user['name'] +
                                  ' te ha enviado una solicitud'),
                              subtitle: Text(
                                DateFormat('dd MMM kk:mm').format(
                                    DateTime.fromMillisecondsSinceEpoch(
                                        int.parse(request['created_at']))),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                FlatButton(
                                  child: Text(
                                    'Aceptar',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor),
                                  ),
                                  onPressed: () {
                                    UserProvider.acceptFriend(user.documentID);
                                  },
                                ),
                                FlatButton(
                                  child: Text(
                                    'Rechazar',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor),
                                  ),
                                  onPressed: () {
                                    UserProvider.removeFriend(user.documentID);
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  );
                },
                itemCount: snapshot.data.documents.length);
          },
        ),
      ),
    );
  }
}
