import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/events_provider.dart';
import 'package:gameapp/src/providers/game_provider.dart';
import 'package:gameapp/src/providers/user_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:gameapp/src/utils/screen_util.dart';
import 'package:gameapp/src/utils/toast_util.dart';

class AddEventPage extends StatefulWidget {
  AddEventPage({Key key}) : super(key: key);

  _AddEventPageState createState() => _AddEventPageState();
}

class _AddEventPageState extends State<AddEventPage> {
  // Fields for add event
  String gameId;
  String title;
  String description;
  String gameName = "Juego";
  String gameAsset;
  String gameIcon;
  bool enabledComments = false;
  var user;

  DateTime selectedDate = DateTime.now();

  // error: In case there is a possible errors
  Map<String, dynamic> error = {
    'mail_not_found': null,
    'fail_password': null,
    'unknown': null,
    'user_disabled': null,
    'user_banned': null,
  };

  @override
  void initState() {
    GameProvider.getGamesUser();
    _getUid();
    super.initState();
  }

  // getUid: Get uid from current user
  _getUid() async {
    var currentUser = await UserProvider.getDataAuthUser();
    setState(() {
      user = currentUser;
    });
  }

  // formKey: Key of the form
  final formKey = new GlobalKey<FormState>();

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate() && gameName != "Juego") {
      form.save();
      var event = {
        'description': this.description,
        'gameId': this.gameId,
        'gameAsset': this.gameAsset,
        'created_at': this.selectedDate,
        'enableComment': this.enabledComments,
        'gameIcon': this.gameIcon,
        'status': false,
        'user': {
          'name': user['name'],
          'photoUrl': user['photoUrl'],
          'uid': user.documentID,
        }
      };
      EventsProvider.createEvent(event);
      ToastUtil.show('Tu tablón se encuentra en revisión. Espera a que un administrador lo acepté.', context);
      Navigator.pop(context);
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {}


  // _buildGames: Create the drop-down to choose the games
  _buildGames(BuildContext context) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: ScreenUtil.instance.setHeight(300.0),
            color: HexColor('23395d'),
            child: FutureBuilder(
              future: GameProvider.getAllGamesDocuments(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                      child: SpinKitDoubleBounce(
                    color: Theme.of(context).accentColor,
                    size: 50.0,
                  ));

                return ListView.builder(
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) {
                    var game = snapshot.data.documents[index];
                    return Column(
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              backgroundImage: game['asset'] != null
                                  ? AssetImage(game['asset'])
                                  : CachedNetworkImageProvider(game['icon'])),
                          title: Text(
                            game['name'],
                            style: TextStyle(fontSize: 14.0),
                          ),
                          onTap: () {
                            setState(() {
                              gameId = game.documentID;
                              gameName = game['name'];
                              gameAsset = game['asset'];
                              gameIcon = game['icon'];
                            });
                            Navigator.pop(context);
                          },
                        ),
                        Divider(
                          height: 1.0,
                          color: Colors.white10,
                        )
                      ],
                    );
                  },
                );
              },
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtils(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Añadir tablon',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      body: Container(
        child: Center(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil.instance.width / 24,
                vertical: ScreenUtil.instance.height / 100),
            color: HexColor('203454'),
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 12.0, bottom: 1.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Contenido',
                          style: TextStyle(
                            fontSize: ScreenUtil.instance.setSp(12.0),
                          )),
                    ),
                  ),
                  Form(
                    key: formKey,
                    child: Column(
                      children: <Widget>[
                        Divider(
                          height: 30,
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20.0),
                          child: TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: 4,
                              decoration: InputDecoration(
                                errorStyle: TextStyle(color: Colors.red),
                                filled: true,
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                fillColor: Colors.black38,
                                hintText: 'Contenido del tablón',
                              ),
                              onChanged: (val) {},
                              onSaved: (value) => description = value),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12.0, bottom: 1.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text('Elige un juego',
                                style: TextStyle(
                                  fontSize: ScreenUtil.instance.setSp(12.0),
                                )),
                          ),
                        ),
                        Divider(
                          height: 30,
                        ),
                        Container(
                            margin: EdgeInsets.only(bottom: 20.0),
                            child: TextFormField(
                              onTap: () => _buildGames(context),
                              decoration: InputDecoration(
                                errorStyle: TextStyle(color: Colors.red),
                                filled: true,
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 12.0, horizontal: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.transparent, width: 0.0),
                                ),
                                fillColor: Colors.black38,
                                hintText: gameName,
                              ),
                            )),
                        Container(
                            child: Row(
                          children: <Widget>[
                            Checkbox(
                              onChanged: (value) {
                                setState(() {
                                  this.enabledComments = value;
                                });
                              },
                              value: this.enabledComments,
                              checkColor: Colors.white,
                              activeColor: HexColor('3f704d'),
                            ), 
                            Text('Activar comentarios') 
                          ],
                        )),
                        Container(
                          height: ScreenUtil.instance.setHeight(45),
                          margin: EdgeInsets.only(top: 20),
                          child: MaterialButton(
                            child: Container(
                                child: Text(
                              ' Crear tablon',
                              textAlign: TextAlign.center,
                            )),
                            color: HexColor('3f704d'),
                            onPressed: validateAndSave,
                            elevation: 4.0,
                            minWidth: double.infinity,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
