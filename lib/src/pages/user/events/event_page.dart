import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/providers/events_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';
import 'package:timeago/timeago.dart' as timeago;

class EventPage extends StatefulWidget {
  final String tablonId;
  final String user;
  final String userId;
  EventPage({Key key, this.tablonId, this.user, this.userId}) : super(key: key);

  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {
  var event;
  bool eventLoaded;
  final TextEditingController _textController = TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  

  @override
  void initState() {
    super.initState();
    _getEvent();
  }

  _getEvent() async {
    var tablon = await EventsProvider.getEvent(widget.tablonId);
    setState(() {
      this.event = tablon;
    });
  }

  @override
  Widget build(BuildContext context) {
    var date =
        DateTime.fromMillisecondsSinceEpoch(event['created_at'].seconds * 1000);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          title: Text(
            'Tablon de ${widget.user}',
            style: TextStyle(fontSize: 16.0),
          ),
        ),
      ),
      body: Container(
        color: HexColor('203454'),
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.transparent,
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.black26,
                ),
                child: Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: ListTile(
                    onTap: () {},
                    isThreeLine: true,
                    title: Text(event['user']['name']),
                    subtitle: Text(timeago.format(date, locale: 'es') +
                        "\n\n${event['description']}"),
                    leading: GestureDetector(
                      onTap: () => Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => ProfilePage(
                                    uid: event['user']['uid'],
                                    username: event['user']['name'],
                                  ))),
                      child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          backgroundImage: CachedNetworkImageProvider(
                              event['user']['photoUrl'])),
                    ),
                    trailing: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: event['gameAsset'] != null
                          ? AssetImage(event['gameAsset'])
                          : CachedNetworkImageProvider(event['gameIcon']),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),
            Divider(
              height: 2,
              color: Colors.white10,
            ),
            Container(
                margin: EdgeInsets.only(top: 10.0, left: 12, bottom: 20.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Comentarios',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ))),
            Container(
              height: 10.0,
              color: Colors.black26,
            ),                 
            Flexible(
              child: StreamBuilder(
                stream: EventsProvider.getComments(widget.tablonId),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return SpinKitCircle();
                  }

                  return ListView.builder(
                    controller: listScrollController,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      var comment = snapshot.data.documents[index];
                      var dateComment =
                          DateTime.fromMillisecondsSinceEpoch(comment['created_at'].seconds * 1000);
                      return Column(
                        children: <Widget>[
                          Divider(height: 1.0),
                          Container(
                            color: Colors.transparent,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                margin: EdgeInsets.only(bottom: 10.0),
                                child: ListTile(
                                  onTap: () {},
                                  isThreeLine: true,
                                  title: Text(comment['user']['name']),
                                  subtitle: Text(
                                      timeago.format(dateComment, locale: 'es') +
                                          "\n\n${comment['text']}"),
                                  leading: GestureDetector(
                                    onTap: () => Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                            builder: (context) => ProfilePage(
                                                  uid: comment['user']['uid'],
                                                  username: comment['user']
                                                      ['name'],
                                                ))),
                                    child: CircleAvatar(
                                        backgroundColor: Colors.transparent,
                                        backgroundImage:
                                            CachedNetworkImageProvider(
                                                comment['user']['photoUrl'])),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Divider(height: 1.0,)
                        ],
                      );
                    },
                  );
                },
              ),
            ),
            Container(
              decoration: new BoxDecoration(color: Theme.of(context).cardColor),
              child: _buildTextComposer(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextComposer() {
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        color: HexColor('3d5a8a'),
        child: Row(
          children: <Widget>[
            Flexible(
              child: Container(
                margin: EdgeInsets.only(left: 20.0),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  controller: _textController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration.collapsed(
                      hintText: "Envia un comentario",
                      focusColor: Colors.black,
                      fillColor: Colors.black),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    color: Theme.of(context).accentColor,
                    icon: Icon(Icons.send),
                    onPressed: () {

                      EventsProvider.addComent(_textController.text, widget.tablonId, userTablonId: widget.userId, );
                      _textController.clear();
                          listScrollController.animateTo(0.0,
        duration: Duration(milliseconds: 300), curve: Curves.easeOut);
                    }
                  ),
            )
          ],
        ),
      ),
    );
  }
}
