import 'package:admob_flutter/admob_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:gameapp/src/providers/chat_provider.dart';
import 'package:gameapp/src/providers/events_provider.dart';
import 'package:gameapp/src/utils/hex_util.dart';

class EventHomePage extends StatefulWidget {
  EventHomePage({Key key}) : super(key: key);

  _EventHomePageState createState() => _EventHomePageState();
}

class _EventHomePageState extends State<EventHomePage> {
  int lastAd = 3;
  /*static MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>['videogames', 'league of legends', 'fortnite', 'videojuegos', 'terraria', 'minecraft', 'moba', 'mmorpg', 'rpg', 'csgo', 'brawl stars', 'stardew valley', 'gamer', 'pc', 'ps4', 'ordenador', 'ps3', 'xbox', 'nintendo', 'switch', ],
    contentUrl: 'https://flutter.io',
    childDirected: false,
  ); */

  @override
  void initState() {
    super.initState();

    ChatProvider.checkConnectivity();
  }

  checkAds(int index) {
    if (index == 3) {
      this.lastAd += 4;
      return true;
    } else if (lastAd == index) {
        this.lastAd += 4;
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: HexColor('23395d'),
          title: Text(
            'Tablones',
            style: TextStyle(fontSize: 16),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add_circle_outline),
              onPressed: () => Navigator.pushNamed(context, 'addEvent'),
            ),
          ],
        ),
      ),
      body: Container(
          color: HexColor('203454'),
          child: FutureBuilder(
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                    child: SpinKitDoubleBounce(
                  color: Theme.of(context).accentColor,
                  size: 50.0,
                ));
              }


              return ListView.builder(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var event = snapshot.data.documents[index];
                  return Column(
                    children: <Widget>[
                      EventsProvider.buildEventGame(context, event),
                      checkAds(index)
                          ? Container(
                              margin: EdgeInsets.only(top: 20),
                              child: Container(
                                color: Colors.transparent,
                                child: Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.black26,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0))),
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        bottom: 10.0, top: 10.0),
                                    child: ListTile(
                                        title: Text('Anuncio\n'),
                                        subtitle: Column(
                                          children: <Widget>[
                                            AdmobBanner(
                                              adUnitId:
                                                  'ca-app-pub-5634554394830090/9694792890',
                                              adSize:
                                                  AdmobBannerSize.LARGE_BANNER,
                                            ),
                                            SizedBox(height: 10.0,)
                                          ],
                                        )),
                                  ),
                                ),
                              ),
                            )
                          : Container()
                    ],
                  );
                },
                itemCount: snapshot.data.documents.length,
              );
            },
            future: EventsProvider.getEvents(),
          )),
    );
  }
}
