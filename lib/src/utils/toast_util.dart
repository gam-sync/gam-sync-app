import 'package:flutter/widgets.dart';
import 'package:toast/toast.dart';

class ToastUtil {
  static show(String text, BuildContext context) {
    Toast.show(
      text,
      context,
      duration: 4,
      gravity: Toast.BOTTOM,
    );
  }
}
