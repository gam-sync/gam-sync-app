import 'package:flutter/material.dart';

class InDevelopmentPage extends StatelessWidget {
  final IconData icon;
  final String name;
  InDevelopmentPage({Key key, @required this.icon, @required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 50),
          child: Column(
            children: <Widget>[
              Center(
                child: Icon(
                  this.icon,
                  size: 100,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
              ),
              Center(
                child: Text(
                  this.name + ' esta en desarrollo',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10.0),
              ),
              Center(
                child: Text(
                  'Este apartado esta siendo desarrollado y llegara muy pronto',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white54),
                ),
              )
            ],
          )),
      appBar: AppBar(
        title: Text(this.name),
        centerTitle: true,
      ),
    );
  }
}
