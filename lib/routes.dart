import 'package:flutter/material.dart';
import 'package:gameapp/src/pages/auth/login_page.dart';
import 'package:gameapp/src/pages/auth/new_register_page.dart';
import 'package:gameapp/src/pages/auth/screen_page.dart';
import 'package:gameapp/src/pages/auth/start_page.dart';
import 'package:gameapp/src/pages/user/activity_page.dart';
import 'package:gameapp/src/pages/user/events/add_event_page.dart';
import 'package:gameapp/src/pages/user/profile/profile_image_page.dart';
import 'package:gameapp/src/pages/user/profile/profile.dart';
import 'package:gameapp/src/pages/user/profile/settings/account/account_edit_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/blocked_users_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/edit_user.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/add_game_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/brawl_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/lol_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/mine_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/overwatch_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/games/games/pokego_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/policy/services_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/policy/terminos_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/privacy_page.dart';
import 'package:gameapp/src/pages/user/profile/settings/settings_page.dart';
import 'package:gameapp/src/pages/user/start_page.dart';
import 'src/pages/user/admin/users/search/search_users.dart';
import 'src/pages/user/profile/settings/verify_email.dart';

class Routes {
  static var routes = <String, WidgetBuilder>{
    'start': (BuildContext context) => StartPage(),
    'register': (BuildContext context) => RegisterPageTwo(),
    'screen': (BuildContext context) => ScreenPage(),
    'startuser': (BuildContext context) => StartUserPage(),
    'profileimage': (BuildContext context) => ProfileImagePage(),
    'settings': (BuildContext context) => SettingsPage(),
    'edituser': (BuildContext context) => EditUser(),
    'verifyEmail': (BuildContext context) => VerifyEmail(),
    'login': (BuildContext context) => LoginPage(),
    'profile': (BuildContext context) => ProfilePage(),
    'terminos': (BuildContext context) => TerminosPage(),
    'services': (BuildContext context) => ServicesPage(),
    'editaccount': (BuildContext context) => EditAccountPage(),
    'addgame': (BuildContext context) => AddGamePage(),
    'loladd': (BuildContext context) => LolPage(),
    'mineadd': (BuildContext context) => MinePage(),
    'brawladd': (BuildContext context) => BrawlPage(),
    'adminSearchUsers': (BuildContext context) => AdminSearchUsersPage(),
    'privacy': (BuildContext context) => PrivacyPage(),
    'notifications': (BuildContext context) => ActivityPage(),
    'addEvent': (BuildContext context) => AddEventPage(),
    'overwatchadd': (BuildContext context) => OverwatchPage(),
    'friendship': (BuildContext context) => ActivityPage(),
    'pokegoadd': (BuildContext context) => PokegoPage(),
    'blockedUsers': (BuildContext context) => BlockedUsersPage(),
  };
}
