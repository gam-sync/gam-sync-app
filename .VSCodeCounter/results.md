# Summary

Date : 2019-11-30 02:07:27

Directory c:\Users\patuf\OneDrive\Desktop\gam-sync

Total : 131 files,  12235 codes, 251 comments, 1219 blanks, all 13705 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 71 | 11,316 | 150 | 1,084 | 12,550 |
| JSON | 8 | 307 | 0 | 2 | 309 |
| Groovy | 6 | 189 | 4 | 38 | 231 |
| XML | 25 | 165 | 50 | 16 | 231 |
| Java | 4 | 71 | 3 | 17 | 91 |
| Batch | 1 | 66 | 0 | 25 | 91 |
| YAML | 1 | 57 | 38 | 12 | 107 |
| Properties | 9 | 22 | 3 | 6 | 31 |
| Objective-C | 2 | 17 | 1 | 6 | 24 |
| Markdown | 2 | 13 | 0 | 9 | 22 |
| Shell Script | 1 | 8 | 2 | 1 | 11 |
| C++ | 1 | 4 | 0 | 3 | 7 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 131 | 12,235 | 251 | 1,219 | 13,705 |
| android | 14 | 252 | 26 | 37 | 315 |
| android1 | 21 | 348 | 32 | 63 | 443 |
| android1\.gradle | 4 | 1 | 1 | 4 | 6 |
| android1\.gradle\4.10.2 | 1 | 0 | 0 | 1 | 1 |
| android1\.gradle\5.1.1 | 1 | 0 | 0 | 1 | 1 |
| android1\.gradle\buildOutputCleanup | 1 | 1 | 1 | 1 | 3 |
| android1\.gradle\vcs-1 | 1 | 0 | 0 | 1 | 1 |
| android1\app | 10 | 185 | 30 | 24 | 239 |
| android1\app\release | 1 | 1 | 0 | 0 | 1 |
| android1\app\src | 7 | 89 | 27 | 14 | 130 |
| android1\app\src\debug | 1 | 4 | 3 | 1 | 8 |
| android1\app\src\main | 5 | 81 | 21 | 12 | 114 |
| android1\app\src\main\java | 2 | 44 | 3 | 7 | 54 |
| android1\app\src\main\java\es | 1 | 11 | 0 | 3 | 14 |
| android1\app\src\main\java\es\kibou | 1 | 11 | 0 | 3 | 14 |
| android1\app\src\main\java\es\kibou\gameapp | 1 | 11 | 0 | 3 | 14 |
| android1\app\src\main\java\es\kibou\gameapp\gameapp | 1 | 11 | 0 | 3 | 14 |
| android1\app\src\main\java\io | 1 | 33 | 3 | 4 | 40 |
| android1\app\src\main\java\io\flutter | 1 | 33 | 3 | 4 | 40 |
| android1\app\src\main\java\io\flutter\plugins | 1 | 33 | 3 | 4 | 40 |
| android1\app\src\main\res | 2 | 10 | 9 | 3 | 22 |
| android1\app\src\main\res\drawable | 1 | 4 | 7 | 2 | 13 |
| android1\app\src\main\res\values | 1 | 6 | 2 | 1 | 9 |
| android1\app\src\profile | 1 | 4 | 3 | 1 | 8 |
| android1\gradle | 1 | 5 | 1 | 1 | 7 |
| android1\gradle\wrapper | 1 | 5 | 1 | 1 | 7 |
| android\app | 9 | 164 | 25 | 27 | 216 |
| android\app\src | 7 | 73 | 24 | 17 | 114 |
| android\app\src\androidTest | 1 | 16 | 0 | 7 | 23 |
| android\app\src\androidTest\java | 1 | 16 | 0 | 7 | 23 |
| android\app\src\androidTest\java\es | 1 | 16 | 0 | 7 | 23 |
| android\app\src\androidTest\java\es\kibou | 1 | 16 | 0 | 7 | 23 |
| android\app\src\androidTest\java\es\kibou\gameapp | 1 | 16 | 0 | 7 | 23 |
| android\app\src\debug | 1 | 4 | 3 | 1 | 8 |
| android\app\src\main | 4 | 49 | 18 | 8 | 75 |
| android\app\src\main\java | 1 | 11 | 0 | 3 | 14 |
| android\app\src\main\java\com | 1 | 11 | 0 | 3 | 14 |
| android\app\src\main\java\com\gamsync | 1 | 11 | 0 | 3 | 14 |
| android\app\src\main\java\com\gamsync\app | 1 | 11 | 0 | 3 | 14 |
| android\app\src\main\res | 2 | 10 | 9 | 3 | 22 |
| android\app\src\main\res\drawable | 1 | 4 | 7 | 2 | 13 |
| android\app\src\main\res\values | 1 | 6 | 2 | 1 | 9 |
| android\app\src\profile | 1 | 4 | 3 | 1 | 8 |
| android\gradle | 1 | 5 | 1 | 1 | 7 |
| android\gradle\wrapper | 1 | 5 | 1 | 1 | 7 |
| assets | 14 | 14 | 0 | 0 | 14 |
| assets\achievements | 12 | 12 | 0 | 0 | 12 |
| assets\achievements\chats | 1 | 1 | 0 | 0 | 1 |
| assets\achievements\friends | 1 | 1 | 0 | 0 | 1 |
| assets\achievements\games | 5 | 5 | 0 | 0 | 5 |
| assets\achievements\xp | 5 | 5 | 0 | 0 | 5 |
| ios | 9 | 238 | 5 | 16 | 259 |
| ios\Flutter | 1 | 8 | 2 | 1 | 11 |
| ios\Runner | 8 | 230 | 3 | 15 | 248 |
| ios\Runner\Assets.xcassets | 3 | 148 | 0 | 4 | 152 |
| ios\Runner\Assets.xcassets\AppIcon.appiconset | 1 | 122 | 0 | 1 | 123 |
| ios\Runner\Assets.xcassets\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| ios\Runner\Base.lproj | 2 | 61 | 2 | 2 | 65 |
| lib | 70 | 11,302 | 140 | 1,077 | 12,519 |
| lib\src | 68 | 11,197 | 134 | 1,061 | 12,392 |
| lib\src\models | 6 | 579 | 2 | 75 | 656 |
| lib\src\models\games | 4 | 530 | 2 | 68 | 600 |
| lib\src\pages | 42 | 8,457 | 69 | 488 | 9,014 |
| lib\src\pages\auth | 5 | 1,347 | 46 | 72 | 1,465 |
| lib\src\pages\user | 37 | 7,110 | 23 | 416 | 7,549 |
| lib\src\pages\user\admin | 1 | 262 | 6 | 10 | 278 |
| lib\src\pages\user\events | 3 | 543 | 5 | 32 | 580 |
| lib\src\pages\user\message | 5 | 943 | 11 | 81 | 1,035 |
| lib\src\pages\user\message\chats | 1 | 302 | 3 | 27 | 332 |
| lib\src\pages\user\message\rooms | 3 | 472 | 8 | 39 | 519 |
| lib\src\pages\user\profile | 21 | 4,256 | 1 | 229 | 4,486 |
| lib\src\pages\user\profile\settings | 15 | 3,106 | 1 | 160 | 3,267 |
| lib\src\pages\user\profile\settings\account | 1 | 383 | 0 | 12 | 395 |
| lib\src\pages\user\profile\settings\games | 7 | 1,884 | 1 | 90 | 1,975 |
| lib\src\pages\user\profile\settings\games\games | 6 | 1,810 | 1 | 85 | 1,896 |
| lib\src\pages\user\profile\settings\policy | 2 | 52 | 0 | 6 | 58 |
| lib\src\pages\user\profile\stats | 1 | 213 | 0 | 15 | 228 |
| lib\src\pages\user\profile\tabs | 1 | 90 | 0 | 8 | 98 |
| lib\src\pages\user\users | 2 | 233 | 0 | 10 | 243 |
| lib\src\providers | 15 | 2,019 | 63 | 485 | 2,567 |
| lib\src\providers\games | 5 | 566 | 0 | 206 | 772 |
| lib\src\utils | 5 | 142 | 0 | 13 | 155 |
| test | 1 | 14 | 10 | 7 | 31 |

[details](details.md)